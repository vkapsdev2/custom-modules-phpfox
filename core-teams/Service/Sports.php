<?php

namespace Apps\PHPfox_Teams\Service;

use Phpfox;
use Phpfox_Error;
use Phpfox_File;
use Phpfox_Image;
use Phpfox_Parse_Input;
use Phpfox_Plugin;
use Phpfox_Request;
use Phpfox_Service;
use Phpfox_Url;

/**
 * Class Category
 *
 * @package Apps\PHPfox_Teams\Service
 */
class Sports extends Phpfox_Service
{
    
	public function addCategoryFromSports($aVals)
    {
        if (!isset($aVals['phrase_var_name'])) {
            //Add phrase for category
            $aLanguages = Phpfox::getService('language')->getAll();
            $name = $aVals['name_' . $aLanguages[0]['language_id']];
            $phrase_var_name = 'teams_category_' . md5('Pages/Groups Category' . $name . PHPFOX_TIME);
            //Add phrases
            $aText = [];
            foreach ($aLanguages as $aLanguage) {
                if (isset($aVals['name_' . $aLanguage['language_id']]) && !empty($aVals['name_' . $aLanguage['language_id']])) {
                    $aText[$aLanguage['language_id']] = $aVals['name_' . $aLanguage['language_id']];
                } else {
                    return Phpfox_Error::set((_p('Provide a "{{ language_name }}" name.',
                        ['language_name' => $aLanguage['title']])));
                }
            }
            $aValsPhrase = [
                'var_name' => $phrase_var_name,
                'text'     => $aText
            ];
            $finalPhrase = Phpfox::getService('language.phrase.process')->add($aValsPhrase);
        } else {
            $finalPhrase = $aVals['phrase_var_name'];
        }

        $iId = $this->database()->select('type_id')
            ->from(Phpfox::getT('pages_type'))
            ->where("sports_id = ".$aVals['sports_id'])
            ->execute('getSlaveField');
		
		if(empty($iId))
		{
			
			$iId = $this->database()->insert(Phpfox::getT('pages_type'), [
						'is_active'  => isset($aVals['is_active']) ? $aVals['is_active'] : '1',
						'name'       => $finalPhrase,
						'time_stamp' => PHPFOX_TIME,
						'ordering'   => '0',
						'item_type'  => 2,
						'sports_id'      => $aVals['sports_id'],
						'image_path'      => $aVals['image_path'],
						'image_server_id' => $aVals['server_id']
					]);
		}
		
		$this->database()->insert(Phpfox::getT('pages_type_links'), [
						'type_id'  => $iId,
						'time_stamp' => PHPFOX_TIME,
						'user_id'      => $aVals['user_id'],
					]);
					
		
		
        return $iId;
    }
}
