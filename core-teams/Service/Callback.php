<?php

namespace Apps\PHPfox_Teams\Service;

use Phpfox;
use Phpfox_Ajax;
use Phpfox_Component;
use Phpfox_Error;
use Phpfox_Plugin;
use Phpfox_Template;
use Phpfox_Url;

/**
 * Class Callback
 *
 * @package Apps\PHPfox_Teams\Service
 */
class Callback extends \Phpfox_Service
{
    public function __construct()
    {
        \Phpfox::getService('teams')->setIsInPage();
    }

    public function getFacade()
    {
        return Phpfox::getService('teams.facade');
    }

    public function getTeamPerms()
    {
        $aPerms = [
            'teams.share_updates' => _p('who_can_share_a_post'),
            'teams.view_admins' => _p('who_can_view_admins'),
            'teams.view_publish_date' => _p('teams_who_can_view_publish_date')
        ];

        return $aPerms;
    }

    public function getNotificationInvite($aNotification)
    {
        $aRow = \Phpfox::getService('teams')->getPage($aNotification['item_id']);

        if (!isset($aRow['page_id'])) {
            return false;
        }

        $sPhrase = Phpfox::getService('notification')->getUsers($aNotification)." invited you to check out the team ".Phpfox::getLib('parse.output')->shorten($aRow['title'],
                \Phpfox::getParam('notification.total_notification_title_length'), '...');

        return [
            'link' => \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']),
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    public function getProfileLink()
    {
        return 'profile.teams';
    }

    public function getProfileMenu($aUser)
    {
        if (!Phpfox::getUserParam('teams.pf_team_browse')) {
            return false;
        }

        if (\Phpfox::getParam('profile.show_empty_tabs') == false) {
            if (!isset($aUser['total_teams'])) {
                return false;
            }

            if (isset($aUser['total_teams']) && (int)$aUser['total_teams'] === 0) {
                return false;
            }
        }

        $iTotal = (int)(isset($aUser['total_teams']) ? $aUser['total_teams'] : 0);
        if (!(Phpfox::getUserParam('core.can_view_private_items') || $aUser['user_id'] == Phpfox::getUserId())) {
            $iSecretCount = $this->database()->select('COUNT(*)')
                ->from(\Phpfox::getT('pages'), 'p')
                ->where('p.user_id = ' . $aUser['user_id'] . ' AND p.reg_method = 2')
                ->execute('getSlaveField');
            $iTotal -= $iSecretCount;
        }
        $aMenus[] = [
            'phrase' => _p('Teams'),
            'url' => 'profile.teams',
            'total' => $iTotal,
            'icon' => 'feed/blog.png'
        ];

        return $aMenus;
    }

    public function canShareItemOnFeed()
    {
    }

    public function getActivityFeed($aItem, $aCallback = null, $bIsChildItem = false)
    {
		
        $itemId = (int)$aItem['item_id'];
        $feedId = (int)$aItem['feed_id'];
        $this->database()->select(\Phpfox::getUserField('u2') . ', ')->join(\Phpfox::getT('user'), 'u2',
            'u2.user_id = p.user_id');
        $aRow = $this->database()->select('p.reg_method, p.privacy, p.time_stamp, p.page_id, p.type_id, p.category_id, p.total_like, p.cover_photo_id, p.title, pu.vanity_url, p.image_path, p.image_server_id, p_type.name AS parent_category_name, pg.name AS category_name, pt.text_parsed')
            ->from(Phpfox::getT('pages'), 'p')
            ->join(Phpfox::getT('pages_text'), 'pt', 'pt.page_id = p.page_id')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = p.page_id')
            ->leftJoin(Phpfox::getT('pages_category'), 'pg', 'pg.category_id = p.category_id')
            ->leftJoin(Phpfox::getT('pages_type'), 'p_type', 'p_type.type_id = pg.type_id')
            ->where('p.page_id = ' . $itemId . ' AND p.item_type = 2')
            ->execute('getSlaveRow');

        if (!isset($aRow['page_id'])) {
            return false;
        }

        if ((!\Phpfox::getService('teams')->isMember($aRow['page_id']) && in_array((int)$aRow['reg_method'], [1, 2]))) {
            return false;
        }

        if ($bIsChildItem) {
            $aItem = array_merge($aRow, ['feed_id' => $aItem['feed_id']]);
        }

        $type = $this->getFacade()->getType()->getById($aRow['type_id']);

        if (empty($aRow['category_name'])) {
            $aRow['category_name'] = ucfirst(\Core\Lib::phrase()->isPhrase($type['name']) ? _p($type['name']) : $type['name']);
            $aRow['category_link'] = Phpfox::permalink('teams.category', $aRow['type_id'], $type['name']);
        } else {
            $aRow['category_name'] = \Core\Lib::phrase()->isPhrase($aRow['category_name']) ? _p($aRow['category_name']) : $aRow['category_name'];
            $aRow['type_link'] = Phpfox::permalink('teams.category', $aRow['type_id'], $type['name']);
            $aRow['category_link'] = Phpfox::permalink('teams.sub-category', $aRow['category_id'],
                \Core\Lib::phrase()->isPhrase($aRow['category_name']) ? _p($aRow['category_name']) : $aRow['category_name']);
        }

        $sLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);
        $aRow['team_url'] = $sLink;

        $iTotalLikes = $this->database()->select('COUNT(*)')
            ->from(Phpfox::getT('like'))
            ->where('item_id = ' . $feedId . " AND type_id = 'teams_created'")
            ->execute('getSlaveField');
        $iIsLikedFeed = $this->database()->select('COUNT(*)')
            ->from(Phpfox::getT('like'))
            ->where('item_id = ' . $feedId . " AND type_id = 'teams_created'" . ' AND user_id = ' . Phpfox::getUserId())
            ->execute('getSlaveField');

        $aRow['is_joined_team'] = Phpfox::getService('teams')->isMember($itemId);

        \Phpfox_Component::setPublicParam('custom_param_feed_team_' . $feedId, $aRow);

        $aRow['full_name'] = Phpfox::getLib('parse.output')->clean($aRow['full_name']);
        $sFullName = Phpfox::getLib('parse.output')->shorten($aRow['full_name'], 0);
        $aReturn = [
            'feed_title' => $aRow['title'],
            'no_user_show' => true,
            'feed_link' => $sLink,
            'feed_info' => _p('teams_user_created_team', [
                'title' => '<a href="' . $sLink . '" title="' . \Phpfox::getLib('parse.output')->clean($aRow['title']) . '">' . \Phpfox::getLib('parse.output')->clean(\Phpfox::getLib('parse.output')->shorten($aRow['title'],
                        50, '...')) . '</a>',
                'full_name' => '<span class="user_profile_link_span" id="js_user_name_link_' . $aRow['user_name'] . '">' . (Phpfox::getService('user.block')->isBlocked(null, $aRow['user_id']) ? '' : '<a href="' . Phpfox::getLib('url')->makeUrl('profile', [$aRow['user_name']])) . '">' . $sFullName . '</a></span>'
            ]),
            'feed_icon' => Phpfox::getLib('image.helper')->display([
                'theme' => 'module/marketplace.png',
                'return_url' => true
            ]),
            'return_url' => true,
            'time_stamp' => $aRow['time_stamp'],
            'like_type_id' => 'teams_created',
            'like_item_id' => $feedId,
            'feed_total_like' => $iTotalLikes,
            'feed_is_liked' => ((int)$iIsLikedFeed > 0 ? true : false),
            'load_block' => 'teams.feed-team',
        ];

        if ($bIsChildItem) {
            $aReturn = array_merge($aReturn, $aItem);
        }

        return $aReturn;
    }

    public function getCommentNotificationTag($aNotification)
    {
        $aRow = $this->database()->select('b.page_id, b.title, pu.vanity_url, u.full_name, fc.feed_comment_id')
            ->from(Phpfox::getT('comment'), 'c')
            ->join(Phpfox::getT('pages_feed_comment'), 'fc', 'fc.feed_comment_id = c.item_id')
            ->join(Phpfox::getT('pages'), 'b', 'b.page_id = fc.parent_user_id')
            ->join(Phpfox::getT('user'), 'u', 'u.user_id = c.user_id')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = b.page_id')
            ->where('c.comment_id = ' . (int)$aNotification['item_id'])
            ->execute('getSlaveRow');

        if (!$aRow) {
            return false;
        }

        $sPhrase = _p('full_name_tagged_you_on_a_team', ['full_name' => Phpfox::getService('notification')->getUsers($aRow)]);

        return [
            'link' => Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'],
                    $aRow['vanity_url']) . 'comment-id_' . $aRow['feed_comment_id'] . '/',
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    public function getNotificationPost_Tag($aNotification)
    {
        $aRow = $this->database()->select('p.page_id, p.title, pu.vanity_url, u.full_name, fc.feed_comment_id')
            ->from(Phpfox::getT('pages_feed_comment'), 'fc')
            ->join(Phpfox::getT('pages'), 'p', 'p.page_id = fc.parent_user_id')
            ->join(Phpfox::getT('user'), 'u', 'u.user_id = fc.user_id')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = p.page_id')
            ->where('fc.feed_comment_id = ' . (int)$aNotification['item_id'])
            ->execute('getSlaveRow');

        if (!$aRow) {
            return false;
        }

        $sPhrase = _p('full_name_tagged_you_in_a_team_team_name_post', [
            'full_name' => $aRow['full_name'],
            'team_name' => $aRow['title']
        ]);

        return [
            'link' => Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'],
                    $aRow['vanity_url']) . 'comment-id_' . $aRow['feed_comment_id'] . '/',
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    /**
     * Site statistics
     * @param $iStartTime
     * @param $iEndTime
     * @return array
     */
    public function getSiteStatsForAdmin($iStartTime, $iEndTime)
    {
        $aCond = [];
        $aCond[] = 'app_id = 0 AND view_id = 0 AND item_type = 2';
        if ($iStartTime > 0) {
            $aCond[] = 'AND time_stamp >= \'' . $this->database()->escape($iStartTime) . '\'';
        }
        if ($iEndTime > 0) {
            $aCond[] = 'AND time_stamp <= \'' . $this->database()->escape($iEndTime) . '\'';
        }

        $iCnt = (int)$this->database()->select('COUNT(*)')
            ->from(\Phpfox::getT('pages'))
            ->where($aCond)
            ->execute('getSlaveField');

        return [
            'phrase' => 'teams',
            'total' => $iCnt
        ];
    }

    public function addPhoto($iId)
    {
        \Phpfox::getService('teams')->setIsInPage();

        return [
            'module' => 'teams',
            'item_id' => $iId,
            'table_prefix' => 'pages_',
            'add_to_main_feed' => true
        ];
    }

    public function getDashboardActivity()
    {
        if (!Phpfox::getUserParam('teams.pf_team_browse')) {
            return [];
        }
        $aUser = Phpfox::getService('user')->get(Phpfox::getUserId(), true);
        return [
            _p('Teams') => $aUser['activity_teams']
        ];
    }

    public function getCommentNotification($aNotification)
    {
        $aRow = $this->database()->select('fc.feed_comment_id, u.user_id, u.gender, u.user_name, u.full_name, e.page_id, e.title, pu.vanity_url')
            ->from(\Phpfox::getT('pages_feed_comment'), 'fc')
            ->join(\Phpfox::getT('user'), 'u', 'u.user_id = fc.user_id')
            ->join(\Phpfox::getT('pages'), 'e', 'e.page_id = fc.parent_user_id')
            ->leftJoin(\Phpfox::getT('pages_url'), 'pu', 'pu.page_id = e.page_id')
            ->where('fc.feed_comment_id = ' . (int)$aNotification['item_id'])
            ->execute('getSlaveRow');

        if (!isset($aRow['feed_comment_id'])) {
            return false;
        }

        if ($aNotification['user_id'] == $aRow['user_id'] && isset($aNotification['extra_users']) && count($aNotification['extra_users'])) {
            $sUsers = Phpfox::getService('notification')->getUsers($aNotification, true);
        } else {
            $sUsers = Phpfox::getService('notification')->getUsers($aNotification);
        }
        $sTitle = Phpfox::getLib('parse.output')->shorten($aRow['title'],
            \Phpfox::getParam('notification.total_notification_title_length'), '...');

        if ($aNotification['user_id'] == $aRow['user_id']) {
            if (isset($aNotification['extra_users']) && count($aNotification['extra_users'])) {
                $sPhrase = _p('teams:users_commented_on_full_name_comment',
                    ['users' => $sUsers, 'full_name' => $aRow['full_name'], 'title' => $sTitle]);
            } else {
                $sPhrase = _p('teams:users_commented_on_gender_own_comment', [
                    'users' => $sUsers,
                    'gender' => Phpfox::getService('user')->gender($aRow['gender'], 1),
                    'title' => $sTitle
                ]);
            }
        } elseif ($aRow['user_id'] == \Phpfox::getUserId()) {
            $sPhrase = _p('teams:users_commented_on_one_of_your_comments',
                ['users' => $sUsers, 'title' => $sTitle]);
        } else {
            $sPhrase = _p('teams:users_commented_on_one_of_full_name_comments',
                ['users' => $sUsers, 'full_name' => $aRow['full_name'], 'title' => $sTitle]);
        }

        $sLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);

        return [
            'link' => $sLink . 'wall/comment-id_' . $aRow['feed_comment_id'],
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    public function getPhotoDetails($aPhoto)
    {
        \Phpfox::getService('teams')->setIsInPage();

        $aRow = \Phpfox::getService('teams')->getPage($aPhoto['team_id']);

        if (!isset($aRow['page_id'])) {
            return false;
        }

        \Phpfox::getService('teams')->setMode();

        $sLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);

        return [
            'breadcrumb_title' => _p('Teams'),
            'breadcrumb_home' => Phpfox_Url::instance()->makeUrl('teams'),
            'module_id' => 'teams',
            'item_id' => $aRow['page_id'],
            'title' => $aRow['title'],
            'url_home' => $sLink,
            'url_home_photo' => $sLink . 'photo/',
            'theater_mode' => _p('In the team <a href="{{ link }}">{{ title }}</a>',
                ['link' => $sLink, 'title' => $aRow['title']]),
            'set_default_phrase' => _p('Set as Team\'s Cover Photo'),
            'feed_table_prefix' => 'pages_'
        ];
    }

    public function getPhotoCount($iPageId)
    {
        $iCnt = $this->database()->select('COUNT(*)')
            ->from(\Phpfox::getT('photo'))
            ->where("module_id = 'pages' AND team_id = " . $iPageId)
            ->execute('getSlaveField');

        return ($iCnt > 0) ? $iCnt : 0;
    }

    public function getAlbumCount($iPageId)
    {
        $iCnt = $this->database()->select('COUNT(*)')
            ->from(\Phpfox::getT('photo_album'))
            ->where("module_id = 'pages' AND team_id = " . $iPageId)
            ->execute('getSlaveField');

        return ($iCnt > 0) ? $iCnt : 0;
    }


    public function addLink($aVals)
    {
        return [
            'module' => 'teams',
            'add_to_main_feed' => true,
            'item_id' => $aVals['callback_item_id'],
            'table_prefix' => 'pages_'
        ];
    }

    public function getFeedDisplay($iTeam)
    {
        $aTeam = \Phpfox::getService('teams')->getPage($iTeam);
        if (!$aTeam) {
            return false;
        }
        $bDisableShare = ($aTeam['reg_method'] == 0) ? false : true;
        if (!$bDisableShare) {
            $bDisableShare = !\Phpfox::getService('teams')->hasPerm($iTeam, 'teams.share_updates');
        }

        return [
            'module' => 'teams',
            'table_prefix' => 'pages_',
            'ajax_request' => 'teams.addFeedComment',
            'item_id' => $iTeam,
            'disable_share' => $bDisableShare
        ];
    }

    public function getActivityFeedCustomChecksComment($aRow)
    {
        if ((defined('PHPFOX_IS_PAGES_VIEW') && !\Phpfox::getService('teams')->hasPerm(null,
                    'teams.view_browse_updates'))
            || (!defined('PHPFOX_IS_PAGES_VIEW') && !\Phpfox::getService('teams')->hasPerm($aRow['custom_data_cache']['page_id'],
                    'teams.view_browse_updates'))
            || (defined('PHPFOX_IS_PAGES_VIEW') && !\Phpfox::getService('teams')->hasPerm(null,
                    'teams.share_updates'))
            || (!defined('PHPFOX_IS_PAGES_VIEW') && !\Phpfox::getService('teams')->hasPerm($aRow['custom_data_cache']['page_id'],
                    'teams.share_updates'))
        ) {
            return false;
        }

        if ($aRow['custom_data_cache']['reg_method'] == 2 &&
            (
                !\Phpfox::getService('teams')->isMember($aRow['custom_data_cache']['page_id']) &&
                !\Phpfox::getService('teams')->isAdmin($aRow['custom_data_cache']['page_id']) &&
                Phpfox::getService('user')->isAdminUser(Phpfox::getUserId())
            )
        ) {
            return false;
        }

        return $aRow;
    }

    public function getActivityFeedComment($aItem, $aCallBack = null, $bIsChildItem = false)
    {
        $aRow = $this->database()->select('fc.*, l.like_id AS is_liked, e.reg_method, e.page_id, e.title, e.app_id AS is_app, pu.vanity_url, ' . \Phpfox::getUserField('u',
                'parent_'))
            ->from(\Phpfox::getT('pages_feed_comment'), 'fc')
            ->join(\Phpfox::getT('pages'), 'e', 'e.page_id = fc.parent_user_id')
            ->join(\Phpfox::getT('user'), 'u', 'u.profile_page_id = e.page_id')
            ->leftJoin(\Phpfox::getT('pages_url'), 'pu', 'pu.page_id = e.page_id')
            ->leftJoin(\Phpfox::getT('like'), 'l',
                'l.type_id = \'teams_comment\' AND l.item_id = fc.feed_comment_id AND l.user_id = ' . \Phpfox::getUserId())
            ->where('fc.feed_comment_id = ' . (int)$aItem['item_id'])
            ->execute('getSlaveRow');

        if (!isset($aRow['page_id'])) {
            return false;
        }

        if ((defined('PHPFOX_IS_PAGES_VIEW') && !\Phpfox::getService('teams')->hasPerm(null,
                    'teams.view_browse_updates'))
            || (!defined('PHPFOX_IS_PAGES_VIEW') && !\Phpfox::getService('teams')->hasPerm($aRow['page_id'],
                    'teams.view_browse_updates'))
        ) {
            return false;
        }

        if ($aRow['reg_method'] == 2 &&
            (
                !\Phpfox::getService('teams')->isMember($aRow['page_id']) &&
                !\Phpfox::getService('teams')->isAdmin($aRow['page_id']) &&
                Phpfox::getService('user')->isAdminUser(Phpfox::getUserId())
            )
        ) {
            return false;
        }

        $sLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'],
                $aRow['vanity_url']) . 'wall/comment-id_' . $aItem['item_id'] . '/';

        $aReturn = [
            'feed_status' => htmlspecialchars($aRow['content']),
            'feed_link' => $sLink,
            'feed_title' => '',
            'total_comment' => $aRow['total_comment'],
            'feed_icon' => Phpfox::getLib('image.helper')->display([
                'theme' => 'misc/comment.png',
                'return_url' => true
            ]),
            'time_stamp' => $aRow['time_stamp'],
            'enable_like' => true,
            'comment_type_id' => 'teams',
            'like_type_id' => 'teams_comment',
            'feed_total_like' => $aRow['total_like'],
            'feed_is_liked' => $aRow['is_liked'],
            'is_custom_app' => $aRow['is_app'],
            'custom_data_cache' => $aRow
        ];

        if ($aRow['reg_method'] != 0) {
            $aReturn['no_share'] = true;
        }

        $aReturn['parent_user_name'] = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'],
            $aRow['vanity_url']);

        if ($aRow['user_id'] != $aRow['parent_user_id']) {
            if (!defined('PHPFOX_IS_PAGES_VIEW') && !defined('PHPFOX_PAGES_ADD_COMMENT')) {
                $aReturn['parent_user'] = Phpfox::getService('user')->getUserFields(true, $aRow, 'parent_');
            }
        }

        if ($bIsChildItem) {
            $aReturn = array_merge($aReturn, $aItem);
        }

        return $aReturn;
    }

    public function getActivityFeedItemLiked($aItem, $aCallback = null, $bIsChildItem = false)
    {
        $itemId = (int)$aItem['item_id'];
        $feedId = (int)$aItem['feed_id'];
        $this->database()->select(\Phpfox::getUserField('u2') . ', ')->join(\Phpfox::getT('user'), 'u2',
            'u2.user_id = p.user_id');
        $aRow = $this->database()->select('p.page_id, p.title, p.total_like, pu.vanity_url, p.image_path, p.image_server_id')
            ->from(\Phpfox::getT('pages'), 'p')
            ->leftJoin(\Phpfox::getT('pages_url'), 'pu', 'pu.page_id = p.page_id')
            ->where('p.page_id = ' . $itemId)
            ->execute('getSlaveRow');

        if (!isset($aRow['page_id'])) {
            return false;
        }

        $sLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);
        $iTotalLikes = $this->database()->select('COUNT(*)')
            ->from(Phpfox::getT('like'))
            ->where('item_id = ' . $feedId . " AND type_id = 'teams_liked'")
            ->execute('getSlaveField');
        $iIsLikedFeed = $this->database()->select('COUNT(*)')
            ->from(Phpfox::getT('like'))
            ->where('item_id = ' . $feedId . " AND type_id = 'teams_liked'" . ' AND user_id = ' . Phpfox::getUserId())
            ->execute('getSlaveField');

        \Phpfox_Component::setPublicParam('custom_param_feed_team_' . $feedId, $aRow);

        $aReturn = [
            'feed_title' => '',
            'feed_info' => _p('joined the team "<a href="{{ link }}" title="{{ link_title }}">{{ title }}</a>".',
                [
                    'link' => $sLink,
                    'link_title' => \Phpfox::getLib('parse.output')->clean($aRow['title']),
                    'title' => \Phpfox::getLib('parse.output')->clean(\Phpfox::getLib('parse.output')->shorten($aRow['title'],
                        50, '...'))
                ]),
            'feed_link' => $sLink,
            'no_target_blank' => true,
            'feed_icon' => \Phpfox::getLib('image.helper')->display([
                'theme' => 'misc/comment.png',
                'return_url' => true
            ]),
            'time_stamp' => $aItem['time_stamp'],
            'like_type_id' => 'teams_liked',
            'feed_total_like' => $iTotalLikes,
            'feed_is_liked' => ((int)$iIsLikedFeed > 0 ? true : false),
            'like_item_id' => $feedId
        ];

        if (!empty($aRow['image_path'])) {
            $sImage = \Phpfox::getLib('image.helper')->display([
                    'server_id' => $aRow['image_server_id'],
                    'path' => 'pages.url_image',
                    'file' => $aRow['image_path'],
                    'suffix' => '_120_square',
                    'max_width' => 120,
                    'max_height' => 120
                ]
            );

            $aReturn['feed_image'] = $sImage;
        }

        if ($bIsChildItem) {
            $aRow['full_name'] = Phpfox::getLib('parse.output')->clean($aRow['full_name']);
            $sFullName = Phpfox::getLib('parse.output')->shorten($aRow['full_name'], 0);
            $aReturn['no_user_show'] = true;
            $aReturn['feed_info'] = _p('teams_user_created_team', [
                'title' => '<a href="' . $sLink . '" title="' . \Phpfox::getLib('parse.output')->clean($aRow['title']) . '">' . \Phpfox::getLib('parse.output')->clean(\Phpfox::getLib('parse.output')->shorten($aRow['title'],
                        50, '...')) . '</a>',
                'full_name' => '<span class="user_profile_link_span" id="js_user_name_link_' . $aRow['user_name'] . '">' . (Phpfox::getService('user.block')->isBlocked(null, $aRow['user_id']) ? '' : '<a href="' . Phpfox::getLib('url')->makeUrl('profile', [$aRow['user_name']])) . '">' . $sFullName . '</a></span>'
            ]);
            $aReturn = array_merge($aReturn, $aItem);
        }

        return $aReturn;

    }

    public function addEvent($iItem)
    {
        \Phpfox::getService('teams')->setIsInPage();

        $aRow = \Phpfox::getService('teams')->getPage($iItem);

        if (!isset($aRow['page_id'])) {
            return false;
        }

        return $aRow;
    }

    public function viewEvent($iItem)
    {
        $aRow = $this->addEvent($iItem);

        if (!$aRow) {
            return false;
        }

        $sLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);

        return [
            'breadcrumb_title' => _p('Teams'),
            'breadcrumb_home' => Phpfox_Url::instance()->makeUrl('teams'),
            'module_id' => 'teams',
            'item_id' => $aRow['page_id'],
            'title' => $aRow['title'],
            'url_home' => $sLink,
            'url_home_pages' => $sLink . 'event/'
        ];
    }
	
	public function addGame($iItem)
    {
        /*\Phpfox::getService('teams')->setIsInPage();*/
        $aRow = \Phpfox::getService('teams')->getPage($iItem);

        if (!isset($aRow['page_id'])) {
            return false;
        }

        return $aRow;
    }
	
	public function viewGame($iItem)
    {
        $aRow = $this->addGame($iItem);

        if (!$aRow) {
            return false;
        }

        $sLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);
		
        $aGameCategoryId = '';
		if (Phpfox::isModule('game')) 
		{
			$aSports = \Phpfox::getService('game.sports')->getSports($aRow['type_id']);
			
			$aCateInsert = [
				'name_en' => $aSports['title'],
				'is_active' => 1,
				'user_id' => Phpfox::getUserId(),
				'sports_id' => $aSports['listing_id']
				];
				
			$aGameCategoryId = Phpfox::getService('game.sports')->addCategoryFromSports($aCateInsert);
		}
			
        return [
            'breadcrumb_title' => _p('Teams'),
            'breadcrumb_home' => Phpfox_Url::instance()->makeUrl('teams'),
            'module_id' => 'teams',
            'item_id' => $aRow['page_id'],
            'title' => $aRow['title'],
            'url_home' => $sLink,
			'aGameCategoryId'=>$aGameCategoryId,
			'sports_id' => $aSports['listing_id'],
            'url_home_pages' => $sLink . 'game/'
        ];
    }

    public function getFeedDetails($iItemId)
    {
        return [
            'module' => 'teams',
            'table_prefix' => 'pages_',
            'item_id' => $iItemId,
            'add_to_main_feed' => true
        ];
    }

    public function deleteFeedItem($callbackData)
    {
        if (empty($callbackData['type_id']) || empty($callbackData['item_id'])) {
            return false;
        }

        // delete feed from main feed
        db()->delete(':feed', ['type_id' => $callbackData['type_id'], 'item_id' => $callbackData['item_id']]);
        if ($callbackData['type_id'] == 'teams_comment') {
            $aFeedComment = $this->database()->select('*')
                ->from(\Phpfox::getT('pages_feed_comment'))
                ->where('feed_comment_id = ' . (int)$callbackData['item_id'])
                ->execute('getSlaveRow');

            if (empty($aFeedComment) || empty($aFeedComment['parent_user_id'])) {
                return;
            }

            $iTotalComments = $this->database()->select('COUNT(*)')
                ->from(\Phpfox::getT('pages_feed'))
                ->where('type_id = \'teams_comment\' AND parent_user_id = ' . $aFeedComment['parent_user_id'])
                ->execute('getSlaveField');

            $this->database()->update(\Phpfox::getT('pages'), ['total_comment' => $iTotalComments],
                'page_id = ' . (int)$aFeedComment['parent_user_id']);
        }
    }

    public function deleteLike($iItemId, $iUserId = 0)
    {
        if (!$iUserId) {
            $iUserId = Phpfox::getUserId();
        }
        // Get the threads from this page
        if (db()->tableExists(Phpfox::getT('forum_thread'))) {
            $aRows = $this->database()->select('thread_id')
                ->from(\Phpfox::getT('forum_thread'))
                ->where('team_id = ' . (int)$iItemId)
                ->execute('getSlaveRows');

            $aThreads = [];
            foreach ($aRows as $sKey => $aRow) {
                $aThreads[] = $aRow['thread_id'];
            }
            if (!empty($aThreads)) {
                $this->database()->delete(Phpfox::getT('forum_subscribe'),
                    'user_id = ' . Phpfox::getUserId() . ' AND thread_id IN (' . implode(',', $aThreads) . ')');
            }
        }

        $aRow = Phpfox::getService('teams')->getPage($iItemId);
        if (!isset($aRow['page_id'])) {
            return false;
        }

        $totalLike = db()->select('total_like')
            ->from(Phpfox::getT('pages'))
            ->where('page_id = ' . (int)$iItemId)
            ->execute('getSlaveField');

        $this->database()->updateCount('like', 'type_id = \'teams\' AND item_id = ' . (int)$iItemId . '', 'total_like', 'pages', 'page_id = ' . (int)$iItemId);

        if (defined('PHPFOX_CANCEL_ACCOUNT') && PHPFOX_CANCEL_ACCOUNT) {
            db()->update(Phpfox::getT('pages'), ['total_like' => ['= total_like - ', 1]], 'page_id = ' . (int)$iItemId . ' AND total_like = ' . (int)$totalLike);
        }

        $iFriendId = (int)$this->database()->select('user_id')
            ->from(Phpfox::getT('user'))
            ->where('profile_page_id = ' . (int)$aRow['page_id'])
            ->execute('getSlaveField');

        $this->database()->delete(Phpfox::getT('friend'), 'user_id = ' . (int)$iFriendId . ' AND friend_user_id = ' . (int)$iUserId);
        $this->database()->delete(Phpfox::getT('friend'), 'friend_user_id = ' . (int)$iFriendId . ' AND user_id = ' . (int)$iUserId);

        // clear cache members
        $this->cache()->remove('teams_' . $iItemId . '_members');
        $this->cache()->remove('member_' . $iUserId . '_secret_teams');
        $this->cache()->remove('member_' . $iUserId . '_teams');

        if (Phpfox::getService('teams')->isAdmin($iItemId, $iUserId)) {
            db()->delete(Phpfox::getT('pages_admin'), 'page_id = ' . (int)$iItemId . ' AND user_id = ' . (int)$iUserId);
            $this->cache()->remove('teams_' . $iItemId . '_admins');
        }

        if ($iUserId == Phpfox::getUserId()) {
            $sLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);
            if (!defined('PHPFOX_CANCEL_ACCOUNT') || PHPFOX_CANCEL_ACCOUNT != true) {
                Phpfox_Ajax::instance()->call('window.location.href = \'' . $sLink . '\';');
                return true;
            }
        } else {  /* Remove invites */ // Its not the user willingly leaving the page
            $this->database()->delete(\Phpfox::getT('pages_invite'), 'page_id = ' . (int)$iItemId . ' AND invited_user_id =' . (int)$iUserId);
        }

        return true;
    }

    public function addLike($iItemId, $bDoNotSendEmail = false, $iUserId = null)
    {
        $aRow = \Phpfox::getService('teams')->getPage($iItemId);

        if (!isset($aRow['page_id'])) {
            return false;
        }

        $this->database()->updateCount('like', 'type_id = \'teams\' AND item_id = ' . (int)$iItemId . '', 'total_like', 'pages', 'page_id = ' . (int)$iItemId);
        $sLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);
        $bIsApprove = true;
        if ($iUserId === null) {
            $bIsApprove = false;
            if (Phpfox::isUser()) {
                $iUserId = Phpfox::getUserId();
                \Phpfox_Queue::instance()->addJob('teams_member_join_notifications', [
                    'aTeam' => $aRow,
                    'iUserId' => $iUserId
                ]);
            }
        } else {
            \Phpfox::getLib('mail')->to($iUserId)
                ->subject(['Membership accepted to "{{ title }}"', ['title' => $aRow['title']]])
                ->message(['Your membership to the team "<a href="{{ link }}">{{ title }}</a>" has been accepted. To view this team follow the link below: <a href="{{ link }}">{{ link }}</a>',
                    ['link' => $sLink, 'title' => $aRow['title']]])
                ->sendToSelf(true)
                ->send();

            Phpfox::getService('notification.process')->add('teams_joined', $aRow['page_id'], $iUserId, $aRow['user_id'], true);
        }

        $iFriendId = (int)$this->database()->select('user_id')
            ->from(\Phpfox::getT('user'))
            ->where('profile_page_id = ' . (int)$aRow['page_id'])
            ->execute('getSlaveField');

        if ($iUserId) {
            $this->database()->insert(\Phpfox::getT('friend'), [
                    'is_page' => 1,
                    'list_id' => 0,
                    'user_id' => $iUserId,
                    'friend_user_id' => $iFriendId,
                    'time_stamp' => PHPFOX_TIME
                ]
            );

            $this->database()->insert(\Phpfox::getT('friend'), [
                    'is_page' => 1,
                    'list_id' => 0,
                    'user_id' => $iFriendId,
                    'friend_user_id' => $iUserId,
                    'time_stamp' => PHPFOX_TIME
                ]
            );

            // clear cache members
            $this->cache()->remove('teams_' . $iItemId . '_members');
            $this->cache()->remove('member_' . $iUserId . '_secret_teams');
            $this->cache()->remove('member_' . $iUserId . '_teams');
        }

        if (!$bIsApprove) {
            \Phpfox_Ajax::instance()->call('window.location.href = \'' . $sLink . '\';');
        }

        return true;
    }
	
	public function addFanLike($iItemId, $bDoNotSendEmail = false, $iUserId = null)
    {
        $aRow = \Phpfox::getService('teams')->getPage($iItemId);

        if (!isset($aRow['page_id'])) {
            return false;
        }

        //$this->database()->updateCount('like', 'type_id = \'teams\' AND item_id = ' . (int)$iItemId . '', 'total_fan', 'pages', 'page_id = ' . (int)$iItemId);
		
		$this->database()->updateCounter('pages', 'total_fan', 'page_id',
                $iItemId);
				
        $sLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);
        $bIsApprove = true;
        if ($iUserId === null) {
            $bIsApprove = false;
            if (Phpfox::isUser()) {
                $iUserId = Phpfox::getUserId();
                \Phpfox_Queue::instance()->addJob('teams_member_join_notifications', [
                    'aTeam' => $aRow,
                    'iUserId' => $iUserId
                ]);
            }
        } else {
            \Phpfox::getLib('mail')->to($iUserId)
                ->subject(['Membership accepted to "{{ title }}"', ['title' => $aRow['title']]])
                ->message(['Your membership to the team "<a href="{{ link }}">{{ title }}</a>" has been accepted. To view this team follow the link below: <a href="{{ link }}">{{ link }}</a>',
                    ['link' => $sLink, 'title' => $aRow['title']]])
                ->sendToSelf(true)
                ->send();

            Phpfox::getService('notification.process')->add('teams_joined', $aRow['page_id'], $iUserId, $aRow['user_id'], true);
        }

        $iFriendId = (int)$this->database()->select('user_id')
            ->from(\Phpfox::getT('user'))
            ->where('profile_page_id = ' . (int)$aRow['page_id'])
            ->execute('getSlaveField');

        if ($iUserId) {
            $this->database()->insert(\Phpfox::getT('friend'), [
                    'is_page' => 1,
                    'list_id' => 0,
                    'user_id' => $iUserId,
                    'friend_user_id' => $iFriendId,
                    'time_stamp' => PHPFOX_TIME
                ]
            );

            $this->database()->insert(\Phpfox::getT('friend'), [
                    'is_page' => 1,
                    'list_id' => 0,
                    'user_id' => $iFriendId,
                    'friend_user_id' => $iUserId,
                    'time_stamp' => PHPFOX_TIME
                ]
            );

            // clear cache members
            $this->cache()->remove('teams_' . $iItemId . '_members');
            $this->cache()->remove('member_' . $iUserId . '_secret_teams');
            $this->cache()->remove('member_' . $iUserId . '_teams');
        }

        if (!$bIsApprove) {
            \Phpfox_Ajax::instance()->call('window.location.href = \'' . $sLink . '\';');
        }

        return true;
    }

    public function getMusicDetails($aItem)
    {
        \Phpfox::getService('teams')->setIsInPage();

        $aRow = \Phpfox::getService('teams')->getPage($aItem['item_id']);

        if (!isset($aRow['page_id'])) {
            return false;
        }

        \Phpfox::getService('teams')->setMode();

        $sLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);

        return [
            'breadcrumb_title' => _p('Teams'),
            'breadcrumb_home' => Phpfox_Url::instance()->makeUrl('teams'),
            'module_id' => 'teams',
            'item_id' => $aRow['page_id'],
            'title' => $aRow['title'],
            'url_home' => $sLink,
            'url_home_photo' => $sLink . 'music/',
            'theater_mode' => _p('In the team <a href="{{ link }}">{{ title }}</a>',
                ['link' => $sLink, 'title' => $aRow['title']])
        ];
    }

    public function getBlogDetails($aItem)
    {
        \Phpfox::getService('teams')->setIsInPage();
        $aRow = \Phpfox::getService('teams')->getPage($aItem['item_id']);
        if (!isset($aRow['page_id'])) {
            return false;
        }
        $sLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);

        return [
            'breadcrumb_title' => _p('Teams'),
            'breadcrumb_home' => Phpfox_Url::instance()->makeUrl('teams'),
            'module_id' => 'teams',
            'item_id' => $aRow['page_id'],
            'title' => $aRow['title'],
            'url_home' => $sLink,
            'url_home_photo' => $sLink . 'blog/',
            'theater_mode' => _p('In the team <a href="{{ link }}">{{ title }}</a>',
                ['link' => $sLink, 'title' => $aRow['title']])
        ];
    }

    public function getVideoDetails($aItem)
    {
        $teamService = Phpfox::getService('teams');

        $aRow = $teamService->getPage($aItem['item_id']);

        if (!isset($aRow['page_id'])) {
            return false;
        }

        $teamService->setMode();

        $sLink = $teamService->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);

        return [
            'breadcrumb_title' => _p('teams'),
            'breadcrumb_home' => Phpfox_Url::instance()->makeUrl('teams'),
            'module_id' => 'teams',
            'item_id' => $aRow['page_id'],
            'title' => $aRow['title'],
            'url_home' => $sLink,
            'url_home_photo' => $sLink . 'video/',
            'theater_mode' => _p('in_the_page_link_title', ['link' => $sLink, 'title' => $aRow['title']])
        ];
    }

    public function uploadVideo($aVals)
    {
        \Phpfox::getService('teams')->setIsInPage();
        return [
            'module' => 'teams',
            'item_id' => (is_array($aVals) && isset($aVals['callback_item_id']) ? $aVals['callback_item_id'] : (int)$aVals)
        ];
    }

    public function uploadSong($iItemId)
    {
        \Phpfox::getService('teams')->setIsInPage();

        return [
            'module' => 'teams',
            'item_id' => $iItemId,
            'table_prefix' => 'pages_',
            'add_to_main_feed' => true
        ];
    }

    public function getNotificationJoined($aNotification)
    {
        $aRow = \Phpfox::getService('teams')->getPage($aNotification['item_id']);

        if (!isset($aRow['page_id'])) {
            return false;
        }

        return [
            'link' => \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']),
            'message' => _p('Your membership has been accepted to join the team "{{ title }}".', [
                'title' => \Phpfox::getLib('parse.output')->shorten($aRow['title'],
                    \Phpfox::getParam('notification.total_notification_title_length'), '...')
            ]),
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    public function getNotificationRegister($aNotification)
    {
        $aRow = $this->database()->select('p.*, pu.vanity_url, ' . \Phpfox::getUserField())
            ->from(\Phpfox::getT('pages_signup'), 'ps')
            ->join(\Phpfox::getT('pages'), 'p', 'p.page_id = ps.page_id')
            ->join(\Phpfox::getT('user'), 'u', 'u.user_id = ps.user_id')
            ->leftJoin(\Phpfox::getT('pages_url'), 'pu', 'pu.page_id = p.page_id')
            ->where('ps.signup_id = ' . (int)$aNotification['item_id'])
            ->execute('getSlaveRow');

        if (!isset($aRow['page_id'])) {
            return false;
        }

        return [
            // 'no_profile_image' => true,
            'link' => \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']),
            'message' => _p('full_name_is_requesting_to_join_your_team_title', [
                'full_name' => Phpfox::getService('notification')->getUsers($aRow),
                'title' => \Phpfox::getLib('parse.output')->shorten($aRow['title'],
                    \Phpfox::getParam('notification.total_notification_title_length'), '...')
            ]),
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    public function getNotificationLike($aNotification)
    {
        $aRow = \Phpfox::getService('teams')->getPage($aNotification['item_id']);

        if (!isset($aRow['page_id'])) {
            return false;
        }

        $sUsers = Phpfox::getService('notification')->getUsers($aNotification);
        if (!isset($aRow['gender'])) {
            $sGender = 'their';
        } else {
            $sGender = Phpfox::getService('user')->gender($aRow['gender'], 1);
        }
        $sTitle = Phpfox::getLib('parse.output')->shorten($aRow['title'],
            Phpfox::getParam('notification.total_notification_title_length'), '...');

        if ($aNotification['user_id'] == $aRow['user_id']) {
            $sPhrase = _p('{{ users }} joined {{ gender }} own team "{{ title }}"',
                ['users' => $sUsers, 'gender' => $sGender, 'title' => $sTitle]);
        } elseif ($aRow['user_id'] == Phpfox::getUserId()) {
            $sPhrase = _p('user_joined_your_team_title', ['users' => $sUsers, 'title' => $sTitle]);
        } else {
            $aOwner = Phpfox::getService('teams')->getPageOwner($aRow['page_id']);
            $sPhrase = _p('user_joined_full_name_team_title',
                [
                    'users' => $sUsers,
                    'full_name' => \Phpfox::getLib('parse.output')->shorten($aOwner['full_name'], 0),
                    'title' => $sTitle
                ]);
        }

        return [
            'link' => \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']),
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    public function addForum($iId)
    {
        \Phpfox::getService('teams')->setIsInPage();

        $aRow = \Phpfox::getService('teams')->getPage($iId);

        if (!isset($aRow['page_id'])) {
            return false;
        }

        $sLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);

        return [
            'module' => 'teams',
            'module_title' => _p('Teams'),
            'item' => $aRow['page_id'],
            'team_id' => $aRow['page_id'],
            'url_home' => $sLink,
            'title' => $aRow['title'],
            'table_prefix' => 'pages_',
            'item_id' => $aRow['page_id'],
            'add_to_main_feed' => true,
            'breadcrumb_title' => _p('Teams'),
            'breadcrumb_home' => Phpfox_Url::instance()->makeUrl('teams'),
        ];
    }

    public function checkFeedShareLink()
    {
        return false;
    }

    public function getAjaxCommentVar()
    {
        return null;
    }

    public function getRedirectComment($iId)
    {
        $aListing = $this->database()->select('pfc.feed_comment_id AS comment_item_id, pfc.privacy_comment, pfc.user_id AS comment_user_id, m.*, pu.vanity_url, pfc.parent_user_id AS item_id')
            ->from(Phpfox::getT('pages_feed_comment'), 'pfc')
            ->join(Phpfox::getT('pages'), 'm', 'm.page_id = pfc.parent_user_id')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = m.page_id')
            ->where('pfc.feed_comment_id = ' . (int)$iId)
            ->execute('getSlaveRow');

        if (!isset($aListing['page_id'])) {
            return false;
        }

        return \Phpfox::getService('teams')->getUrl($aListing['page_id'], $aListing['title'],
                $aListing['vanity_url']) . 'comment-id_' . $aListing['comment_item_id'] . '/';
    }

    public function getFeedRedirect($iId, $iChild = 0)
    {
        $aListing = $this->database()->select('m.page_id, m.title, pu.vanity_url, pf.item_id')
            ->from(Phpfox::getT('pages_feed'), 'pf')
            ->join(Phpfox::getT('pages'), 'm', 'm.page_id = pf.parent_user_id')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = m.page_id')
            ->where('pf.feed_id = ' . (int)$iId)
            ->execute('getSlaveRow');

        if (!isset($aListing['page_id'])) {
            return false;
        }

        return \Phpfox::getService('teams')->getUrl($aListing['page_id'], $aListing['title'],
                $aListing['vanity_url']) . 'comment-id_' . $aListing['item_id'] . '/';
    }

    public function getItemName($iId, $sName)
    {
        return '<a href="' . Phpfox_Url::instance()->makeUrl('comment.view', ['id' => $iId]) . '">' . _p('on_name_s_team_comment', ['name' => $sName]) . '</a>';
    }

    public function getCommentItem($iId)
    {
        $aRow = $this->database()->select('feed_comment_id AS comment_item_id, privacy_comment, user_id AS comment_user_id')
            ->from(Phpfox::getT('pages_feed_comment'))
            ->where('feed_comment_id = ' . (int)$iId)
            ->execute('getSlaveRow');

        $aRow['comment_view_id'] = '0';

        if (!Phpfox::getService('comment')->canPostComment($aRow['comment_user_id'], $aRow['privacy_comment'])) {
            Phpfox_Error::set(_p('Unable to post a comment on this item due to privacy settings.'));

            unset($aRow['comment_item_id']);
        }

        $aRow['parent_module_id'] = 'teams';

        return $aRow;
    }

    public function addComment($aVals, $iUserId = null, $sUserName = null)
    {
        $aRow = $this->database()->select('fc.feed_comment_id, fc.user_id, e.page_id, e.title, u.full_name, u.gender, pu.vanity_url, u.profile_page_id')
            ->from(Phpfox::getT('pages_feed_comment'), 'fc')
            ->join(Phpfox::getT('pages'), 'e', 'e.page_id = fc.parent_user_id')
            ->join(Phpfox::getT('user'), 'u', 'u.user_id = fc.user_id')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = e.page_id')
            ->where('fc.feed_comment_id = ' . (int)$aVals['item_id'])
            ->execute('getSlaveRow');

        // Update the post counter if its not a comment put under moderation or if the person posting the comment is the owner of the item.
        if (empty($aVals['parent_id'])) {
            $this->database()->updateCounter('pages_feed_comment', 'total_comment', 'feed_comment_id',
                $aRow['feed_comment_id']);
        }

        if ($aRow['profile_page_id']) {
            $aTeam = Phpfox::getService('teams')->getPage($aRow['profile_page_id']);
            $aRow['user_id'] = $aTeam['user_id'];
        }

        // Send the user an email
        $sLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'],
                $aRow['vanity_url']) . 'wall/comment-id_' . $aRow['feed_comment_id'] . '/';
        $sItemLink = \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);

        Phpfox::getService('comment.process')->notify([
                'user_id' => $aRow['user_id'],
                'item_id' => $aRow['feed_comment_id'],
                'owner_subject' => ['{{ full_name }} commented on a comment posted on the team "{{ title }}".',
                    ['full_name' => Phpfox::getUserBy('full_name'), 'title' => $aRow['title']]],
                'owner_message' => ['{{ full_name }} commented on one of your comments you posted on the team "<a href="{{ item_link }}">{{ title }}</a>". To see the comment thread, follow the link below: <a href="{{ link }}">{{ link }}</a>',
                    [
                        'full_name' => Phpfox::getUserBy('full_name'),
                        'item_link' => $sItemLink,
                        'title' => $aRow['title'],
                        'link' => $sLink
                    ]],
                'owner_notification' => 'comment.add_new_comment',
                'notify_id' => 'teams_comment_feed',
                'mass_id' => 'teams',
                'mass_subject' => (Phpfox::getUserId() == $aRow['user_id'] ? ['{{ full_name }} commented on one of {{ gender }} team comments.',
                    [
                        'full_name' => Phpfox::getUserBy('full_name'),
                        'gender' => Phpfox::getService('user')->gender($aRow['gender'], 1)
                    ]] : ['{{ full_name }} commented on one of {{ other_full_name }}\'s team comments.',
                    ['full_name' => Phpfox::getUserBy('full_name'), 'other_full_name' => $aRow['full_name']]]),
                'mass_message' => (Phpfox::getUserId() == $aRow['user_id'] ? ['{{ full_name }} commented on one of {{ gender }} own comments on the team "<a href="{{ item_link }}">{{ title }}</a>". To see the comment thread, follow the link: <a href="{{ link }}">{{ link }}</a>',
                    [
                        'full_name' => Phpfox::getUserBy('full_name'),
                        'gender' => Phpfox::getService('user')->gender($aRow['gender'], 1),
                        'item_link' => $sItemLink,
                        'title' => $aRow['title'],
                        'link' => $sLink
                    ]] : ['{{ full_name }} commented on one of {{ other_full_name }}\'s comments on the team "<a href="{{ item_link }}">{{ title }}</a>". To see the comment thread, follow the link: <a href="{{ link }}">{{ link }}</a>',
                    [
                        'full_name' => Phpfox::getUserBy('full_name'),
                        'other_full_name' => $aRow['full_name'],
                        'item_link' => $sItemLink,
                        'title' => $aRow['title'],
                        'link' => $sLink
                    ]])
            ]
        );
    }

    public function getNotificationComment($aNotification)
    {
        $aRow = $this->database()->select('fc.feed_comment_id, u.user_id, u.gender, u.user_name, u.full_name, e.page_id, e.title, pu.vanity_url')
            ->from(Phpfox::getT('pages_feed_comment'), 'fc')
            ->join(Phpfox::getT('user'), 'u', 'u.user_id = fc.user_id')
            ->join(Phpfox::getT('pages'), 'e', 'e.page_id = fc.parent_user_id')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = e.page_id')
            ->where('fc.feed_comment_id = ' . (int)$aNotification['item_id'])
            ->execute('getSlaveRow');

        if (!isset($aRow['feed_comment_id'])) {
            return false;
        }

        if ($aNotification['item_user_id'] == $aRow['user_id'] && isset($aNotification['extra_users']) && count($aNotification['extra_users'])) {
            $sUsers = Phpfox::getService('notification')->getUsers($aNotification, true);
        } else {
            $sUsers = Phpfox::getService('notification')->getUsers($aNotification);
        }
        $sTitle = Phpfox::getLib('parse.output')->shorten($aRow['title'],
            Phpfox::getParam('notification.total_notification_title_length'), '...');

        $sPhrase = _p('user_commented_on_the_team_title', ['users' => $sUsers, 'title' => $sTitle]);

        return [
            'link' => \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'],
                    $aRow['vanity_url']) . 'wall/comment-id_' . $aRow['feed_comment_id'] . '/',
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    public function getNotificationComment_Feed($aNotification)
    {
        $aRow = $this->database()->select('fc.feed_comment_id, u.user_id, u.gender, u.user_name, u.full_name, e.page_id, e.title, pu.vanity_url')
            ->from(Phpfox::getT('pages_feed_comment'), 'fc')
            ->join(Phpfox::getT('user'), 'u', 'u.user_id = fc.user_id')
            ->join(Phpfox::getT('pages'), 'e', 'e.page_id = fc.parent_user_id')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = e.page_id')
            ->where('fc.feed_comment_id = ' . (int)$aNotification['item_id'])
            ->execute('getSlaveRow');

        if (!isset($aRow['feed_comment_id'])) {
            return false;
        }

        if ($aNotification['user_id'] == $aRow['user_id'] && isset($aNotification['extra_users']) && count($aNotification['extra_users'])) {
            $sUsers = Phpfox::getService('notification')->getUsers($aNotification, true);
        } else {
            $sUsers = Phpfox::getService('notification')->getUsers($aNotification);
        }
        $sGender = Phpfox::getService('user')->gender($aRow['gender'], 1);
        $sTitle = Phpfox::getLib('parse.output')->shorten($aRow['title'],
            Phpfox::getParam('notification.total_notification_title_length'), '...');

        if ($aNotification['user_id'] == $aRow['user_id']) {
            if (isset($aNotification['extra_users']) && count($aNotification['extra_users'])) {
                $sPhrase = _p('{{ users }} commented on <span class="drop_data_user">{{ full_name }}\'s</span> comment on the team "{{ title }}"',
                    ['users' => $sUsers, 'full_name' => $aRow['full_name'], 'title' => $sTitle]);
            } else {
                $sPhrase = _p('{{ users }} commented on {{ gender }} own comment on the team "{{ title }}"',
                    ['users' => $sUsers, 'gender' => $sGender, 'title' => $sTitle]);
            }
        } elseif ($aRow['user_id'] == Phpfox::getUserId()) {
            $sPhrase = _p('{{ users }} commented on one of your comments on the team "{{ title }}"',
                ['users' => $sUsers, 'title' => $sTitle]);
        } else {
            $sPhrase = _p('{{ users }} commented on one of <span class="drop_data_user">{{ full_name }}\'s</span> comments on the team "{{ title }}"',
                ['users' => $sUsers, 'full_name' => $aRow['full_name'], 'title' => $sTitle]);
        }

        return [
            'link' => \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'],
                    $aRow['vanity_url']) . 'wall/comment-id_' . $aRow['feed_comment_id'] . '/',
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    public function getTotalItemCount($iUserId)
    {
        return [
            'field' => 'total_teams',
            'total' => $this->database()->select('COUNT(*)')
                ->from(Phpfox::getT('pages'), 'p')
                ->where('p.view_id = 0 AND p.user_id = ' . (int)$iUserId . ' AND p.app_id = 0 AND p.item_type = 2')
                ->execute('getSlaveField')
        ];
    }

    public function globalUnionSearch($sSearch)
    {
        if (Phpfox::isAdmin()) {
            $this->database()->select('item.page_id AS item_id, item.title AS item_title, item.time_stamp AS item_time_stamp, item.user_id AS item_user_id, \'teams\' AS item_type_id, item.image_path AS item_photo, item.image_server_id AS item_photo_server')
                ->from(Phpfox::getT('pages'), 'item')
                ->where('item.view_id = 0 AND ' . $this->database()->searchKeywords('item.title',
                        $sSearch) . ' AND item.privacy = 0 AND item.item_type = 2')
                ->union();
        } else {
            $this->database()->select('item.page_id AS item_id, item.title AS item_title, item.time_stamp AS item_time_stamp, item.user_id AS item_user_id, \'teams\' AS item_type_id, item.image_path AS item_photo, item.image_server_id AS item_photo_server')
                ->from(Phpfox::getT('pages'), 'item')
                ->where('item.view_id = 0 AND ' . $this->database()->searchKeywords('item.title',
                        $sSearch) . ' AND item.privacy = 0 AND item.item_type = 2 AND item.reg_method <> 2')
                ->union();
            if (Phpfox::isUser()) {
                $this->database()->select('item.page_id AS item_id, item.title AS item_title, item.time_stamp AS item_time_stamp, item.user_id AS item_user_id, \'teams\' AS item_type_id, item.image_path AS item_photo, item.image_server_id AS item_photo_server')
                    ->from(Phpfox::getT('pages'), 'item')
                    ->join(Phpfox::getT('like'), 'l',
                        'l.type_id = \'teams\' AND l.item_id = item.page_id AND l.user_id = ' . Phpfox::getUserId())
                    ->where('item.view_id = 0 AND ' . $this->database()->searchKeywords('item.title',
                            $sSearch) . ' AND item.privacy = 0 AND item.item_type = 2 AND item.reg_method = 2')
                    ->union();
            }
        }
    }

    public function getSearchInfo($aRow)
    {
        $aPage = $this->database()->select('p.page_id, p.item_type, p.title, pu.vanity_url, ' . Phpfox::getUserField())
            ->from(Phpfox::getT('pages'), 'p')
            ->join(Phpfox::getT('user'), 'u', 'u.profile_page_id = p.page_id')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = p.page_id')
            ->where('p.page_id = ' . (int)$aRow['item_id'])
            ->execute('getSlaveRow');

        $aInfo = [];
        $aInfo['item_link'] = \Phpfox::getService('teams')->getUrl($aPage['page_id'], $aPage['title'],
            $aPage['vanity_url']);
        $aInfo['item_name'] = _p('Teams');
        $aInfo['profile_image'] = $aPage;

        return $aInfo;
    }

    public function getSearchTitleInfo()
    {
        return [
            'name' => _p('Teams')
        ];
    }

    public function getNotificationApproved($aNotification)
    {
        $aRow = $this->database()->select('v.page_id, v.title, v.user_id, u.gender, u.full_name, pu.vanity_url')
            ->from(Phpfox::getT('pages'), 'v')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = v.page_id')
            ->join(Phpfox::getT('user'), 'u', 'u.user_id = v.user_id')
            ->where('v.page_id = ' . (int)$aNotification['item_id'])
            ->execute('getSlaveRow');

        if (!isset($aRow['page_id'])) {
            return false;
        }

        $sPhrase = _p('Your team "{{ title }}" has been approved.', [
            'title' => Phpfox::getLib('parse.output')->shorten($aRow['title'],
                Phpfox::getParam('notification.total_notification_title_length'), '...')
        ]);

        return [
            'link' => \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']),
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog'),
            'no_profile_image' => true
        ];
    }

    public function addLikeComment($iItemId, $bDoNotSendEmail = false)
    {
        $aRow = $this->database()->select('fc.feed_comment_id, fc.content, fc.user_id, e.page_id, e.title, pu.vanity_url, u.profile_page_id')
            ->from(Phpfox::getT('pages_feed_comment'), 'fc')
            ->join(Phpfox::getT('pages'), 'e', 'e.page_id = fc.parent_user_id')
            ->join(Phpfox::getT('user'), 'u', 'u.user_id = fc.user_id')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = e.page_id')
            ->where('fc.feed_comment_id = ' . (int)$iItemId)
            ->execute('getSlaveRow');

        if (!isset($aRow['feed_comment_id'])) {
            return;
        }

        if ($aRow['profile_page_id']) {
            $aTeam = Phpfox::getService('teams')->getPage($aRow['profile_page_id']);
            $aRow['user_id'] = $aTeam['user_id'];
        }

        $this->database()->updateCount('like', 'type_id = \'teams_comment\' AND item_id = ' . (int)$iItemId . '',
            'total_like', 'pages_feed_comment', 'feed_comment_id = ' . (int)$iItemId);

        if (!$bDoNotSendEmail) { // check has liked
            $sLink = Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']) . 'wall/comment-id_' . $aRow['feed_comment_id'] . '/';
            $sItemLink = Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);

            Phpfox::getLib('mail')->to($aRow['user_id'])
                ->subject([
                    'pages.full_name_liked_a_comment_you_made_on_the_page_title',
                    ['full_name' => Phpfox::getUserBy('full_name'), 'title' => $aRow['title']]
                ])
                ->message([
                    'pages.full_name_liked_a_comment_you_made_on_the_page_title_to_view_the_comment_thread_follow_the_link_below_a_href_link_link_a',
                    [
                        'full_name' => Phpfox::getUserBy('full_name'),
                        'link' => $sLink,
                        'item_link' => $sItemLink,
                        'title' => $aRow['title']
                    ]
                ])
                ->notification('like.new_like')
                ->send();

            Phpfox::getService('notification.process')->add('teams_comment_like', $aRow['feed_comment_id'], $aRow['user_id']);
        }
    }

    //It is posting feeds for comments made in a Page of type team set to registration method "invite only", this should not happen.
    public function deleteLikeComment($iItemId)
    {
        $this->database()->updateCount('like', 'type_id = \'teams_comment\' AND item_id = ' . (int)$iItemId . '',
            'total_like', 'pages_feed_comment', 'feed_comment_id = ' . (int)$iItemId);
    }

    public function deleteComment($iId)
    {
        $this->database()->updateCounter('pages_feed_comment', 'total_comment', 'feed_comment_id', $iId, true);
    }

    public function updateCounterList()
    {
        $aList = [];

        $aList[] = [
            'name' => _p('Users Teams Count'),
            'id' => 'teams-total'
        ];

        return $aList;
    }

    public function updateCounter($iId, $iPage, $iPageLimit)
    {
        $iCnt = $this->database()->select('COUNT(*)')
            ->from(Phpfox::getT('user'))
            ->execute('getSlaveField');

        $aRows = $this->database()->select('u.user_id, u.user_name, u.full_name, COUNT(b.page_id) AS total_items')
            ->from(Phpfox::getT('user'), 'u')
            ->leftJoin(Phpfox::getT('pages'), 'b',
                'b.user_id = u.user_id AND b.view_id = 0 AND b.app_id = 0 AND b.item_type = 2')
            ->limit($iPage, $iPageLimit, $iCnt)
            ->team('u.user_id')
            ->execute('getSlaveRows');

        foreach ($aRows as $aRow) {
            $this->database()->update(Phpfox::getT('user_field'), ['total_teams' => $aRow['total_items']],
                'user_id = ' . $aRow['user_id']);
        }

        return $iCnt;
    }

    public function getNotificationComment_Like($aNotification)
    {
        $aRow = $this->database()->select('fc.feed_comment_id, u.user_id, u.gender, u.user_name, u.full_name, e.page_id, e.title, pu.vanity_url')
            ->from(Phpfox::getT('pages_feed_comment'), 'fc')
            ->join(Phpfox::getT('user'), 'u', 'u.user_id = fc.user_id')
            ->join(Phpfox::getT('pages'), 'e', 'e.page_id = fc.parent_user_id')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = e.page_id')
            ->where('fc.feed_comment_id = ' . (int)$aNotification['item_id'])
            ->execute('getSlaveRow');

        $sUsers = Phpfox::getService('notification')->getUsers($aNotification);
        $sTitle = Phpfox::getLib('parse.output')->shorten($aRow['title'],
            Phpfox::getParam('notification.total_notification_title_length'), '...');

        if ($aNotification['user_id'] == $aRow['user_id']) {
            if (isset($aNotification['extra_users']) && count($aNotification['extra_users'])) {
                $sPhrase = _p('{{ users }} liked <span class="drop_data_user">{{ row_full_name }}\'s</span> comment on the team "{{ title }}"',
                    [
                        'users' => Phpfox::getService('notification')->getUsers($aNotification, true),
                        'row_full_name' => $aRow['full_name'],
                        'title' => $sTitle
                    ]);
            } else {
                $sPhrase = _p('{{ users }} liked {{ gender }} own comment on the team "{{ title }}"', [
                    'users' => $sUsers,
                    'gender' => Phpfox::getService('user')->gender($aRow['gender'], 1),
                    'title' => $sTitle
                ]);
            }
        } elseif ($aRow['user_id'] == Phpfox::getUserId()) {
            $sPhrase = _p('{{ users }} liked one of your comments on the team "{{ title }}"',
                ['users' => $sUsers, 'title' => $sTitle]);
        } else {
            $sPhrase = _p('{{ users }} liked one on <span class="drop_data_user">{{ row_full_name }}\'s</span> comments on the team "{{ title }}"',
                ['users' => $sUsers, 'row_full_name' => $aRow['full_name'], 'title' => $sTitle]);
        }

        return [
            'link' => Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']) . 'wall/comment-id_' . $aRow['feed_comment_id'] . '/',
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    /* Used to get a page when there is no certainty of the module */
    public function getItem($iId)
    {
        \Phpfox::getService('teams')->setIsInPage();
        $aItem = $this->database()->select('*')->from(Phpfox::getT('pages'))->where('item_type = 2 AND page_id = ' . (int)$iId)->execute('getSlaveRow');
        if (empty($aItem)) {
            return false;
        }
        $aItem['module'] = 'teams';
        $aItem['module_title'] = _p('Teams');
        $aItem['item_id'] = $iId;

        return $aItem;
    }

    /**
     * @param $iUser
     * @throws \Exception
     */
    public function onDeleteUser($iUser)
    {
        $aRows = $this->database()->select('*')
            ->from(Phpfox::getT('pages'))
            ->where('user_id = ' . (int)$iUser . ' AND item_type = 2')
            ->execute('getSlaveRows');

        foreach ($aRows as $aRow) {
            Phpfox::getService('teams.process')->delete($aRow['page_id'], true, true);
        }
    }

    /**
     * If a call is made to an unknown method attempt to connect
     * it to a specific plug-in with the same name thus allowing
     * plug-in developers the ability to extend classes.
     *
     * @param string $sMethod is the name of the method
     * @param array $aArguments is the array of arguments of being passed
     */
    public function __call($sMethod, $aArguments)
    {
        /**
         * Check if such a plug-in exists and if it does call it.
         */
        if ($sPlugin = Phpfox_Plugin::get('teams.service_callback__call')) {
            eval($sPlugin);

            return;
        }

        /**
         * No method or plug-in found we must throw a error.
         */
        Phpfox_Error::trigger('Call to undefined method ' . __CLASS__ . '::' . $sMethod . '()', E_USER_ERROR);
    }

    public function checkPermission($iId, $sName)
    {
        return \Phpfox::getService('teams')->hasPerm($iId, $sName);
    }

    public function addItemNotification($aParams)
    {
        \Phpfox_Queue::instance()->addJob('teams_member_notifications', $aParams);
    }

    public function getNotificationStatus_NewItem_Teams($aNotification)
    {
        $aItem = $this->database()->select('fc.feed_comment_id, u.user_id, u.gender, u.user_name, u.full_name, e.page_id, e.title, pu.vanity_url')
            ->from(Phpfox::getT('pages_feed_comment'), 'fc')
            ->join(Phpfox::getT('user'), 'u', 'u.user_id = fc.user_id')
            ->join(Phpfox::getT('pages'), 'e', 'e.page_id = fc.parent_user_id')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = e.page_id')
            ->where('fc.feed_comment_id = ' . (int)$aNotification['item_id'])
            ->execute('getSlaveRow');

        if (!isset($aItem['feed_comment_id'])) {
            return false;
        }

        $sPhrase = _p('{{ users }} add a new comment in the team "{{ title }}"', [
            'users' => Phpfox::getService('notification')->getUsers($aNotification),
            'title' => Phpfox::getLib('parse.output')->shorten($aItem['title'],
                Phpfox::getParam('notification.total_notification_title_length'), '...')
        ]);

        return [
            'link' => \Phpfox::getService('teams')->getUrl($aItem['page_id'], $aItem['title'],
                    $aItem['vanity_url']) . 'wall/comment-id_' . $aItem['feed_comment_id'] . '/',
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];

    }

    public function getNotificationConverted($aNotification)
    {
        return [
            'link' => Phpfox::getLib('url')->makeUrl('teams'),
            'message' => _p("All old teams (page type) converted new teams"),
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];

    }

    public function canShareOnMainFeed($iPageId, $sPerm, $bChildren)
    {
        return \Phpfox::getService('teams')->hasPerm($iPageId, $sPerm);
    }

    public function getExtraBrowseConditions($sPrefix = 'pages')
    {
        $sCondition = " AND ({$sPrefix}.user_id = " . Phpfox::getUserId() . " OR {$sPrefix}.reg_method <> 2";
        if (Phpfox::getUserParam('core.can_view_private_items')) {
            $sCondition .= " OR {$sPrefix}.reg_method = 2";
        } else {
            $aTeamIds = Phpfox::getService('teams')->getAllSecretTeamIdsOfMember();
            if (count($aTeamIds)) {
                $sCondition .= " OR {$sPrefix}.page_id IN (" . implode(',', $aTeamIds) . ")";
            }
        }
        $sCondition .= ') ';

        return $sCondition;
    }

    public function getReportRedirect($iId)
    {
        return Phpfox::getService('teams')->getUrl($iId);
    }

    /**
     * @description: callback to check permission to get feed of a team
     * @param $iId
     *
     * @return bool
     */
    public function canGetFeeds($iId)
    {
        $aTeam = \Phpfox::getService('teams')->getPage($iId);
        if (!$aTeam || empty($aTeam['page_id'])) {
            return false;
        }

        //return false if user isn't admin/member want to get  feed of a closed/secret team
        if (!\Phpfox::getService('teams')->isAdmin($aTeam['page_id']) && !\Phpfox::getService('teams')->isMember($aTeam['page_id']) && in_array($aTeam['reg_method'],
                [1, 2])) {
            return false;
        }

        return \Phpfox::getService('teams')->hasPerm($aTeam['page_id'], 'teams.view_browse_updates');
    }

    /**
     * @description: return callback param for adding feed comment on team
     * @param $iId
     * @param $aVals
     *
     * @return array|bool
     */
    public function getFeedComment($iId, $aVals)
    {
        //check permission
        Phpfox::isUser(true);

        if (!\Phpfox::getService('teams')->hasPerm($iId, 'teams.share_updates')) {
            return false;
        }

        if (\Phpfox::getLib('parse.format')->isEmpty($aVals['user_status'])) {
            Phpfox_Error::set(_p('add_some_text_to_share'));

            return false;
        }

        $aTeam = \Phpfox::getService('teams')->getPage($iId);

        //check team is exists
        if (!isset($aTeam['page_id'])) {
            Phpfox_Error::set(_p('Unable to find the page you are trying to comment on.'));

            return false;
        }

        $sLink = \Phpfox::getService('teams')->getUrl($aTeam['page_id'], $aTeam['title'], $aTeam['vanity_url']);
        $aCallback = [
            'module' => 'teams',
            'table_prefix' => 'pages_',
            'link' => $sLink,
            'email_user_id' => $aTeam['user_id'],
            'subject' => ['{{ full_name }} wrote a comment on your team "{{ title }}".',
                ['full_name' => Phpfox::getUserBy('full_name'), 'title' => $aTeam['title']]],
            'message' => ['{{ full_name }} wrote a comment on your team "<a href="{{ link }}">{{ title }}</a>". To see the comment thread, follow the link below: <a href="{{ link }}">{{ link }}</a>',
                ['full_name' => Phpfox::getUserBy('full_name'), 'link' => $sLink, 'title' => $aTeam['title']]],
            'notification' => null,
            'feed_id' => 'teams_comment',
            'item_id' => $aTeam['page_id'],
            'add_to_main_feed' => true,
            'add_tag' => true
        ];

        return $aCallback;
    }

    /**
     * @description: callback after a comment feed added on event
     * @param $iId
     */
    public function onAddFeedCommentAfter($iId)
    {
        \Phpfox_Database::instance()->updateCounter('pages', 'total_comment', 'page_id', $iId);
    }

    /**
     * @description: check permission when add like for team
     * @param $iId
     *
     * @return bool
     */
    public function canLikeItem($iId)
    {
        $aItem = \Phpfox::getService('teams')->getForView($iId);
        if (empty($aItem) || empty($aItem['page_id'])) {
            return false;
        }

        $bIsAdmin = Phpfox::getService('teams')->isAdmin($iId) || Phpfox::isAdmin();
        if (!$bIsAdmin && ($aItem['reg_method'] == 2 || $aItem['reg_method'] == 1)) {
			
            return \Phpfox::getService('teams')->checkCurrentUserInvited($iId);
        }

        return true;
    }
	
	public function canFanLikeItem($iId)
    {
        $aItem = \Phpfox::getService('teams')->getForView($iId);
        if (empty($aItem) || empty($aItem['page_id'])) {
            return false;
        }

        $bIsAdmin = Phpfox::getService('teams')->isAdmin($iId) || Phpfox::isAdmin();
        if (!$bIsAdmin && ($aItem['reg_method'] == 2 || $aItem['reg_method'] == 1)) {
			
          //  return \Phpfox::getService('teams')->checkCurrentUserInvited($iId);
        }

        return true;
    }

    public function getNotificationInvite_Admin($aNoti)
    {
        $aRow = $this->database()->select('v.page_id, v.title, v.user_id, u.gender, u.full_name, pu.vanity_url')
            ->from(Phpfox::getT('pages'), 'v')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = v.page_id')
            ->join(Phpfox::getT('user'), 'u', 'u.user_id = v.user_id')
            ->where('v.page_id = ' . (int)$aNoti['item_id'])
            ->execute('getSlaveRow');

        if (!isset($aRow['page_id'])) {
            return false;
        }

        $sPhrase = _p('you_have_been_invited_to_become_an_admin_of_team', ['page_name' => $aRow['title']]);

        return [
            'link' => Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']),
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    /**
     * Check if need to hide profile photos or cover photos
     * @param $iTeamId
     * @return array
     */
    public function getHiddenAlbums($iTeamId)
    {
        $aHiddenAlbums = [];
        if (!Phpfox::getParam('teams.display_teams_profile_photo_within_gallery', true)) {
            list($iCnt, $aProfileAlbums) = Phpfox::getService('photo.album')->get([
                'pa.module_id = \'teams\'',
                'AND pa.team_id = ' . $iTeamId,
                'AND pa.profile_id != 0'
            ]);
            $iCnt && ($aHiddenAlbums[] = $aProfileAlbums[0]['album_id']);
        }
        if (!Phpfox::getParam('teams.display_teams_cover_photo_within_gallery', true)) {
            list($iCnt, $aCoverAlbums) = Phpfox::getService('photo.album')->get([
                'pa.module_id = \'teams\'',
                'AND pa.team_id = ' . $iTeamId,
                'AND pa.cover_id != 0'
            ]);
            $iCnt && ($aHiddenAlbums[] = $aCoverAlbums[0]['album_id']);
        }

        return $aHiddenAlbums;
    }

    /**
     * This function will add number of pending teams to admin dashboard statistics
     * @return array
     */
    public function pendingApproval()
    {
        return [
            'phrase' => _p('Teams'),
            'value' => Phpfox::getService('teams')->getPendingTotal(),
            'link' => Phpfox_Url::instance()->makeUrl('teams', ['view' => 'pending'])
        ];
    }

    public function getAdmincpAlertItems()
    {
        $iTotalPending = Phpfox::getService('teams')->getPendingTotal();
        return [
            'message' => _p('you_have_total_pending_teams', ['total' => $iTotalPending]),
            'value' => $iTotalPending,
            'link' => Phpfox_Url::instance()->makeUrl('teams', ['view' => 'pending'])
        ];
    }

    /**
     * Check if user is admin of team
     * @param $iTeamId
     * @return bool
     * @throws \Exception
     */
    public function isAdmin($iTeamId)
    {
        $aErrors = Phpfox_Error::get();
        $bIsAdmin = Phpfox::getService('teams')->isAdmin($iTeamId);
        Phpfox_Error::reset();
        foreach ($aErrors as $sError) {
            Phpfox_Error::set($sError);
        }

        return $bIsAdmin;
    }

    /**
     * Show notification when someone post an image on team,
     * notifications will be sent to team's owner and admins
     *
     * @param $aNotification
     * @return array|bool
     */
    public function getNotificationPost_Image($aNotification)
    {
        // get pages from photo id
        $aPhoto = Phpfox::getService('photo')->getPhotoItem($aNotification['item_id']);
        if (!$aPhoto) {
            return false;
        }

        $aTeam = Phpfox::getService('teams')->getPage($aPhoto['team_id']);
        if (!$aTeam) {
            return false;
        }

        if (!empty($aTeam['user_id']) && $aTeam['user_id'] == Phpfox::getUserId()) {
            // notification of owner
            $sPhrase = _p('full_name_post_some_images_on_your_team_title', [
                'full_name' => Phpfox::getService('notification')->getUsers($aNotification),
                'title' => $aTeam['title']
            ]);
        } else {
            // notification of admin
            $sPhrase = _p('full_name_post_some_images_on_team_title', [
                'full_name' => Phpfox::getService('notification')->getUsers($aNotification),
                'title' => $aTeam['title']
            ]);
        }

        return [
            'link' => Phpfox::getService('photo.callback')->getLink(['item_id' => $aPhoto['photo_id']]),
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    /**
     * Show notification when someone post a link on team,
     * notifications will be sent to team's owner and admins
     *
     * @param $aNotification
     * @return array|bool
     */
    public function getNotificationComment_Link($aNotification)
    {
        $aLink = Phpfox::getService('link')->getLinkById($aNotification['item_id']);
        if (!$aLink) {
            return false;
        }

        $aTeam = Phpfox::getService('teams')->getPage($aLink['item_id']);
        if (!$aTeam) {
            return false;
        }

        if (!empty($aTeam['user_id']) && $aTeam['user_id'] == Phpfox::getUserId()) {
            // notification of owner
            $sPhrase = _p('full_name_posted_a_link_on_your_team_title', [
                'full_name' => Phpfox::getService('notification')->getUsers($aNotification),
                'title' => $aTeam['title']
            ]);
        } else {
            // notification of admin
            $sPhrase = _p('full_name_posted_a_link_on_team_title', [
                'full_name' => Phpfox::getService('notification')->getUsers($aNotification),
                'title' => $aTeam['title']
            ]);
        }

        return [
            'link' => $aLink['redirect_link'],
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    /**
     * Get notification of posted video
     *
     * @param $aNotification
     * @return array|bool
     */
    public function getNotificationPosted_Video($aNotification)
    {
        if (!Phpfox::isAppActive('PHPfox_Videos')) {
            return false;
        }
        $aVideo = Phpfox::getService('v.video')->getForEdit($aNotification['item_id']);
        if (!$aVideo) {
            return false;
        }

        $aTeam = Phpfox::getService('teams')->getPage($aVideo['item_id']);
        if (!$aTeam) {
            return false;
        }

        if (!empty($aTeam['user_id']) && $aTeam['user_id'] == Phpfox::getUserId()) {
            // notification of owner
            $sPhrase = _p('full_name_posted_a_video_on_your_team_title', [
                'full_name' => Phpfox::getService('notification')->getUsers($aNotification),
                'title' => $aTeam['title']
            ]);
        } else {
            // notification of admin
            $sPhrase = _p('full_name_posted_a_video_on_team_title', [
                'full_name' => Phpfox::getService('notification')->getUsers($aNotification),
                'title' => $aTeam['title']
            ]);
        }

        return [
            'link' => Phpfox::permalink('video.play', $aVideo['video_id'], $aVideo['title']),
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    public function onVideoPublished($aVideo)
    {
        if ($aVideo && isset($aVideo['item_id'])) {
            $aTeam = Phpfox::getService('teams')->getPage($aVideo['item_id']);
            if (!$aTeam) {
                return true;
            }
            $bForce = false;
            if (isset($aVideo['view_id'])) { // Approve called
                $bForce = true;
            }
            $sLink = Phpfox::getService('teams')->getUrl($aTeam['page_id'], $aTeam['title'], $aTeam['vanity_url']);
            $postedUser = Phpfox::getService('user')->getUser($aVideo['user_id'], 'u.full_name');
            $postedUserFullName = $postedUser['full_name'];

            $varPhraseTitle = 'full_name_posted_a_video_on_team_title';
            $varPhraseLink = 'full_name_posted_a_video_on_team_link';

            // get all admins (include owner) and send notification
            $aAdmins = Phpfox::getService('teams')->getPageAdmins($aTeam['page_id']);
            foreach ($aAdmins as $aAdmin) {
                if ($aVideo['user_id'] == $aAdmin['user_id']) { // is owner of video
                    continue;
                }

                if ($aTeam['user_id'] == $aAdmin['user_id']) { // is owner of team
                    $varPhraseTitle = 'full_name_posted_a_video_on_your_team_title';
                    $varPhraseLink = 'full_name_posted_a_video_on_your_team_link';
                }

                Phpfox::getLib('mail')->to($aAdmin['user_id'])
                    ->subject([$varPhraseTitle, [
                        'full_name' => $postedUserFullName,
                        'title' => $aTeam['title']
                    ]])
                    ->message([$varPhraseLink, [
                        'full_name' => $postedUserFullName,
                        'link' => $sLink,
                        'title' => $aTeam['title']
                    ]])
                    ->notification('comment.add_new_comment')
                    ->send();

                if (Phpfox::isModule('notification')) {
                    Phpfox::getService('notification.process')->add('teams_posted_video', $aVideo['video_id'], $aAdmin['user_id'], $aVideo['user_id'], $bForce);
                }
            }
        }
    }

    public function getSiteStatsForAdmins()
    {
        $iToday = mktime(0, 0, 0, date('m'), date('d'), date('Y'));

        return [
            'phrase' => _p('teams'),
            'value' => $this->database()->select('COUNT(*)')
                ->from(':pages')
                ->where('view_id = 0 AND item_type = 2 AND time_stamp >= ' . $iToday)
                ->executeField()
        ];
    }

    /**
     * @return array
     */
    public function getUploadParams()
    {
        return Phpfox::getService('teams')->getUploadPhotoParams();
    }

    public function getActivityFeedPhoto($aItem, $aCallback = null, $bIsChildItem = false)
    {
        $sSelect = 'p.*';
        if (Phpfox::isModule('like')) {
            $sSelect .= ', count(l.like_id) as total_like';
            $this->database()->leftJoin(Phpfox::getT('like'), 'l',
                'l.type_id = \'photo\' AND l.item_id = p.photo_id');

            $this->database()->team('p.photo_id');

            $sSelect .= ', l2.like_id AS is_liked';
            $this->database()->leftJoin(Phpfox::getT('like'), 'l2',
                'l2.type_id = \'photo\' AND l2.item_id = p.photo_id AND l2.user_id = ' . Phpfox::getUserId());
        }
        $aRow = $this->database()->select($sSelect . ' , p.destination, u.server_id, u.full_name, u.profile_page_id')
            ->from(Phpfox::getT('photo'), 'p')
            ->join(':user', 'u', 'u.user_id=p.user_id')
            ->where([
                'p.photo_id' => (int)$aItem['item_id'],
                'p.is_profile_photo' => 1
            ])->execute('getSlaveRow');

        if (empty($aRow)) {
            return false;
        }

        if ((defined('PHPFOX_IS_PAGES_VIEW') && !\Phpfox::getService('teams')->hasPerm(null, 'teams.view_browse_updates'))
            || (!defined('PHPFOX_IS_PAGES_VIEW') && !\Phpfox::getService('teams')->hasPerm($aRow['team_id'], 'teams.view_browse_updates'))
        ) {
            return false;
        }

        $aTeam = Phpfox::getService('teams')->getPage($aRow['team_id']);
        if ($aTeam['reg_method'] == 2 && (!\Phpfox::getService('teams')->isMember($aTeam['page_id']) &&
                !\Phpfox::getService('teams')->isAdmin($aTeam['page_id']) && Phpfox::getService('user')->isAdminUser(Phpfox::getUserId()))
        ) {
            return false;
        }

        $sImage = Phpfox::getLib('image.helper')->display([
            'server_id' => $aRow['server_id'],
            'path' => 'photo.url_photo',
            'file' => $aRow['destination'],
            'suffix' => '_500',
            'class' => 'photo_holder',
            'defer' => true
        ]);
        $aReturn = [
            'feed_title' => '',
            'feed_info' => _p('updated_their_profile_photo'),
            'feed_link' => Phpfox_Url::instance()->permalink('photo', $aRow['photo_id'], $aRow['title']),
            'feed_image' => $sImage,
            'feed_icon' => Phpfox::getLib('image.helper')->display([
                'theme' => 'misc/report_user.png',
                'return_url' => true
            ]),
            'time_stamp' => $aItem['time_stamp'],
            'feed_total_like' => $aRow['total_like'],
            'like_type_id' => 'photo',
            'enable_like' => true,
            'feed_is_liked' => isset($aRow['is_liked']) ? $aRow['is_liked'] : false,
            'total_comment' => $aRow['total_comment'],
            'comment_type_id' => 'photo',
            'parent_user_id' => Phpfox::getService('teams')->getPageOwnerId($aRow['profile_page_id']),
            'item_type' => 2
        ];

        if ($bIsChildItem) {
            $aReturn = array_merge($aReturn, $aItem);
        }

        return $aReturn;
    }

    public function getActivityFeedCover_Photo($aItem, $aCallback = null, $bIsChildItem = false)
    {
        $sSelect = 'p.*';
        if (Phpfox::isModule('like')) {
            $sSelect .= ', count(l.like_id) as total_like';
            $this->database()->leftJoin(Phpfox::getT('like'), 'l',
                'l.type_id = \'photo\' AND l.item_id = p.photo_id');

            $this->database()->team('p.photo_id');

            $sSelect .= ', l2.like_id AS is_liked';
            $this->database()->leftJoin(Phpfox::getT('like'), 'l2',
                'l2.type_id = \'photo\' AND l2.item_id = p.photo_id AND l2.user_id = ' . Phpfox::getUserId());
        }
        $aRow = $this->database()->select($sSelect . ' , p.destination, p.server_id, u.full_name, u.profile_page_id')
            ->from(Phpfox::getT('photo'), 'p')
            ->join(':user', 'u', 'u.user_id=p.user_id')
            ->where([
                'p.photo_id' => (int)$aItem['item_id']
            ])->execute('getSlaveRow');

        if (empty($aRow)) {
            return false;
        }

        $aTeam = Phpfox::getService('teams')->getPage($aRow['team_id']);
        if ((defined('PHPFOX_IS_PAGES_VIEW') && !\Phpfox::getService('teams')->hasPerm(null, 'teams.view_browse_updates'))
            || (!defined('PHPFOX_IS_PAGES_VIEW') && !\Phpfox::getService('teams')->hasPerm($aTeam['page_id'], 'teams.view_browse_updates'))
        ) {
            return false;
        }

        if (!empty($aTeam['reg_method']) && $aTeam['reg_method'] == 2 && (!\Phpfox::getService('teams')->isMember($aTeam['page_id']) &&
                !\Phpfox::getService('teams')->isAdmin($aTeam['page_id']) && Phpfox::getService('user')->isAdminUser(Phpfox::getUserId()))
        ) {
            return false;
        }

        $sImage = Phpfox::getLib('image.helper')->display([
            'server_id' => $aRow['server_id'],
            'path' => 'photo.url_photo',
            'file' => $aRow['destination'],
            'suffix' => '_500',
            'class' => 'photo_holder',
            'defer' => true
        ]);
        $aReturn = [
            'feed_title' => '',
            'feed_info' => _p('updated_their_cover_photo'),
            'feed_link' => Phpfox_Url::instance()->permalink('photo', $aRow['photo_id'], $aRow['title']),
            'feed_image' => $sImage,
            'feed_icon' => Phpfox::getLib('image.helper')->display([
                'theme' => 'misc/report_user.png',
                'return_url' => true
            ]),
            'time_stamp' => $aItem['time_stamp'],
            'feed_total_like' => $aRow['total_like'],
            'like_type_id' => 'photo',
            'enable_like' => true,
            'feed_is_liked' => isset($aRow['is_liked']) ? $aRow['is_liked'] : false,
            'total_comment' => $aRow['total_comment'],
            'comment_type_id' => 'photo',
            'parent_user_id' => Phpfox::getService('teams')->getPageOwnerId($aRow['profile_page_id']),
            'item_type' => 2
        ];

        if ($bIsChildItem) {
            $aReturn = array_merge($aReturn, $aItem);
        }

        return $aReturn;
    }

    /**
     * Get statistic for each user
     *
     * @param $iUserId
     * @return array|bool
     */
    public function getUserStatsForAdmin($iUserId)
    {
        if (!$iUserId) {
            return false;
        }

        $iTotalPages = db()->select('COUNT(*)')->from(':pages')->where(['user_id' => $iUserId, 'item_type' => 2])->executeField();

        return [
            'total_name' => _p('teams'),
            'total_value' => $iTotalPages,
            'type' => 'item'
        ];
    }

    public function showCoverInDetailItem($iTeamId)
    {
        $aTeam = Phpfox::getService('teams')->getForView($iTeamId);
        Phpfox_Component::setPublicParam('show_team_cover', true);
        Phpfox_Component::setPublicParam('team_to_show_cover', $aTeam);
    }

    /**
     * @param $aParams
     * @return bool
     */
    public function enableSponsor($aParams)
    {
        return Phpfox::getService('teams.process')->sponsor($aParams['item_id'], 1);
    }

    public function getToSponsorInfo($iId)
    {
        $aPage = db()->select('p.user_id, p.title, p.page_id as item_id, p.image_server_id as server_id, p.image_path as image, u.user_name')
            ->from(':pages', 'p')
            ->join(Phpfox::getT('user'), 'u', 'u.user_id = p.user_id')
            ->where('p.page_id = ' . (int)$iId)
            ->execute('getSlaveRow');

        if (empty($aPage)) {
            return [
                'error' => _p('sorry_the_team_you_are_looking_for_no_longer_exists',
                    ['link' => Phpfox::getLib('url')->makeUrl('teams')])
            ];
        }

        $aPage['link'] = Phpfox::permalink('teams', $aPage['item_id'], $aPage['title']);
        $aPage['paypal_msg'] = _p('sponsor_paypal_message_team', ['sTeamTitle' => $aPage['title']]);
        $aPage['image_dir'] = 'pages.url_image';
        $aPage['title'] = _p('sponsor_title_team', ['sTeamTitle' => $aPage['title']]);

        $aPage = array_merge($aPage, [
            'redirect_completed' => 'teams',
            'message_completed' => _p('purchase_team_sponsor_completed'),
            'redirect_pending_approval' => 'teams',
            'message_pending_approval' => _p('purchase_team_sponsor_pending_approval')
        ]);
        return $aPage;
    }

    /**
     * This callback will be called when admin delete a sponsor in admincp
     * @param $aParams
     */
    public function deleteSponsorItem($aParams)
    {
        db()->update(':pages', ['is_sponsor' => 0], ['page_id' => $aParams['item_id']]);
        $this->cache()->remove('teams_sponsored');
    }

    /**
     * @param $aParams
     * @return bool|string
     */
    public function getLink($aParams)
    {
        $aPage = db()->select('p.title, p.page_id, pu.vanity_url')
            ->from(Phpfox::getT('pages'), 'p')
            ->join(Phpfox::getT('user'), 'u', 'u.user_id = p.user_id')
            ->leftJoin(':pages_url', 'pu', 'pu.page_id = p.page_id')
            ->where('p.page_id = ' . (int)$aParams['item_id'])
            ->execute('getSlaveRow');
        if (empty($aPage)) {
            return false;
        }
        return Phpfox::getService('teams')->getUrl($aPage['page_id'], $aPage['title'], $aPage['vanity_url']);
    }

    public function getNotificationReassign_Owner($aNotification)
    {
        $aRow = \Phpfox::getService('teams')->getPage($aNotification['item_id']);

        if (!isset($aRow['page_id'])) {
            return false;
        }

        $sPhrase = _p('full_name_just_assigned_you_as_owner_of_team_title', [
            'full_name' => Phpfox::getService('notification')->getUsers($aNotification),
            'title' => \Phpfox::getLib('parse.output')->shorten($aRow['title'],
                \Phpfox::getParam('notification.total_notification_title_length'), '...')
        ]);

        return [
            'link' => \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']),
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

    public function getNotificationOwner_Changed($aNotification)
    {
        $aRow = \Phpfox::getService('teams')->getPage($aNotification['item_id']);

        if (!isset($aRow['page_id'])) {
            return false;
        }

        $sPhrase = _p('full_name_just_transfer_your_team_title_to_other_user', [
            'full_name' => Phpfox::getService('notification')->getUsers($aNotification),
            'title' => \Phpfox::getLib('parse.output')->shorten($aRow['title'],
                \Phpfox::getParam('notification.total_notification_title_length'), '...')
        ]);

        return [
            'link' => \Phpfox::getService('teams')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']),
            'message' => $sPhrase,
            'icon' => Phpfox_Template::instance()->getStyle('image', 'activity.png', 'blog')
        ];
    }

}
