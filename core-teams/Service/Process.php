<?php

namespace Apps\PHPfox_Teams\Service;

use Core;
use Phpfox;
use Phpfox_Error;
use Phpfox_File;
use Phpfox_Pages_Process;
use Phpfox_Plugin;
use Phpfox_Request;
use Phpfox_Image;

/**
 * Class Browse
 *
 * @package Apps\PHPfox_Teams\Service
 */
class Process extends Phpfox_Pages_Process
{
    public function updateUserImageAndPhotoProfileForProcessCrop($iTeamId, $sPath = null)
    {
        if (empty($sPath)) {
            return false;
        }

        $oFile = Phpfox_File::instance();
        $oImage = Phpfox_Image::instance();

        $iTeamUserId = Phpfox::getService('teams')->getUserId($iTeamId);

        $sUserImage = $this->database()->select('user_image')
            ->from(Phpfox::getT('user'))
            ->where('user_id = ' . (int)$iTeamUserId)
            ->execute('getSlaveField');

        if (!empty($sUserImage)) {
            if (file_exists(Phpfox::getParam('core.dir_user') . sprintf($sUserImage, ''))) {
                $oFile->unlink(Phpfox::getParam('core.dir_user') . sprintf($sUserImage, ''));
                foreach (Phpfox::getService('user')->getUserThumbnailSizes() as $iSize) {
                    if (file_exists(Phpfox::getParam('core.dir_user') . sprintf($sUserImage, '_' . $iSize))) {
                        $oFile->unlink(Phpfox::getParam('core.dir_user') . sprintf($sUserImage, '_' . $iSize));
                    }

                    if (file_exists(Phpfox::getParam('core.dir_user') . sprintf($sUserImage, '_' . $iSize . '_square'))) {
                        $oFile->unlink(Phpfox::getParam('core.dir_user') . sprintf($sUserImage, '_' . $iSize . '_square'));
                    }
                }
            }
        }

        $sFileName = md5($iTeamUserId . PHPFOX_TIME . uniqid()) . '%s.' . substr($sPath, -3);
        $sTo = Phpfox::getParam('core.dir_user') . sprintf($sFileName, '');

        if (file_exists($sTo)) {
            Phpfox::getService('user.space')->update(Phpfox::getUserId(), 'photo', filesize($sTo), '-');
            $oFile->unlink($sTo);
        }

        $mReturn = Phpfox_Request::instance()->send($sPath, [], 'GET');
        $hFile = @fopen($sTo, 'w');
        @fwrite($hFile, $mReturn);
        @fclose($hFile);

        if (filesize($sTo) > 0) {
            $bReturn = Phpfox::getLib('cdn')->put($sTo);
        } else {
            $oFile->unlink($sTo);
            $oFile->copy($sPath, $sTo);
            $bReturn = Phpfox::getLib('cdn')->put($sTo);
        }

        $sTo = Phpfox::getParam('core.dir_user') . sprintf($sFileName, '');

        if (file_exists($sTo)) {
            Phpfox::getService('user.space')->update(Phpfox::getUserId(), 'photo', filesize($sTo));
        }


        $iServerId = Phpfox_Request::instance()->getServer('PHPFOX_SERVER_ID');

        foreach (Phpfox::getService('user')->getUserThumbnailSizes() as $iSize) {
            if (Phpfox::getParam('core.keep_non_square_images')) {
                $oImage->createThumbnail(Phpfox::getParam('core.dir_user') . sprintf($sFileName, ''), Phpfox::getParam('core.dir_user') . sprintf($sFileName, '_' . $iSize), $iSize, $iSize);
            }

            $oImage->createThumbnail(Phpfox::getParam('core.dir_user') . sprintf($sFileName, ''), Phpfox::getParam('core.dir_user') . sprintf($sFileName, '_' . $iSize . '_square'), $iSize, $iSize, false);
        }
        if (Phpfox::isAppActive('Core_Photos')) {
            $iMaxWidth = (int)Phpfox::getUserParam('photo.maximum_image_width_keeps_in_server');
            list($width, $height, $type, $attr) = getimagesize(Phpfox::getParam('core.dir_user') . sprintf($sFileName, ''));
            if ($iMaxWidth < $width) {
                $oImage->createThumbnail(Phpfox::getParam('core.dir_user') . sprintf($sFileName, ''), Phpfox::getParam('core.dir_user') . sprintf($sFileName, ''), $iMaxWidth, $height);
            }
        }
        $this->database()->update(Phpfox::getT('user'), ['user_image' => $sFileName, 'server_id' => $iServerId], 'user_id = ' . (int)$iTeamUserId);

        if (Phpfox::isAppActive('Core_Photos')) {
            $aProfileImage = db()->select('photo_id, destination')
                ->from(Phpfox::getT('photo'))
                ->where('module_id = "teams" AND team_id = ' . (int)$iTeamId . ' AND is_cover = 1 AND is_profile_photo = 1')
                ->execute('getSlaveRow');
            if (!empty($aProfileImage)) {
                $sExtension = pathinfo($sTo, PATHINFO_EXTENSION);
                $sNewPhotoPath = md5($aProfileImage['photo_id']) . '%s.' . $sExtension;

                $oFile->unlink(Phpfox::getParam('photo.dir_photo') . sprintf($aProfileImage['destination'], ''));
                @copy($sTo, Phpfox::getParam('photo.dir_photo') . sprintf($sNewPhotoPath, ''));

                //push to cdn
                Phpfox::getLib('cdn')->put(Phpfox::getParam('photo.dir_photo') . sprintf($sNewPhotoPath, ''));

                foreach (Phpfox::getService('photo')->getPhotoPicSizes() as $iSize) {
                    $oFile->unlink(Phpfox::getParam('photo.dir_photo') . sprintf($aProfileImage['destination'], '_' . $iSize));
                    $oImage->createThumbnail(Phpfox::getParam('photo.dir_photo') . sprintf($sNewPhotoPath, ''),
                        Phpfox::getParam('photo.dir_photo') . sprintf($sNewPhotoPath, '_' . $iSize), $iSize, $iSize, true,
                        false);
                }
                db()->update(Phpfox::getT('photo'), ['destination' => $sNewPhotoPath],
                    'photo_id = ' . (int)$aProfileImage['photo_id']);
            }
        }
    }

    public function addWidget($aVals, $iEditId = null)
    {
        $aPage = $this->getFacade()->getItems()->getPage($aVals['page_id']);

        if (!isset($aPage['page_id'])) {
            return Phpfox_Error::set($this->getFacade()->getPhrase('unable_to_find_the_page_you_are_looking_for'));
        }

        $bCanModerate = $this->getFacade()->getUserParam('can_moderate_pages');
        if ($bCanModerate === null) {
            $bCanModerate = $this->getFacade()->getUserParam('can_approve_pages') || $this->getFacade()->getUserParam('can_edit_all_pages') || $this->getFacade()->getUserParam('can_delete_all_pages');
        }

        if (!$this->getFacade()->getItems()->isAdmin($aPage) && !$bCanModerate) {
            return Phpfox_Error::set($this->getFacade()->getPhrase('unable_to_add_a_widget_to_this_page'));
        }

        if (empty($aVals['title'])) {
            Phpfox_Error::set($this->getFacade()->getPhrase('provide_a_title_for_your_widget'));
        }

        // parse content, remove script
        $aVals['text'] = preg_replace('/<script.*<\/script>/', '', $aVals['text']);

        if (empty($aVals['text'])) {
            Phpfox_Error::set($this->getFacade()->getPhrase('provide_content_for_your_widget'));
        }

        if (!$aVals['is_block']) {
            if (empty($aVals['menu_title'])) {
                Phpfox_Error::set($this->getFacade()->getPhrase('provide_a_menu_title_for_your_widget'));
            }

            if (empty($aVals['url_title'])) {
                Phpfox_Error::set($this->getFacade()->getPhrase('provide_a_url_title_for_your_widget'));
            }
        }

        if (Phpfox::isModule($aVals['url_title'])) {
            Phpfox_Error::set($this->getFacade()->getPhrase('you_cannot_use_this_url_for_your_widget'));
        }

        if (!Phpfox_Error::isPassed()) {
            return false;
        }

        $oFilter = Phpfox::getLib('parse.input');

        if ($iEditId !== null) {
            $sNewTitle = $this->database()->select('url_title')
                ->from(Phpfox::getT('pages_widget'))
                ->where('widget_id = ' . (int)$iEditId)
                ->execute('getSlaveField');
        }

        if (!$aVals['is_block'] && ($iEditId !== null && ($sNewTitle != $aVals['url_title']))) {
            $sNewTitle = Phpfox::getLib('parse.input')->prepareTitle('pages', $aVals['url_title'], 'url_title',
                Phpfox::getUserId(), Phpfox::getT('pages_widget'),
                'page_id = ' . (int)$aPage['page_id'] . ' AND url_title LIKE \'%' . $aVals['url_title'] . '%\'');
        }

        //Check duplicate widget title_url
        if (!$aVals['is_block']) {
            if ($iEditId) {
                $sMoreConds = ' AND widget_id !=' . (int)$iEditId;
            } else {
                $sMoreConds = '';
            }
            $iCnt = $this->database()->select('COUNT(*)')
                ->from(':pages_widget')
                ->where('page_id=' . (int)$aPage['page_id'] . ' AND url_title="' . $aVals['url_title'] . '"' . $sMoreConds)
                ->executeField();

            if ($iCnt) {
                return Phpfox_Error::set(_p("the_url_title_exists"));
            }
        }
        $aSql = [
            'page_id' => $aPage['page_id'],
            'title' => $aVals['title'],
            'is_block' => (int)$aVals['is_block'],
            'menu_title' => ($aVals['is_block'] ? null : $aVals['menu_title']),
            'url_title' => ($aVals['is_block'] ? null : (isset($sNewTitle) ? $sNewTitle : $aVals['url_title']))
        ];

        if ($iEditId === null) {
            $aSql['time_stamp'] = PHPFOX_TIME;
            $aSql['user_id'] = Phpfox::getUserId();

            $iId = $this->database()->insert(Phpfox::getT('pages_widget'), $aSql);

            $this->database()->insert(Phpfox::getT('pages_widget_text'), [
                    'widget_id' => $iId,
                    'text' => $oFilter->clean($aVals['text']),
                    'text_parsed' => $oFilter->prepare($aVals['text'])
                ]
            );
        } else {
            $this->database()->update(Phpfox::getT('pages_widget'), $aSql, 'widget_id = ' . (int)$iEditId);
            $this->database()->update(Phpfox::getT('pages_widget_text'), [
                'text' => $oFilter->clean($aVals['text']),
                'text_parsed' => $oFilter->prepare($aVals['text'])
            ], 'widget_id = ' . (int)$iEditId
            );

            $iId = $iEditId;
        }

        return $iId;
    }


    public function updateProfilePictureForThumbnail($iPage, $sNewPath)
    {
        db()->update($this->_sTable, ['image_path' => $sNewPath], 'page_id = ' . $iPage);
    }

    public function getFacade()
    {
        return Phpfox::getService('teams.facade');
    }

    /**
     * Add new team
     * @param $aVals
     * @param bool $bIsApp
     * @return int
     * @throws \Exception
     */
	public function checkUniqueTeam($aTitle)
	{
		$aTitle = $this->database()->select('title')
                    ->from(Phpfox::getT('pages'))
                    ->where('title = "'.$aTitle.'"')
                    ->execute('getSlaveField');
		if(!empty($aTitle))
		{
			return true;
		}
		
		return false;
					
	}
    public function add($aVals, $bIsApp = false)
    {
		
		if ($this->checkUniqueTeam($aVals['title']))
		{
            Phpfox_Error::set(_p('Team Name already Taken.'));
			return false;
        }

        $iViewId = ($this->getFacade()->getUserParam('approve_pages') ? '1' : '0');

        // Flood control
        if ($iWaitTime = Phpfox::getUserParam('teams.flood_control')) {
            $aFlood = [
                'action' => 'last_post', // The SPAM action
                'params' => [
                    'field' => 'time_stamp', // The time stamp field
                    'table' => Phpfox::getT('pages'), // Database table we plan to check
                    'condition' => 'item_type = 2 AND user_id = ' . Phpfox::getUserId(), // Database WHERE query
                    'time_stamp' => $iWaitTime * 60 // Seconds);
                ]
            ];

            // actually check if flooding
            if (Phpfox::getLib('spam')->check($aFlood)) {
                return Phpfox_Error::set(Phpfox::getLib('spam')->getWaitTime());
            }
        }

        if (empty($aVals['title'])) {
            return Phpfox_Error::set($this->getFacade()->getPhrase('page_name_cannot_be_empty'));
        }

        if (defined('PHPFOX_APP_CREATED') || $bIsApp) {
            $iViewId = 0;
        }

        if ($sPlugin = Phpfox_Plugin::get($this->getFacade()->getItemType() . '.service_process_add_1')) {
            eval($sPlugin);
        }

        $aInsert = [
            'view_id' => $iViewId,
            'type_id' => (isset($aVals['type_id']) ? (int)$aVals['type_id'] : 0),
            'app_id' => (isset($aVals['app_id']) ? (int)$aVals['app_id'] : 0),
            'category_id' => (isset($aVals['category_id']) ? (int)$aVals['category_id'] : 0),
            'user_id' => Phpfox::getUserId(),
            'title' => $this->preParse()->clean($aVals['title'], 255),
            'time_stamp' => PHPFOX_TIME,
            'item_type' => $this->getFacade()->getItemTypeId(),
            'reg_method' => isset($aVals['reg_method']) ? $aVals['reg_method'] : 0,
            'team_type' => isset($aVals['team_type']) ? $aVals['team_type'] : 0,
            'team_region' => $aVals['team_region']
        ];

        $iId = $this->database()->insert($this->_sTable, $aInsert);

		$aInsertText = ['page_id' => $iId];
        if (isset($aVals['info'])) {
            $aInsertText['text'] = $this->preParse()->clean($aVals['info']);
            $aInsertText['text_parsed'] = $this->preParse()->prepare($aVals['info']);
        }
        $this->database()->insert(Phpfox::getT('pages_text'), $aInsertText);

        $sSalt = '';
        for ($i = 0; $i < 3; $i++) {
            $sSalt .= chr(rand(33, 91));
        }

        $sPossible = '23456789bcdfghjkmnpqrstvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
        $sPassword = '';
        $i = 0;
        while ($i < 10) {
            $sPassword .= substr($sPossible, mt_rand(0, strlen($sPossible) - 1), 1);
            $i++;
        }

        $iUserId = $this->database()->insert(Phpfox::getT('user'), [
                'profile_page_id' => $iId,
                'user_group_id' => NORMAL_USER_ID,
                'view_id' => '7',
                'full_name' => $this->preParse()->clean($aVals['title']),
                'joined' => PHPFOX_TIME,
                'password' => Phpfox::getLib('hash')->setHash($sPassword, $sSalt),
                'password_salt' => $sSalt
            ]
        );

        $aExtras = [
            'user_id' => $iUserId
        ];

        $this->database()->insert(Phpfox::getT('user_activity'), $aExtras);
        $this->database()->insert(Phpfox::getT('user_field'), $aExtras);
        $this->database()->insert(Phpfox::getT('user_space'), $aExtras);
        $this->database()->insert(Phpfox::getT('user_count'), $aExtras);
        $this->setDefaultPermissions($iId);

        if (!$this->getFacade()->getUserParam('approve_pages')) {
            Phpfox::getService('user.activity')->update(Phpfox::getUserId(), $this->getFacade()->getItemType());
        }

        Phpfox::getService('like.process')->add('teams', $iId, null, null);

		/*Update URL*/
		$sNewTitle = Phpfox::getLib('parse.input')->cleanTitle($aVals['title']);
		$sNewTitle = $sNewTitle.'-'.$iId;
		$aTitle = $this->database()->select('*')
            ->from(Phpfox::getT('pages_url'))
            ->where('page_id = ' . (int)$iId)
            ->execute('getSlaveRow');

        if (isset($aTitle['vanity_url'])) {
            $this->database()->update(Phpfox::getT('pages_url'), array('vanity_url' => $sNewTitle),
                'page_id = ' . (int)$iId);
        } else {
            $this->database()->insert(Phpfox::getT('pages_url'),
                array('vanity_url' => $sNewTitle, 'page_id' => (int)$iId));
        }

        $this->database()->update(Phpfox::getT('user'), array('user_name' => $sNewTitle),
            'profile_page_id = ' . (int)$iId);
			
        return $iId;
    }

    /**
     * Update team
     * @param $iId
     * @param $aVals
     * @param $aPage
     * @return bool
     * @throws \Exception
     */
    public function update($iId, $aVals, $aPage)
    {
        if (!$this->_verify($aVals)) {
            return false;
        }
        if ($sPlugin = Phpfox_Plugin::get($this->getFacade()->getItemType() . '.service_process_update_0')) {
            eval($sPlugin);
            if (isset($mReturnFromPlugin)) {
                return $mReturnFromPlugin;
            }
        }

        $aUser = $this->database()->select('user_id')
            ->from(Phpfox::getT('user'))
            ->where('profile_page_id = ' . (int)$iId)
            ->execute('getSlaveRow');

        $aUpdate = [
            'type_id' => (isset($aVals['type_id']) ? (int)$aVals['type_id'] : '0'),
            'category_id' => (isset($aVals['category_id']) ? (int)$aVals['category_id'] : 0),
            'reg_method' => (isset($aVals['reg_method']) ? (int)$aVals['reg_method'] : 0),
            'privacy' => (isset($aVals['privacy']) ? (int)$aVals['privacy'] : 0)
        ];

        /* Only store the location if the admin has set a google key or ipinfodb key. This input is not always available */
        if (Phpfox::getParam('core.google_api_key') && isset($aVals['location'])) {
            if (isset($aVals['location']['name'])) {
                $aUpdate['location_name'] = $this->preParse()->clean($aVals['location']['name']);
            }
            if (isset($aVals['location']['latlng']) && $aVals['location']['latlng'] != '-43.132123,9.140625') {
                $aMatch = explode(',', $aVals['location']['latlng']);
                if (isset($aMatch[1])) {
                    $aUpdate['location_latitude'] = $aMatch[0];
                    $aUpdate['location_longitude'] = $aMatch[1];
                }
            }
        }

        if (isset($aVals['landing_page'])) {
            $aUpdate['landing_page'] = $aVals['landing_page'];
        }
        if (!empty($aVals['title'])) {
            $aUpdate['title'] = $this->preParse()->clean($aVals['title']);
        }

        // remove old image
        if (!empty($aPage['image_path']) && (!empty($aVals['temp_file']) || !empty($aVals['remove_photo'])) && $this->deleteImage($aPage)) {
            $aUpdate['image_path'] = null;
            $aUpdate['image_server_id'] = 0;
            if (!empty($aVals['remove_photo'])) {
                $oFile = Phpfox_File::instance();
                $iTeamUserId = Phpfox::getService('teams')->getUserId($iId);
                $sUserImage = $this->database()->select('user_image')
                    ->from(Phpfox::getT('user'))
                    ->where('user_id = ' . (int)$iTeamUserId)
                    ->execute('getSlaveField');
                if (!empty($sUserImage)) {
                    if (file_exists(Phpfox::getParam('core.dir_user') . sprintf($sUserImage, ''))) {
                        $oFile->unlink(Phpfox::getParam('core.dir_user') . sprintf($sUserImage, ''));
                        foreach (Phpfox::getService('user')->getUserThumbnailSizes() as $iSize) {
                            if (file_exists(Phpfox::getParam('core.dir_user') . sprintf($sUserImage, '_' . $iSize))) {
                                $oFile->unlink(Phpfox::getParam('core.dir_user') . sprintf($sUserImage, '_' . $iSize));
                            }

                            if (file_exists(Phpfox::getParam('core.dir_user') . sprintf($sUserImage, '_' . $iSize . '_square'))) {
                                $oFile->unlink(Phpfox::getParam('core.dir_user') . sprintf($sUserImage, '_' . $iSize . '_square'));
                            }
                        }
                        $this->database()->update(Phpfox::getT('user'), ['user_image' => null, 'server_id' => 0], 'user_id = ' . (int)$iTeamUserId);
                    }
                }
                storage()->del('user/avatar/' . $iTeamUserId);
            }
        }

        if (!empty($aVals['temp_file'])) {
            // get image from temp file
            $aFile = Phpfox::getService('core.temp-file')->get($aVals['temp_file']);
            if (!empty($aFile)) {
                if (!Phpfox::getService('user.space')->isAllowedToUpload($aPage['user_id'], $aFile['size'])) {
                    Phpfox::getService('core.temp-file')->delete($aVals['temp_file'], true);

                    return false;
                }
                $aUpdate['image_path'] = $aFile['path'];
                $aUpdate['image_server_id'] = $aFile['server_id'];
                $aUpdate['item_type'] = $this->getFacade()->getItemTypeId();
                Phpfox::getService('user.space')->update($aPage['user_id'], 'teams', $aFile['size']);
                Phpfox::getService('core.temp-file')->delete($aVals['temp_file']);
            }
            // change profile image of page
            define('PHPFOX_PAGES_IS_IN_UPDATE', true);
            $iServerId = Phpfox_Request::instance()->getServer('PHPFOX_SERVER_ID');
            $sPath = Phpfox::getParam('pages.dir_image') . sprintf($aFile['path'], '');

            if (!empty($iServerId)) {
                $sFileUrl = Phpfox::getLib('cdn')->getUrl(str_replace(PHPFOX_DIR, '', $sPath), $iServerId);
                //Download to local server to process
                file_put_contents($sPath, fox_get_contents($sFileUrl));
                //Remove this temp file after process end
                register_shutdown_function(function () use ($sPath) {
                    @unlink($sPath);
                });
            }

            Phpfox::getService('user.process')->uploadImage($aUser['user_id'], true, $sPath);

            // add feed after updating page's profile image
            $iTeamUserId = Phpfox::getService('teams')->getUserId($iId);
            if (Phpfox::isModule('feed') && $oProfileImage = storage()->get('user/avatar/' . $iTeamUserId, null)) {
                Phpfox::getService('feed.process')->callback([
                    'table_prefix' => 'pages_',
                    'module' => 'teams',
                    'add_to_main_feed' => true,
                    'has_content' => true
                ])->add('teams_photo', $oProfileImage->value, 0, 0, $iId, $iTeamUserId);
            }
        }

        $this->database()->update($this->_sTable, $aUpdate, 'page_id = ' . (int)$iId);

        $this->database()->update(Phpfox::getT('pages_text'), [
            'text' => $this->preParse()->clean($aVals['text']),
            'text_parsed' => $this->preParse()->prepare($aVals["text"])
        ], 'page_id = ' . (int)$iId);

        if ($sPlugin = Phpfox_Plugin::get($this->getFacade()->getItemType() . '.service_process_update_1')) {
            eval($sPlugin);
            if (isset($mReturnFromPlugin)) {
                return $mReturnFromPlugin;
            }
        }

        // Invite to page
        if ((isset($aVals['invite']) && is_array($aVals['invite'])) || (isset($aVals['emails']) && $aVals['emails'])) {
            // get invited friends, emails
            $aInvites = $this->database()->select('invited_user_id, invited_email')
                ->from(Phpfox::getT('pages_invite'))
                ->where('page_id = ' . (int)$iId)
                ->execute('getSlaveRows');
            $aInvited = [];
            foreach ($aInvites as $aInvite) {
                $aInvited[(empty($aInvite['invited_email']) ? 'user' : 'email')][(empty($aInvite['invited_email']) ? $aInvite['invited_user_id'] : $aInvite['invited_email'])] = true;
            }

            // invite friends
            if (isset($aVals['invite']) && is_array($aVals['invite'])) {
                $sUserIds = '';
                foreach ($aVals['invite'] as $iUserId) {
                    if (!is_numeric($iUserId)) {
                        continue;
                    }
                    $sUserIds .= $iUserId . ',';
                }
                $sUserIds = rtrim($sUserIds, ',');

                $aUsers = $this->database()->select('user_id, email, language_id, full_name')
                    ->from(Phpfox::getT('user'))
                    ->where('user_id IN(' . $sUserIds . ')')
                    ->execute('getSlaveRows');

                $sLink = $this->getFacade()->getItems()->getUrl($aPage['page_id'], $aPage['title'],
                    $aPage['vanity_url']);

                list(, $aMembers) = $this->getFacade()->getItems()->getMembers($aPage['page_id']);

                foreach ($aUsers as $aUser) {
                    if (in_array($aUser['user_id'], array_column($aMembers, 'user_id'))) {
                        continue;
                    }

                    if (isset($aCachedEmails[$aUser['email']])) {
                        continue;
                    }

                    if (isset($aInvited['user'][$aUser['user_id']])) {
                        continue;
                    }

                    $sMessage = $this->getFacade()->getPhrase('full_name_invited_you_to_the_page_title', [
                        'full_name' => Phpfox::getUserBy('full_name'),
                        'title' => $aPage['title']
                    ], $aUser['language_id']);
                    $sMessage .= "\n" . $this->getFacade()->getPhrase('to_view_this_page_click_the_link_below_a_href_link_link_a',
                            ['link' => $sLink], $aUser['language_id']) . "\n";

                    // add personal message
                    if (!empty($aVals['personal_message'])) {
                        $sMessage .= _p('full_name_added_the_following_personal_message',
                                ['full_name' => Phpfox::getUserBy('full_name')], $aUser['language_id'])
                            . $aVals['personal_message'];
                    }
                    // send email to user
                    Phpfox::getLib('mail')->to($aUser['user_id'])
                        ->subject($this->getFacade()->getPhrase('full_name_sent_you_a_page_invitation',
                            ['full_name' => Phpfox::getUserBy('full_name')], $aUser['language_id']))
                        ->message($sMessage)
                        ->translated()
                        ->send();

                    $aCachedEmails[$aUser['email']] = true;

                    // add to table pages_invite
                    $this->database()->insert(Phpfox::getT('pages_invite'), [
                            'page_id' => $iId,
                            'type_id' => $this->getFacade()->getItemTypeId(),
                            'user_id' => Phpfox::getUserId(),
                            'invited_user_id' => $aUser['user_id'],
                            'time_stamp' => PHPFOX_TIME
                        ]
                    );
                    // send notification
                    (Phpfox::isModule('request') ? Phpfox::getService('request.process')->add($this->getFacade()->getItemType() . '_invite',
                        $iId, $aUser['user_id']) : null);
                }
            }

            // invite emails
            if (isset($aVals['emails']) && $aVals['emails']) {
                $aEmails = explode(',', $aVals['emails']);
                foreach ($aEmails as $sEmail) {
                    $sEmail = trim($sEmail);
                    if (!Phpfox::getLib('mail')->checkEmail($sEmail)) {
                        continue;
                    }

                    if (isset($aCachedEmails[$sEmail])) {
                        continue;
                    }

                    if (isset($aInvited['email'][$sEmail])) {
                        continue;
                    }

                    $sLink = $this->getFacade()->getItems()->getUrl($iId, $aPage['title'], $aPage['vanity_url']) . '?code=' . base64_encode($sEmail);

                    $sMessage = _p('full_name_invited_you_to_the_team_title_link_check_out', [
                        'full_name' => Phpfox::getUserBy('full_name'),
                        'title' => $aPage['title'],
                        'link' => $sLink
                    ]);
                    if (!empty($aVals['personal_message'])) {
                        $sMessage .= _p('full_name_added_the_following_personal_message',
                                ['full_name' => Phpfox::getUserBy('full_name')])
                            . $aVals['personal_message'];
                    }
                    $oMail = Phpfox::getLib('mail');
                    if (isset($aVals['invite_from']) && $aVals['invite_from'] == 1) {
                        $oMail->fromEmail(Phpfox::getUserBy('email'))
                            ->fromName(Phpfox::getUserBy('full_name'));
                    }
                    $bSent = $oMail->to($sEmail)
                        ->subject(_p('full_name_invited_you_to_the_team_title',
                            [
                                'full_name' => Phpfox::getUserBy('full_name'),
                                'title' => $aPage['title']
                            ]
                        ))
                        ->message($sMessage)
                        ->translated()
                        ->send();
                    if ($bSent) {
                        // cache email for not duplicate invite.
                        $aCachedEmails[$sEmail] = true;

                        $this->database()->insert(Phpfox::getT('pages_invite'), [
                                'page_id' => $iId,
                                'type_id' => $this->getFacade()->getItemTypeId(),
                                'user_id' => Phpfox::getUserId(),
                                'invited_email' => $sEmail,
                                'time_stamp' => PHPFOX_TIME
                            ]
                        );
                    }
                }
            }
            // notification message
            Phpfox::addMessage($this->getFacade()->getPhrase('invitations_sent_out'));
        }

        $aUserCache = [];
        // get old admins
        $aOldAdmins = Phpfox::getService('teams')->getPageAdmins($iId);
        $this->database()->delete(Phpfox::getT('pages_admin'), 'page_id = ' . (int)$iId);
        $aAdmins = Phpfox_Request::instance()->getArray('admins');
        if (count($aAdmins)) {
            foreach ($aAdmins as $iAdmin) {
                if (isset($aUserCache[$iAdmin])) {
                    continue;
                }

                $aUserCache[$iAdmin] = true;
                //Add to member first
                $sType = $this->getFacade()->getItemType();

                //Check is liked
                if (!Phpfox::getService('teams')->isMember($iId, $iAdmin)) {
                    db()->insert(':like', [
                        'type_id' => 'teams',
                        'item_id' => (int)$iId,
                        'user_id' => $iAdmin,
                        'time_stamp' => PHPFOX_TIME
                    ]);
                    $this->database()->updateCount('like', 'type_id = \'teams\' AND item_id = ' . (int)$iId . '', 'total_like', 'pages', 'page_id = ' . (int)$iId);
                    $this->cache()->remove('teams_' . (int)$iId . '_members');
                }
                // Notify to new admin for the first time
                if (!in_array($iAdmin, array_column($aOldAdmins, 'user_id'))) {
                    Phpfox::getService('notification.process')->add($this->getFacade()->getItemType() . '_invite_admin', $iId, $iAdmin);
                }
                //Then add to admin
                $this->database()->insert(Phpfox::getT('pages_admin'), ['page_id' => $iId, 'user_id' => $iAdmin]);
            }
        }
        $this->cache()->remove('teams_' . $iId . '_admins');

        if (isset($aVals['perms'])) {
            $this->database()->delete(Phpfox::getT('pages_perm'), 'page_id = ' . (int)$iId);
            foreach ($aVals['perms'] as $sPermId => $iPermValue) {
                $this->database()->insert(Phpfox::getT('pages_perm'),
                    ['page_id' => (int)$iId, 'var_name' => $sPermId, 'var_value' => (int)$iPermValue]);
            }
        }

        $this->database()->update(Phpfox::getT('user'),
            ['full_name' => Phpfox::getLib('parse.input')->clean($aVals['title'], 255)],
            'profile_page_id = ' . (int)$iId);

        return true;
    }

    /**
     * Verify params on update team
     * @param $aVals
     * @return bool
     * @throws \Exception
     */
    private function _verify($aVals)
    {
        $bValid = true;
        if (isset($_FILES['image']['name']) && ($_FILES['image']['name'] != '')) {
            $aImage = Phpfox_File::instance()->load('image', ['jpg', 'gif', 'png'],
                ($this->getFacade()->getUserParam('max_upload_size_pages') == 0 ? null : ($this->getFacade()->getUserParam('max_upload_size_pages') / 1024)));

            if ($aImage === false) {
                $bValid = false;
            }

            $this->_bHasImage = true;
        }

        if (empty($aVals['title'])) {
            Phpfox_Error::set(_p('team_name_is_empty'));
            $bValid = false;
        }

        return $bValid;
    }

    /**
     * Remove admin
     * @param $iTeamId
     * @param $iAdminId
     */
    public function removeAdmin($iTeamId, $iAdminId)
    {
        db()->delete(':pages_admin', ['page_id' => $iTeamId, 'user_id' => $iAdminId]);
        $this->cache()->remove('teams_' . $iTeamId . '_admins');
    }

    /**
     * @param $iId
     * @return bool
     */
	
	public function addFan($sType, $iItemId, $iUserId = null, $app_id = null, $params = [], $sTablePrefix = '', $iReactId = null, $bIsReReact = false)
    {
        $bIsNotNull = false;
        if ($iUserId === null) {
            $iUserId = Phpfox::getUserId();
            $bIsNotNull = true;
        }
        if ($sType == 'pages') {
            $bIsNotNull = false;
        }

        // check if iUserId can Like this item
        $aFeed = $this->database()->select('*')
            ->from(Phpfox::getT($sTablePrefix . 'feed'))
            ->where(($app_id === null ? 'item_id = ' . (int)$iItemId . ' AND type_id = \'' . Phpfox::getLib('parse.input')->clean($sType) . '\'' : 'feed_id = ' . (int)$iItemId))
            ->execute('getSlaveRow');

        if (!empty($aFeed) && isset($aFeed['privacy']) && !empty($aFeed['privacy']) && !empty($aFeed['user_id']) && $aFeed['user_id'] != $iUserId) {
            if (Phpfox::getService('user.block')->isBlocked($iUserId, $aFeed['user_id'])) {
                return Phpfox_Error::display(_p('you_are_not_allowed_to_like_this_item'));
            }
            if ($aFeed['privacy'] == 1 && Phpfox::isModule('friend') && Phpfox::getService('friend')->isFriend($iUserId, $aFeed['user_id']) != true) {
                return Phpfox_Error::display(_p('you_are_not_allowed_to_like_this_item'));
            } else if ($aFeed['privacy'] == 2 && Phpfox::isModule('friend') && Phpfox::getService('friend')->isFriendOfFriend($iUserId) != true) {
                return Phpfox_Error::display(_p('you_are_not_allowed_to_like_this_item'));
            } else if ($aFeed['privacy'] == 3 && $aFeed['user_id'] != $iUserId && !(Phpfox::isModule('feed') && Phpfox::getService('feed')->checkTaggedUser($iItemId, $sType, $iUserId))) {
                return Phpfox_Error::display(_p('you_are_not_allowed_to_like_this_item'));
            } else if ($aFeed['privacy'] == 4 && ($bCheck = Phpfox::getService('privacy')->check($sType, $iItemId, $aFeed['user_id'], $aFeed['privacy'], null, true)) != true) {
                return Phpfox_Error::display(_p('you_are_not_allowed_to_like_this_item'));
            }
        }

        $iCheck = $this->database()->select('COUNT(*)')
            ->from(Phpfox::getT('like'))
            ->where('type_id = \'' . $this->database()->escape($sType) . '\' AND item_id = ' . (int)$iItemId . ' AND user_id = ' . $iUserId)
            ->execute('getSlaveField');

        if ($iCheck) {
            if (Phpfox::isAppActive('P_Reaction') && $bIsReReact) {
                $iCheckReacted = db()->select('COUNT(*)')
                    ->from(Phpfox::getT('like'))
                    ->where('type_id = \'' . db()->escape($sType) . '\' AND item_id = ' . (int)$iItemId . ' AND user_id = ' . $iUserId . ' AND react_id = ' . (int)$iReactId)
                    ->execute('getSlaveField');
                if ($iCheckReacted) {
                    return Phpfox_Error::set(_p('you_have_already_reacted_this_feed'));
                }
                $this->delete($sType, $iItemId, $iUserId, false, $sTablePrefix);
            } else {
                return Phpfox_Error::set(_p('you_have_already_reacted_this_feed'));
            }
        }

        //check permission when like an item
        if (empty($params['ignoreCheckPermission']) && Phpfox::isModule($sType) && Phpfox::hasCallback($sType, 'canFanLikeItem') && !Phpfox::callback($sType . '.canFanLikeItem', $iItemId)) {
            return Phpfox_Error::set(_p('you_are_not_allowed_to_like_this_item'));
        }

        $iCnt = (int)$this->database()->select('COUNT(*)')
            ->from(Phpfox::getT('like_cache'))
            ->where('type_id = \'' . $this->database()->escape($sType) . '\' AND item_id = ' . (int)$iItemId . ' AND user_id = ' . (int)$iUserId)
            ->execute('getSlaveField');

        $data = [
            'type_id' => $sType,
            'item_id' => (int)$iItemId,
            'user_id' => $iUserId,
			'is_fan' => 1,
            'time_stamp' => PHPFOX_TIME
        ];

        if ($sPlugin = Phpfox_Plugin::get('like.service_process_add_start')) {
            eval($sPlugin);
        }

        if (Phpfox::isAppActive('P_Reaction') && $iReactId) {
            $data['react_id'] = (int)$iReactId;
        }

        if ($sType == 'app') {
            $data['feed_table'] = $sTablePrefix . 'feed';
        }
        $this->database()->insert(Phpfox::getT('like'), $data);
        //Update time_update of feed when like
        if (Phpfox::getParam('feed.top_stories_update') != 'comment') {
            $this->database()->update(Phpfox::getT($sTablePrefix . 'feed'), [
                'time_update' => PHPFOX_TIME
            ], [
                    'item_id' => (int)$iItemId,
                    'type_id' => $sType
                ]
            );

            if (!empty($sTablePrefix)) {
                $this->database()->update(Phpfox::getT('feed'), [
                    'time_update' => PHPFOX_TIME
                ], [
                        'item_id' => (int)$iItemId,
                        'type_id' => $sType
                    ]
                );
            }
        }
        if (!$iCnt) {
            $this->database()->insert(Phpfox::getT('like_cache'), [
                    'type_id' => $sType,
                    'item_id' => (int)$iItemId,
                    'user_id' => $iUserId
                ]
            );
        }

        (Phpfox::isModule('feed') ? Phpfox::getService('feed.process')->clearCache($sType, $iItemId) : null);

        if ($sPlugin = Phpfox_Plugin::get('like.service_process_add__1')) {
            eval($sPlugin);
        }

        if ($sType == 'app') {
            $app = app($app_id);
            if (isset($app->notifications) && isset($app->notifications->{'__like'})) {
                notify($app->id, '__like', $iItemId, $aFeed['user_id'], false);
            }
            return true;
        }
        if (Phpfox::hasCallback($sType, 'addFanLike')) {
            Phpfox::callback($sType . '.addFanLike', $iItemId, ($iCnt ? true : false), ($bIsNotNull ? null : $iUserId));
        }

        return true;
    }
	
    public function register($iId)
    {
		
        $aPage = $this->database()->select('*')
            ->from(Phpfox::getT('pages'))
            ->where('page_id = ' . (int)$iId)
            ->execute('getSlaveRow');
			
		$aTeamMaxPlayer = Phpfox::getService('teams.type')->CheckMaxPlayer($aPage['type_id']);
		
		list($iCnt, $aMembers) = Phpfox::getService('teams')->getMembers($iId, 4);

		if($aTeamMaxPlayer == $iCnt)
		{
			return Phpfox_Error::set('Max Player Limit Reached.');
		}
        if (!isset($aPage['page_id'])) {
            return false;
        }

        $iSignupId = $this->database()->insert(Phpfox::getT('pages_signup'), [
                'page_id' => $iId,
                'user_id' => Phpfox::getUserId(),
                'time_stamp' => PHPFOX_TIME
            ]
        );

        if (Phpfox::isModule('notification')) {
            Phpfox::getService('notification.process')->add($this->getFacade()->getItemType() . '_register', $iSignupId, $aPage['user_id']);

            $aAdmins = $this->database()->select('*')
                ->from(Phpfox::getT('pages_admin'))
                ->where('page_id = ' . (int)$aPage['page_id'])
                ->execute('getSlaveRows');
            foreach ($aAdmins as $aAdmin) {
                if ($aAdmin['user_id'] == $aPage['user_id']) {
                    continue;
                }

                Phpfox::getService('notification.process')->add($this->getFacade()->getItemType() . '_register', $iSignupId, $aAdmin['user_id']);
            }
        }
        $this->cache()->remove('teams_' . $iId . '_pending_users');
        return true;
    }

    /**
     * @param $iId
     * @return bool
     */
    public function deleteRegister($iId)
    {
        $aPage = $this->database()->select('*')
            ->from(Phpfox::getT('pages'))
            ->where('page_id = ' . (int)$iId)
            ->execute('getSlaveRow');

        if (!isset($aPage['page_id'])) {
            return false;
        }

        $this->database()->delete(Phpfox::getT('pages_signup'), [
                'page_id' => $iId,
                'user_id' => Phpfox::getUserId()
            ]
        );

        $this->cache()->remove('teams_' . $iId . '_pending_users');
        return true;
    }

    /**
     * Delete category
     * @param $iId
     * @param bool $bIsSub
     * @param bool $bDeleteChildren
     * @return bool
     * @throws \Exception
     */
    public function deleteCategory($iId, $bIsSub = false, $bDeleteChildren = false)
    {
        if ($bIsSub) {
            if ($bDeleteChildren) {
                // delete all teams belong to this category
                $aTeams = Phpfox::getService('teams')->getItemsByCategory($iId, true, 1, 0, false, 'delete');
                foreach ($aTeams as $aTeam) {
                    Phpfox::getService('teams.process')->delete($aTeam['page_id']);
                }
            }

            // Delete phrase of category
            $aCategory = $this->database()->select('*')
                ->from(':pages_category')
                ->where('category_id=' . (int)$iId)
                ->execute('getSlaveRow');

            if (isset($aCategory['name']) && Core\Lib::phrase()->isPhrase($aCategory['name'])) {
                Phpfox::getService('language.phrase.process')->delete($aCategory['name'], true);
            }
            $this->database()->delete(Phpfox::getT('pages_category'), 'category_id = ' . (int)$iId);
        } else {
            if ($bDeleteChildren) {
                // delete all teams belong to this type
                $aTeams = Phpfox::getService('teams')->getItemsByCategory($iId, false, 1, 0, false, 'delete');
                foreach ($aTeams as $aTeam) {
                    Phpfox::getService('teams.process')->delete($aTeam['page_id']);
                }
                // delete all categories belong to this type
                $aCategories = $this->database()->select('category_id')
                    ->from(':pages_category')
                    ->where('type_id=' . (int)$iId)
                    ->executeRows();
                foreach ($aCategories as $aCategory) {
                    $this->deleteCategory($aCategory['category_id'], true, $bDeleteChildren);
                }
            }

            // delete category image
            $this->getFacade()->getType()->deleteImage((int)$iId);

            // Delete phrase of type
            $aType = $this->database()->select('*')
                ->from(':pages_type')
                ->where('type_id=' . (int)$iId)
                ->execute('getSlaveRow');

            if (isset($aType['name']) && Core\Lib::phrase()->isPhrase($aType['name'])) {
                Phpfox::getService('language.phrase.process')->delete($aType['name'], true);
            }

            $this->database()->delete(Phpfox::getT('pages_type'), 'type_id = ' . (int)$iId);
        }

        //$this->cache()->removeTeam('teams');

        return true;
    }

    /**
     * Delete profile image
     * @param $aTeam
     * @return bool
     */
    public function deleteImage($aTeam)
    {
        if (!$aTeam['image_path']) {
            return true;
        }

        $aParams = Phpfox::getService('teams')->getUploadPhotoParams();
        $aParams['type'] = 'pages';
        $aParams['path'] = $aTeam['image_path'];
        $aParams['user_id'] = $aTeam['user_id'];
        $aParams['update_space'] = true;
        $aParams['server_id'] = $aTeam['image_server_id'];

        return Phpfox::getService('user.file')->remove($aParams);
    }

    /**
     * @param $iPageId
     * @param $iPhotoId
     * @param bool $bIsAjaxPageUpload
     * @return bool
     * @throws \Exception
     */
    public function setCoverPhoto($iPageId, $iPhotoId, $bIsAjaxPageUpload = false)
    {
        if (!$this->getFacade()->getItems()->isAdmin($iPageId) && !Phpfox::isAdmin() && !$this->getFacade()->getUserParam('can_edit_all_pages')) {
            return Phpfox_Error::set($this->getFacade()->getPhrase('user_is_not_an_admin'));
        }

        if ($bIsAjaxPageUpload == false) {
            // check that this photo belongs to this page
            $iPhotoId = $this->database()->select('photo_id')
                ->from(Phpfox::getT('photo'))
                ->where('module_id = \'' . $this->getFacade()->getItemType() . '\' AND team_id = ' . (int)$iPageId . ' AND photo_id = ' . (int)$iPhotoId)
                ->execute('getSlaveField');
        }

        if (!empty($iPhotoId)) {
            if ($bIsAjaxPageUpload == false) {
                $iPhotoId = $this->processSetCoverPhoto($iPageId, $iPhotoId);
                if ($iPhotoId === false) {
                    return _p('Cannot set cover photo for this team.');
                }
            }
            $this->database()->update(Phpfox::getT('pages'),
                ['cover_photo_position' => '', 'cover_photo_id' => (int)$iPhotoId], 'page_id = ' . (int)$iPageId);
            // create feed after changing cover
            if (Phpfox::isModule('feed')) {
                Phpfox::getService('feed.process')->callback([
                    'table_prefix' => 'pages_',
                    'module' => 'teams',
                    'add_to_main_feed' => true,
                    'has_content' => true
                ])->add('teams_cover_photo', $iPhotoId, 0, 0, $iPageId, Phpfox::getService('teams')->getUserId($iPageId));

            }
            return true;
        }

        return Phpfox_Error::set($this->getFacade()->getPhrase('the_photo_does_not_belong_to_this_page'));
    }

    /**
     * Clone photo for team's cover photo
     * @param $pageId
     * @param $photoId
     * @return bool
     */
    private function processSetCoverPhoto($pageId, $photoId)
    {
        $photoInfo = Phpfox::getService('photo')->getPhotoItem($photoId);
        $photoPath = Phpfox::getParam('photo.dir_photo') . sprintf($photoInfo['destination'], '');
        if (!file_exists($photoPath) && $photoInfo['server_id'] > 0) {
            $photoPath = Phpfox::getLib('image.helper')->display([
                'server_id' => $photoInfo['server_id'],
                'path' => 'photo.url_photo',
                'file' => $photoInfo['destination'],
                'suffix' => '',
                'return_url' => true
            ]);
        }

        $tempFilePath = Phpfox::getParam('core.dir_cache') . md5($photoId . PHPFOX_TIME . uniqid()) . '.' . $photoInfo['extension'];
        file_put_contents($tempFilePath, file_get_contents($photoPath));
        register_shutdown_function(function () use ($tempFilePath) {
            @unlink($tempFilePath);
        });

        if (!file_exists($tempFilePath)) {
            return false;
        }

        $pageUserId = $this->getFacade()->getItems()->getUserId($pageId);
        $insert = [
            'description' => null,
            'type_id' => 0,
            'module_id' => 'teams',
            'team_id' => $pageId,
            'is_cover_photo' => 1,
            'name' => $photoInfo['title'],
            'ext' => $photoInfo['extension'],
            'size' => $photoInfo['file_size'],
            'type' => $photoInfo['mime_type'],
        ];
        $photoId = Phpfox::getService('photo.process')->add($pageUserId, $insert);
        if ($photoId) {
            $sFileName = Phpfox::getLib('file')->upload($tempFilePath, Phpfox::getParam('photo.dir_photo'), $photoId, true);
            $file = Phpfox::getParam('photo.dir_photo') . sprintf($sFileName, '');
            // Get the current image width/height
            $aSize = getimagesize($file);

            $update = [
                'destination' => $sFileName,
                'width' => $aSize[0],
                'height' => $aSize[1],
                'server_id' => \Phpfox_Request::instance()->getServer('PHPFOX_SERVER_ID'),
                'allow_download' => 1,
                'view_id' => 0,
            ];
            Phpfox::getService('photo.process')->update($pageUserId, $photoId, $update);
            Phpfox::getService('teams.process')->updateCoverPhoto($photoId, $pageId);

            $oImage = Phpfox::getLib('image');
            foreach (Phpfox::getService('photo')->getPhotoPicSizes() as $size) {
                $oImage->createThumbnail($file, Phpfox::getParam('photo.dir_photo') . sprintf($sFileName, '_' . $size), $size, $aSize[1], true, false);
            }
            return $photoId;
        }
        return false;
    }

    /**
     * @param $iId
     * @param bool $bDoCallback
     * @param bool $bForce
     * @return bool
     * @throws \Exception
     */
    public function delete($iId, $bDoCallback = true, $bForce = false)
    {
        $aPage = $this->database()->select('*')
            ->from(Phpfox::getT('pages'))
            ->where('page_id = ' . (int)$iId)
            ->execute('getSlaveRow');

        if (!isset($aPage['page_id'])) {
            return $bForce ? false : Phpfox_Error::set($this->getFacade()->getPhrase('unable_to_find_the_page_you_are_trying_to_delete'));
        }

        if ($bForce || $aPage['user_id'] == Phpfox::getUserId() || Phpfox::getUserParam('teams.can_delete_all_teams')) {
            $iUser = $this->database()->select('user_id')->from(Phpfox::getT('user'))->where('profile_page_id = ' . (int)$aPage['page_id'] . ' AND view_id = 7')->execute('getSlaveField');

            $this->database()->delete(Phpfox::getT('pages_url'), 'page_id = ' . (int)$aPage['page_id']);

            $this->database()->delete(Phpfox::getT('feed'),
                'type_id = \'' . $this->getFacade()->getItemType() . '_itemLiked\' AND item_id = ' . (int)$aPage['page_id']);

            if (((int)$iUser) > 0 && $bDoCallback === true) {
                Phpfox::massCallback('onDeleteUser', $iUser);
            }

            if ($bDoCallback) {
                Phpfox::massCallback('onDeletePage', $iId, $this->getFacade()->getItemType());
            }

            $this->deleteImage($aPage);
            $this->database()->delete(Phpfox::getT('pages'), 'page_id = ' . $aPage['page_id']);

            if ($aPage['view_id'] == 0) {
                Phpfox::getService('user.activity')->update($aPage['user_id'], $this->getFacade()->getItemType(), '-');
            }

            (Phpfox::isModule('like') ? Phpfox::getService('like.process')->delete($this->getFacade()->getItemType(),
                (int)$aPage['page_id'], 0, true) : null);

            //close all sponsorships
            (Phpfox::isAppActive('Core_BetterAds') ? Phpfox::getService('ad.process')->closeSponsorItem('teams',
                (int)$aPage['page_id']) : null);

            // cache all teams
            $this->cache()->remove('teams_' . $iId . '_admins');
            $this->cache()->remove('teams_' . $iId . '_members');
            $this->cache()->remove('teams_' . $iId . '_pending_users');

            if ($aPage['is_sponsor'] == 1) {
                $this->cache()->remove('teams_sponsored');
            }

            return true;
        }

        return Phpfox_Error::set($this->getFacade()->getPhrase('you_are_unable_to_delete_this_page'));
    }

    public function removeLogo($iPageId = null)
    {
        $aPage = $this->getFacade()->getItems()->getPage($iPageId);
        if (!isset($aPage['page_id'])) {
            return false;
        }

        $aPage['link'] = $this->getFacade()->getItems()->getUrl($aPage['page_id'], $aPage['title'],
            $aPage['vanity_url']);

        if (!$this->getFacade()->getItems()->isAdmin($aPage) && !Phpfox::getUserParam('teams.can_edit_all_teams')) {
            return false;
        }

        $this->database()->update(Phpfox::getT('pages'), ['cover_photo_id' => '0', 'cover_photo_position' => null],
            'page_id = ' . (int)$iPageId);

        return $aPage;
    }

    public function deleteWidget($iId)
    {
        $aWidget = $this->database()->select('*')
            ->from(Phpfox::getT('pages_widget'))
            ->where('widget_id = ' . (int)$iId)
            ->execute('getSlaveRow');

        if (!isset($aWidget['widget_id'])) {
            return false;
        }

        $aPage = $this->getFacade()->getItems()->getPage($aWidget['page_id']);

        if (!isset($aPage['page_id'])) {
            return Phpfox_Error::set($this->getFacade()->getPhrase('unable_to_find_the_team_you_are_looking_for'));
        }

        if (!$this->getFacade()->getItems()->isAdmin($aPage) && !Phpfox::isAdmin() && !Phpfox::getUserParam('teams.can_edit_all_teams')) {
            return Phpfox_Error::set($this->getFacade()->getPhrase('unable_to_delete_this_widget'));
        }

        $this->database()->delete(Phpfox::getT('pages_widget'), 'widget_id = ' . (int)$iId);
        $this->database()->delete(Phpfox::getT('pages_widget_text'), 'widget_id = ' . (int)$iId);

        return true;
    }

    public function feature($iId, $iType)
    {
        Phpfox::isUser(true);
        Phpfox::getUserParam('teams.can_feature_team', true);
        $this->database()->update($this->_sTable, ['is_featured' => ($iType ? '1' : '0')], 'page_id = ' . (int)$iId);
        $this->cache()->remove('teams_featured');
        return true;
    }

    public function sponsor($iId, $sType)
    {
        if (!Phpfox::getUserParam('teams.can_sponsor_teams') && !Phpfox::getUserParam('teams.can_purchase_sponsor_teams') && !defined('PHPFOX_API_CALLBACK')) {
            return Phpfox_Error::set(_p('hack_attempt'));
        }

        $iType = (int)$sType;
        if ($iType != 0 && $iType != 1) {
            return false;
        }
        db()->update($this->_sTable, ['is_sponsor' => $iType], 'page_id = ' . (int)$iId);
        if ($sPlugin = Phpfox_Plugin::get('teams.service_process_sponsor__end')) {
            eval($sPlugin);
        }
        return true;
    }

    public function reassignOwner($iPageId, $iUserId)
    {
        $aPage = $this->getFacade()->getItems()->getPage($iPageId);
        if (empty($aPage['page_id'])) {
            return Phpfox_Error::set(_p('unable_to_find_the_team_you_are_looking_for'));
        }
        if ($iUserId == $aPage['user_id']) {
            return Phpfox_Error::set(_p('you_can_not_reassign_for_current_owner'));
        }
        if ($aPage['user_id'] != Phpfox::getUserId() && !Phpfox::isAdmin()) {
            return Phpfox_Error::set(_p('you_do_not_have_permission_to_do_this'));
        }
        db()->update($this->_sTable, ['user_id' => $iUserId], 'page_id = ' . $iPageId);

        $this->cache()->remove('teams_' . $iPageId . '_admins');
        $this->cache()->remove('member_' . $iUserId . '_secret_teams');
        $this->cache()->remove('member_' . $iUserId . '_teams');
        $this->cache()->remove('member_' . $aPage['user_id'] . '_secret_teams');
        $this->cache()->remove('member_' . $aPage['user_id'] . '_teams');

        //Update activity
        Phpfox::getService('user.activity')->update($iUserId, 'teams');
        Phpfox::getService('user.activity')->update($aPage['user_id'], 'teams', '-');

        //Auto join team for owner
        if (!Phpfox::getService('teams')->isMember($iPageId, $iUserId)) {
            db()->insert(':like', [
                'type_id' => 'teams',
                'item_id' => (int)$iPageId,
                'user_id' => $iUserId,
                'time_stamp' => PHPFOX_TIME
            ]);
            $this->database()->updateCount('like', 'type_id = \'teams\' AND item_id = ' . (int)$iPageId . '', 'total_like', 'pages', 'page_id = ' . (int)$iPageId);
            $this->cache()->remove('teams_' . $iPageId . '_members');
        }

        Phpfox::getService('notification.process')->add($this->getFacade()->getItemType() . '_reassign_owner',
            $iPageId, $iUserId);
        if (Phpfox::getUserId() != $aPage['user_id']) {
            Phpfox::getService('notification.process')->add($this->getFacade()->getItemType() . '_owner_changed',
                $iPageId, $aPage['user_id']);
        }
        return true;
    }
	
	public function updateCoverPhoto($iPhotoId, $iGroupId)
    {
        $iUserId = $this->getFacade()->getItems()->getUserId($iGroupId);
        $iAlbumId = db()->select('album_id')->from(':photo_album')
            ->where(['module_id' => $this->getFacade()->getItemType(), 'team_id' => $iGroupId, 'cover_id' => $iUserId])
            ->executeField();
        if (empty($iAlbumId)) {
            $iAlbumId = db()->insert(':photo_album', [
                'module_id'       => $this->getFacade()->getItemType(),
                'team_id'        => $iGroupId,
                'privacy'         => '0',
                'privacy_comment' => '0',
                'user_id'         => $iUserId,
                'name'            => "{_p var='cover_photo'}",
                'time_stamp'      => PHPFOX_TIME,
                'cover_id'        => $iUserId,
                'total_photo'     => 0
            ]);
            db()->insert(':photo_album_info', array('album_id' => $iAlbumId));
        }
        db()->update(':photo', ['is_cover' => 0], 'album_id=' . (int)$iAlbumId);
        db()->update(':photo', [
            'album_id'         => $iAlbumId,
            'is_cover'         => 1,
            'is_profile_photo' => 0,
            'view_id' => 0
        ], 'photo_id=' . (int)$iPhotoId);

        Phpfox::getService('photo.album.process')->updateCounter((int)$iAlbumId, 'total_photo');
    }
}
