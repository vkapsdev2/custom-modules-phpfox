<?php

namespace Apps\PHPfox_Teams\Service;

use Phpfox;
use Phpfox_Pages_Type;

/**
 * Class Teams
 *
 * @package Apps\PHPfox_Teams\Service
 */
class Type extends Phpfox_Pages_Type
{
    public function getFacade()
    {
        return Phpfox::getService('teams.facade');
    }

    /**
     * Get teams types
     * @param int $iCacheTime , default 5
     * @param bool $bNotUseCache
     * @return array
     */
	public function CheckMaxPlayer($aTypeId)
	{
		$aMaxplayer = $this->database()->select('s.max_player')
                ->from(Phpfox::getT('sports '),'s')
                ->join($this->_sTable, 'pt', 'pt.sports_id = s.listing_id')
                ->where('pt.type_id = '.$aTypeId)
                ->execute('getSlaveField');
				
		return $aMaxplayer + 1;
				
	}
	
	public function CheckSports($aUserId)
	{
		return $this->database()->select('count(*)')
                ->from(Phpfox::getT('sports_join '))
                ->where('user_id = '.$aUserId)
                ->execute('getSlaveField');
	}
	public function getForLeftSide()
    {
        
		$aRows = $this->database()->select('pt.*')
			->from($this->_sTable,'pt')
			->join(Phpfox::getT('pages_type_links '), 'ptl', 'ptl.type_id = pt.type_id')
			->where('pt.is_active = 1 AND pt.item_type = 2 ')
			->order('ordering ASC')
			->group('pt.type_id')
			->execute('getSlaveRows');

		foreach ($aRows as $iKey => $aRow) {
			$aRows[$iKey]['pages_count'] = db()->select('count(*)')
				->from(':pages')
				->where(['type_id' => $aRow['type_id'], 'view_id' => 0])
				->executeField();
			$aRows[$iKey]['category_id'] = $aRow['type_id'];
			$aRows[$iKey]['url'] = Phpfox::permalink($this->getFacade()->getItemType() . '.category',
				$aRow['type_id'], $aRow['name']);
			$aRows[$iKey]['categories'] = $aRows[$iKey]['sub'] = $this->getFacade()->getCategory()->getByTypeId($aRow['type_id']);
			if (isset($aRow['image_path']) && strpos($aRow['image_path'], 'default-category') === false) {
				$aRows[$iKey]['image_path'] = sprintf($aRow['image_path'], '_200');
			}
		}

           

        return $aRows;
    }
	
    public function get($iCacheTime = 5, $bNotUseCache = false)
    {
        
		$aRows = $this->database()->select('pt.*')
			->from($this->_sTable,'pt')
			->join(Phpfox::getT('pages_type_links '), 'ptl', 'ptl.type_id = pt.type_id')
			->where('pt.is_active = 1 AND pt.item_type = 2 AND ptl.user_id = '.Phpfox::getUserId())
			->order('ordering ASC')
			->execute('getSlaveRows');

		foreach ($aRows as $iKey => $aRow) {
			$aRows[$iKey]['pages_count'] = db()->select('count(*)')
				->from(':pages')
				->where(['type_id' => $aRow['type_id'], 'view_id' => 0])
				->executeField();
			$aRows[$iKey]['category_id'] = $aRow['type_id'];
			$aRows[$iKey]['url'] = Phpfox::permalink($this->getFacade()->getItemType() . '.category',
				$aRow['type_id'], $aRow['name']);
			$aRows[$iKey]['categories'] = $aRows[$iKey]['sub'] = $this->getFacade()->getCategory()->getByTypeId($aRow['type_id']);
			if (isset($aRow['image_path']) && strpos($aRow['image_path'], 'default-category') === false) {
				$aRows[$iKey]['image_path'] = sprintf($aRow['image_path'], '_200');
			}
		}

           

        return $aRows;
    }

    /**
     * Get number of sub categories belong to a type
     * @param $iTypeId
     * @return int
     */
    public function countSubCategories($iTypeId)
    {
        return db()->select('count(*)')->from(':pages_category')->where(['type_id' => $iTypeId])->executeField();
    }
}
