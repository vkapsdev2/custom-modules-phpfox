<?php

namespace Apps\PHPfox_Teams\Service;

use Phpfox;
use Phpfox_Ajax;
use Phpfox_Component;
use Phpfox_Error;
use Phpfox_Plugin;
use Phpfox_Template;
use Phpfox_Url;
use Phpfox_Service;
use Phpfox_Module;

/**
 * Class Callback
 *
 * @package Apps\PHPfox_Teams\Service
 */
class Privacy extends \Phpfox_Service
{
	public function getTeamsMenus($aPage){
	
	
		
		/* game start */
		
			$aMenus[] = [
				'phrase' => _p('Games'),
				'url' => Phpfox::getService('teams')
						->getUrl($aPage['page_id'], $aPage['title'], $aPage['vanity_url']) . 'game/',
				'icon' => 'module/event.png',
				'landing' => 'game',
				'total' => $this->database()->select('COUNT(*)')->from(Phpfox::getT('game'))->where("module_id = 'teams' AND item_id =".$aPage['page_id'])->execute('getSlaveField').' Games'
			];
		
		/* game end */
		
		
		
        return $aMenus;
	
	}
	public function getgameSubMenu($aPage)
    {
        if (!Phpfox::getUserParam('event.can_create_event')
        ) {
            return null;
        }

        return [
            [
                'phrase' => _p('add_new_game'),
                'url' => Phpfox_Url::instance()->makeUrl('game.add', [
                    'module' => 'teams',
                    'item' => $aPage['page_id']
                ])
            ]
        ];
    }
	
}