<?php

namespace Apps\PHPfox_Teams\Service;

use Phpfox;
use Phpfox_Pages_Category;

/**
 * Class Category
 *
 * @package Apps\PHPfox_Teams\Service
 */
class Category extends Phpfox_Pages_Category
{
    public function getFacade()
    {
        return Phpfox::getService('teams.facade');
    }

    /**
     * @param $iCategory
     *
     * @return int
     */
	
	public function addCategoryFromSports($aVals)
    {
        if (!isset($aVals['phrase_var_name'])) {
            //Add phrase for category
            $aLanguages = Phpfox::getService('language')->getAll();
            $name = $aVals['name_' . $aLanguages[0]['language_id']];
            $phrase_var_name = $this->getFacade()->getItemType() . '_category_' . md5('Pages/Groups Category' . $name . PHPFOX_TIME);
            //Add phrases
            $aText = [];
            foreach ($aLanguages as $aLanguage) {
                if (isset($aVals['name_' . $aLanguage['language_id']]) && !empty($aVals['name_' . $aLanguage['language_id']])) {
                    $aText[$aLanguage['language_id']] = $aVals['name_' . $aLanguage['language_id']];
                } else {
                    return Phpfox_Error::set((_p('Provide a "{{ language_name }}" name.',
                        ['language_name' => $aLanguage['title']])));
                }
            }
            $aValsPhrase = [
                'var_name' => $phrase_var_name,
                'text'     => $aText
            ];
            $finalPhrase = Phpfox::getService('language.phrase.process')->add($aValsPhrase);
        } else {
            $finalPhrase = $aVals['phrase_var_name'];
        }

        
		$iId = $this->database()->insert(Phpfox::getT('pages_type'), [
'is_active'  => isset($aVals['is_active']) ? $aVals['is_active'] : '1',
'name'       => $finalPhrase,
'time_stamp' => PHPFOX_TIME,
'ordering'   => '0',
'item_type'  => 2,
'user_id'      => $aVals['user_id'],
'image_path'      => $aVals['image_path'],
'image_server_id' => $aVals['server_id']
]);
        
        Core\Lib::phrase()->clearCache();
        $this->cache()->removeGroup(2);
        return $iId;
    }
	
    public function getTypeIdByCategoryId($iCategory)
    {
        $pageType = db()->select('type_id')
            ->from($this->_sTable)
            ->where('category_id = ' . (int)$iCategory)
            ->execute('getSlaveField');
        return $pageType;
    }

    public function getForBrowse($bIncludePages = false, $userId = null, $iPagesLimit = null, $sView = '')
    {
        $where = 'pt.item_type = 2';
        if (!in_array($sView, ['my', 'joined'])) {
            $where .= ' AND pt.is_active = 1';
        }
        $aTypes = $this->database()->select('pt.*')
            ->from(Phpfox::getT('pages_type'), 'pt')
            ->where($where)
            ->order('pt.ordering ASC')
            ->execute('getSlaveRows');

        foreach ($aTypes as $iKey => $aType) {
            if ($bIncludePages) {
                $aTypes[$iKey]['pages'] = $this->getLatestPages($aType['type_id'], $userId, $iPagesLimit, $sView);

                foreach ($aTypes[$iKey]['pages'] as $iSubKey => &$aPage) {
                    $aPage['link'] = Phpfox::getService('teams')->getUrl($aPage['page_id'], $aPage['title'], $aPage['vanity_url']);

                    // check permission for each action
                    Phpfox::getService('teams')->getActionsPermission($aPage, $sView);

                    // check is member
                    $aPage['is_liked'] = Phpfox::getService('teams')->isMember($aPage['page_id']);

                    // pending request to be member
                    $aPage['joinRequested'] = Phpfox::getService('teams')->joinTeamRequested($aPage['page_id']);
                }

                // get total pages for each category
                $aTypes[$iKey]['total_pages'] = Phpfox::getService('teams')->getItemsByCategory($aType['type_id'], false, 1, $userId, true, $sView);
            }

            if ($sView) {
                $aTypes[$iKey]['link'] = Phpfox::permalink(['teams.category', 'view' => $sView], $aType['type_id'], $aType['name']);
            } else {
                $aTypes[$iKey]['link'] = Phpfox::permalink('teams.category', $aType['type_id'], $aType['name']);
            }

            // get sub categories
            $aTypes[$iKey]['image_path'] = sprintf($aTypes[$iKey]['image_path'], '_200');
        }

        return $aTypes;
    }

    /**
     * Get lastest pages
     *
     * @param        $iId
     * @param null   $userId
     * @param int    $iPagesLimit , number of page want to limit, 0 for unlimited
     * @param string $sView
     *
     * @return array|int|string
     */
    public function getLatestPages($iId, $userId = null, $iPagesLimit = 8, $sView = '')
    {
        $extra_conditions = 'pages.type_id = ' . (int)$iId;
        if (!empty($userId)) {
            $extra_conditions .= ' AND pages.app_id = 0 AND pages.view_id IN(0,1) AND pages.user_id = ' . (int)$userId . ' ';
        } else if ($sView != 'my') {
            if ($sView == 'pending') {
                $extra_conditions .= ' AND pages.app_id = 0 AND pages.view_id = 1 ';
            } else {
                $extra_conditions .= ' AND pages.app_id = 0 AND pages.view_id = 0 ';
            }
            if($sView == 'joined') {
                $aTeamIds = Phpfox::getService('teams')->getAllTeamIdsOfMember();
                if (count($aTeamIds)) {
                    $extra_conditions .= " AND pages.page_id IN (" . implode(',', $aTeamIds) . ")";
                }
                else {
                    $extra_conditions .= " AND pages.page_id IN (0)";
                }
            }
            else if ($sView == 'friend' || Phpfox::getParam('core.friends_only_community')) {
                $sFriendsList = '0';
                if (Phpfox::getUserId()) {
                    $aFriends = (Phpfox::isModule('friend') ? Phpfox::getService('friend')->getFromCache() : []);
                    $aFriendIds = array_column($aFriends, 'user_id');
                    if ($sView != 'friend') {
                        $aFriendIds[] = Phpfox::getUserId();
                    }
                    if (!empty($aFriendIds)) {
                        $sFriendsList = implode(',', $aFriendIds);
                    }
                }
                $extra_conditions .= ' AND (pages.user_id IN (' . $sFriendsList . ') ';
                if (Phpfox::getParam('core.friends_only_community')) {
                    $aTeamIds = Phpfox::getService('teams')->getAllTeamIdsOfMember();
                    if (count($aTeamIds)) {
                        $extra_conditions .= " OR pages.page_id IN (" . implode(',', $aTeamIds) . ")";
                    }
                }
                $extra_conditions .= ') ';
            }
        }

        if (($userId != Phpfox::getUserId() || $userId === null) && Phpfox::hasCallback('teams', 'getExtraBrowseConditions')) {
            $extra_conditions .= Phpfox::callback('teams.getExtraBrowseConditions', 'pages');
        }

        $sOrder = 'pages.time_stamp DESC';
        $this->database()->select('pages.*')
            ->from(Phpfox::getT('pages'), 'pages')
            ->where($extra_conditions)
            ->order($sOrder)
            ->limit($iPagesLimit)
            ->union()
            ->unionFrom('pages');

        $this->database()->select('pages.*, pt.text, pt.text_parsed, pu.vanity_url, ' . Phpfox::getUserField('u2', 'profile_'))
            ->join(Phpfox::getT('user'), 'u2', 'u2.profile_page_id = pages.page_id')
            ->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = pages.page_id')
            ->leftJoin(':pages_text', 'pt', 'pt.page_id = pages.page_id');

        return $this->database()
            ->order($sOrder)
            ->limit($iPagesLimit)
            ->execute('getSlaveRows');
    }

    /**
     * Move sub categories to another type
     *
     * @param $iOldTypeId
     * @param $iNewTypeId
     */
    public function moveSubCategoriesToAnotherType($iOldTypeId, $iNewTypeId)
    {
        // update type id of sub-categories
        db()->update(':pages_category', [
            'type_id' => $iNewTypeId
        ], [
            'type_id' => $iOldTypeId
        ]);
    }

    /**
     * Get category name
     *
     * @param $iCategoryId
     *
     * @return string
     */
    public function getCategoryName($iCategoryId)
    {
        return _p(db()->select('name')->from(':pages_category')->where(['category_id' => $iCategoryId])->executeField());
    }
}
