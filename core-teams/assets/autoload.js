$Core.Teams = {
  searching: false,
  searchAction: null,
  cmds: {
    add_new_team: function(ele, evt) {
      tb_show('Add New Team',
          $.ajaxBox('teams.addTeam', 'height=400&width=550&type_id=' +
              ele.data('type-id')));
      return false;
    },

    select_category: function(ele, evt) {
      $('[class^=select-category-]').hide();
      $('.select-category-' + ele.val()).show();
      $('#select_sub_category_id').val(0);
    },

    add_team_process: function(ele, evt) {
      evt.preventDefault();
      ele.ajaxCall('teams.add');
      // disable submit button
      var submit = $('input[type="submit"]', ele);
      submit.prop('disabled', true).addClass('submitted');
    },

    widget_add_form: function(ele, evt) {
      if (ele.val() === '1') {
        $('#js_teams_widget_block').slideUp('fast');
      } else {
        $('#js_teams_widget_block').slideDown('fast');
      }
    },

    init_drag: function(ele) {
      Core_drag.init({table: ele.data('table'), ajax: ele.data('ajax')});
    },

    search_member: function(ele, evt) {
      if ($Core.Teams.searching === true) {
        return;
      }

      clearTimeout($Core.Teams.searchAction);
      $Core.Teams.searchAction = setTimeout(function() {
        // process searching
        $Core.Teams.searching = true;

        var parentBlock = $('.teams-block-members'),
            activeTab = $('li.active a', parentBlock),
            container = $(ele.data('container')),
            resultContainer = ele.val() ? ele.data('result-container') : ele.data('listing-container'),
            spinner = $('.teams-searching', parentBlock);

        container.addClass('hide');
        spinner.removeClass('hide');
        $.ajaxCall('teams.getMembers', 'tab=' + activeTab.data('tab') + '&container=' + resultContainer + '&team_id=' + ele.data('team-id') + '&search=' + ele.val());
      }, 500);
    },

    change_tab: function(ele, evt) {
      evt.preventDefault();
      var container = $(ele.data('container')),
          resultCotainer = $(ele.data('result-container'));

      // hide search result div, show container div
      container.hasClass('hide') && container.removeClass('hide');
      !resultCotainer.hasClass('hide') && resultCotainer.addClass('hide');

      // only show moderation in `all members` tab
      if (ele.data('tab') === 'all') {
        $('.moderation_placeholder').removeClass('hide');
      } else {
        $('.moderation_placeholder').addClass('hide');
      }

      // ajax call to get tab members
      $.ajaxCall('teams.getMembers', 'tab=' + ele.data('tab') + '&container=' + ele.data('container') + '&team_id=' + ele.data('team-id'));
    },

    remove_member: function(ele, evt) {
      $Core.jsConfirm({
        message: ele.data('message')
      }, function() {
        $.ajaxCall('teams.removeMember', 'team_id=' + ele.data('team-id') + '&user_id=' + ele.data('user-id'))
      }, function() {});
    },

    remove_pending: function(ele, evt) {
      $Core.jsConfirm({
        message: ele.data('message')
      }, function() {
        $.ajaxCall('teams.removePendingRequest', 'sign_up=' + ele.data('signup-id') + '&user_id=' + ele.data('user-id'));
      }, function() {});
    },

    remove_admin: function(ele, evt) {
      $Core.jsConfirm({
        message: ele.data('message')
      }, function() {
        $.ajaxCall('teams.removeAdmin', 'team_id=' + ele.data('team-id') + '&user_id=' + ele.data('user-id'))
      }, function() {});
    },

    disable_submit: function(form) {
      $('input[type="submit"]', form).prop('disabled', true).addClass('submitted');
    },

    join_team: function(ele, evt) {
      ele.fadeOut('fast', function() {
        if (ele.data('is-closed') == 1) {
          ele.prev().fadeIn('fast');
          $.ajaxCall('teams.signup', 'page_id=' + ele.data('id')); return false;
        } else {
          ele.prev().prev().fadeIn('fast');
          $.ajaxCall('like.add', 'type_id=teams&item_id=' + ele.data('id')); return false;
        }
      });
    },
  },

  setAsCover: function(iPageId, iPhotoId) {
    $.ajaxCall('teams.setCoverPhoto', 'page_id=' + iPageId +
        '&photo_id=' + iPhotoId);
  },

  removeCover: function(iPageId) {
    $Core.jsConfirm({}, function() {
      $.ajaxCall('teams.removeCoverPhoto', 'page_id=' + iPageId);
    }, function() {});
  },

  resetSubmit: function() {
    $('input[type="submit"].submitted').each(function() {
      $(this).prop('disabled', false).removeClass('submitted');
    });
  },

  searchingDone: function(searchingDone) {
    $Core.Teams.searching = false;
    $('.teams-searching').addClass('hide');

    if (typeof searchingDone === 'boolean' && searchingDone) {
      $('.search-member-result').removeClass('hide');
      $('.teams-member-listing').empty();
    }
  },
  
  updateCounter: function(selector) {
    var ele = $(selector),
        counter = ele.html().substr(1, ele.html().length - 2);

    ele.html('('+ (parseInt(counter) - 1) +')');
  },

  hideSearchResults: function() {
    $Core.Teams.searching = false;
    $('.teams-searching').addClass('hide');
    $('.search-member-result').empty();
  },
  initTeamCategory: function () {
    // Creating/Editing teams
    if ($Core.exists('#js_teams_add_holder')) {
      $(document).on('change', '.teams_add_category select', function() {
        var detailBlock = $('#js_teams_block_detail');
        $('.js_teams_add_sub_category', detailBlock).hide();
        $('#js_teams_add_sub_category_' + $(this).val(), detailBlock).show();
        $('#js_category_teams_add_holder').
        val($('#js_teams_add_sub_category_' + $(this).val() +' option:first', detailBlock).val());
      });

      $(document).on('change', '.js_teams_add_sub_category select', function() {
        $('#js_category_teams_add_holder').val($(this).val());
      });
    }
  },
  processTeamFeed: function(oObj){
      var oThis = $(oObj);
      var type = oThis.data('type');
      var opposite_type = (type == 'like' ? 'unlike' : 'like');
      var sAjaxCall = (type == 'like' ? 'like.add' : 'like.delete');
      var oActionContent = oThis.closest('.js_team_feed_action_content');
      $.ajaxCall(sAjaxCall, 'type_id=teams&item_id=' + oActionContent.data('team-id'));
      oThis.parent().addClass('hide');
      oActionContent.find('[data-type="' + opposite_type + '"]').parent().removeClass('hide');
  },
  redirectToDetailTeam: function(oObj)
  {
      var sUrl = $(oObj).data('url');
      if(sUrl)
      {
          window.location.href = sUrl;
      }
  },
  processEditFeedStatus: function(feed_id, status, deleteLink) {
      if($('#js_item_feed_' + feed_id).length) {
          var parent = $('#js_item_feed_' + feed_id).find('.activity_feed_content_text:first');
          if(parent.find('.activity_feed_content_status:first').length) {
              parent.find('.activity_feed_content_status:first').html(status);
          }
          else {
              parent.prepend('<div class="activity_feed_content_status">' + status + '</div>');
          }
          if(typeof deleteLink !== 'undefined') {
              $("#js_item_feed_" + feed_id).find(".activity_feed_content_link").remove();
          }
      }
  }
};

$(document).on('click', '[data-app="core_teams"]', function(evt) {
  var action = $(this).data('action'),
      type = $(this).data('action-type');
  if (type === 'click' && $Core.Teams.cmds.hasOwnProperty(action) &&
      typeof $Core.Teams.cmds[action] === 'function') {
    $Core.Teams.cmds[action]($(this), evt);
  }
});

$(document).on('change', '[data-app="core_teams"]', function(evt) {
  var action = $(this).data('action'),
      type = $(this).data('action-type');
  if (type === 'change' && $Core.Teams.cmds.hasOwnProperty(action) &&
      typeof $Core.Teams.cmds[action] === 'function') {
    $Core.Teams.cmds[action]($(this), evt);
  }
});

$(document).on('submit', '[data-app="core_teams"]', function(evt) {
  var action = $(this).data('action'),
      type = $(this).data('action-type');
  if (type === 'submit' && $Core.Teams.cmds.hasOwnProperty(action) &&
      typeof $Core.Teams.cmds[action] === 'function') {
    $Core.Teams.cmds[action]($(this), evt);
  }
});

$(document).on('keyup', '[data-app="core_teams"]', function(evt) {
  var action = $(this).data('action'),
      type = $(this).data('action-type');
  if (type === 'keyup' && $Core.Teams.cmds.hasOwnProperty(action) &&
      typeof $Core.Teams.cmds[action] === 'function') {
    $Core.Teams.cmds[action]($(this), evt);
  }
});

$(document).on('click', '#js_teams_add_change_photo', function() {
  $('#js_event_current_image').hide();
  $('#js_event_upload_image').fadeIn();
});

$Behavior.teamsInitElements = function() {
  $('[data-app="core_teams"][data-action-type="init"]').each(function() {
    var t = $(this);
    if (t.data('action-type') === 'init' &&
        $Core.Teams.cmds.hasOwnProperty(t.data('action')) &&
        typeof $Core.Teams.cmds[t.data('action')] === 'function') {
      $Core.Teams.cmds[t.data('action')](t);
    }
  });
};

$Behavior.contentHeight = function() {
  $('#content').height($('.main_timeline').height());
};

$Behavior.fixSizeTinymce = function() {
  //The magic code to add show/hide custom event triggers
  (function($) {
    $.each(['show', 'hide'], function(i, ev) {
      var el = $.fn[ev];
      $.fn[ev] = function() {
        this.trigger(ev);
        return el.apply(this, arguments);
      };
    });
  })(jQuery);

  $('#js_teams_block_info').on('show', function() {
    $('.mceIframeContainer.mceFirst.mceLast iframe').height('275px');
  });
};

PF.event.on('on_document_ready_end', function() {
  $Core.Teams.initTeamCategory();
});

PF.event.on('on_page_change_end', function() {
  $Core.Teams.initTeamCategory();
});