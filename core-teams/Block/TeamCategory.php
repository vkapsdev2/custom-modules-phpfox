<?php

namespace Apps\PHPfox_Teams\Block;

use Phpfox;
use Phpfox_Component;
use Phpfox_Plugin;

defined('PHPFOX') or exit('NO DICE!');

/**
 * Class TeamCategory
 * @package Apps\PHPfox_Teams\Block
 */
class TeamCategory extends Phpfox_Component
{
    /**
     * Controller
     */
    public function process()
    {
        if (defined('PHPFOX_IS_USER_PROFILE')) {
            return false;
        }

        
        $iCurrentCategory = $this->getParam('sCurrentCategory', null);
        $iParentCategoryId = $this->getParam('iParentCategoryId', 0);

        $aCategories = Phpfox::getService('teams.type')->getForLeftSide();

        if (!is_array($aCategories) || !count($aCategories)) {
            return false;
        }

        $this->template()->assign([
            'sHeader' => _p('categories'),
            'iCurrentCategory' => $iCurrentCategory,
            'iParentCategoryId' => $iParentCategoryId,
            'aCategories' => $aCategories
        ]);

        return 'block';
    }

    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
        (($sPlugin = Phpfox_Plugin::get('teams.component_block_category_clean')) ? eval($sPlugin) : false);
    }
}
