<?php

namespace Apps\PHPfox_Teams\Block;

use Phpfox_Component;

defined('PHPFOX') or exit('NO DICE!');

class TeamCropme extends Phpfox_Component
{
    public function process()
    {
        $iTeamId = $this->request()->get('id');
        $aTeam = \Phpfox::getService('teams')->getForEdit($iTeamId);
        $this->template()->assign([
            'aTeamCropMe' => $aTeam
        ]);
    }
}
