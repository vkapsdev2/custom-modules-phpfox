<?php

namespace Apps\PHPfox_Teams\Block;

use Phpfox;
use Phpfox_Pager;
use Phpfox_Plugin;

defined('PHPFOX') or exit('NO DICE!');

class SearchFanmember extends \Phpfox_Component
{
    public function process()
    {
        $sTab = $this->getParam('tab', 'all');
        $iTeamId = $this->getParam('team_id');
        $iPage = $this->getParam('page', 1);
        $iLimit = Phpfox::getParam('core.items_per_page', 20);
        $sSearch = $this->getParam('search');

        switch ($sTab) {
            case 'pending':
                $aMembers = Phpfox::getService('teams')->getPendingUsers($iTeamId, false, $iPage, $iLimit, $sSearch);
                $iSize = Phpfox::getService('teams')->getPendingUsers($iTeamId, true);
                break;
            case 'admin':
                $aMembers = Phpfox::getService('teams')->getPageAdmins($iTeamId, $iPage, $iLimit, $sSearch);
                $iSize = Phpfox::getService('teams')->getTeamAdminsCount($iTeamId);
                break;
            case 'all':
            default:
                list($iSize, $aMembers) = Phpfox::getService('teams')->getFanMembers($iTeamId, $iLimit, $iPage, $sSearch);
                break;
        }

        // Pagination configuration
        if (!$sSearch) {
            Phpfox_Pager::instance()->set(array(
                'page' => $iPage,
                'size' => $iLimit,
                'count' => $iSize,
                'paging_mode' => 'pagination',
                'ajax_paging' => [
                    'block' => 'teams.search-member',
                    'params' => [
                        'tab' => $sTab,
                        'team_id' => $iTeamId
                    ],
                    'container' => $this->getParam('container')
                ]
            ));
        }

        $this->template()->assign([
            'sTab' => $sTab,
            'aMembers' => $aMembers,
            'bIsAdmin' => Phpfox::getService('teams')->isAdmin($iTeamId),
            'bIsOwner' => Phpfox::getService('teams')->getPageOwnerId($iTeamId) == Phpfox::getUserId(),
            'iTeamId' => $iTeamId,
            'sSearch' => $sSearch,
            'bShowFriendInfo' => true,
        ]);
        $this->setParam(['mutual_list' => true]);

        return 'block';
    }

    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
        (($sPlugin = Phpfox_Plugin::get('teams.component_block_search_member_clean')) ? eval($sPlugin) : false);
    }
}
