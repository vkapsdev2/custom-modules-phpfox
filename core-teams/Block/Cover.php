<?php

namespace Apps\PHPfox_Teams\Block;

use Phpfox;
use Phpfox_Plugin;
use Phpfox_Component;
use Phpfox_File;

class Cover extends Phpfox_Component
{
    public function process()
    {
        if (($iPageId = $this->request()->get('page_id'))) {
            $this->template()->assign(array(
                'iPageId' => $iPageId
            ));
        }
        if (($iGroupId = $this->request()->get('groups_id'))) {
            $this->template()->assign(array(
                'iGroupId' => $iGroupId
            ));
        }

		if (($iTeamId = $this->request()->get('teams_id'))) {
            $this->template()->assign(array(
                'iTeamId' => $iTeamId
            ));
        }
		
        $iMaxUploadFileSize = Phpfox::getUserParam('photo.photo_max_upload_size');
        $this->template()->assign([
            'iMaxUploadFileSize' => $iMaxUploadFileSize,
            'sMaxUploadFileSize' => Phpfox_File::filesize($iMaxUploadFileSize * 1024)
        ]);
        return 'block';
    }

    
    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
       
    }
}