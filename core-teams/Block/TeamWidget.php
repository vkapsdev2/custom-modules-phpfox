<?php

namespace Apps\PHPfox_Teams\Block;

use Phpfox_Component;
use Phpfox_Plugin;

defined('PHPFOX') or exit('NO DICE!');

class TeamWidget extends Phpfox_Component
{
    public function process()
    {
        $aWidgetBlocks = \Phpfox::getService('teams')->getWidgetBlocks();

        if (!count($aWidgetBlocks)) {
            return false;
        }

        $this->template()->assign([
                'aWidgetBlocks' => $aWidgetBlocks,
            ]
        );

        return null;
    }

    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
        (($sPlugin = Phpfox_Plugin::get('teams.component_block_widget_clean')) ? eval($sPlugin) : false);
    }
}
