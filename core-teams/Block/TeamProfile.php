<?php

namespace Apps\PHPfox_Teams\Block;

use Phpfox;
use Phpfox_Component;
use Phpfox_Plugin;

defined('PHPFOX') or exit('NO DICE!');

class TeamProfile extends Phpfox_Component
{
    /**
     * Controller
     */
    public function process()
    {
        if (!Phpfox::getUserParam('teams.pf_team_browse', false)) {
            return false;
        }

        $aUser = $this->getParam('aUser');
        $sExtraConds = (Phpfox::getUserParam('core.can_view_private_items') || $aUser['user_id'] == Phpfox::getUserId()) ? "" : " AND (p.reg_method <> 2)";
        list($iTotal, $aPages) = Phpfox::getService('teams')->getForProfile($aUser['user_id'], 10, false, $sExtraConds);

        if (!$iTotal) {
            return false;
        }

        $this->template()->assign([
                'sHeader' => '<a href="' . $this->url()->makeUrl($aUser['user_name'],
                        'teams/?view=all') . '" title="' . _p('Teams you created and joined') . '">' . _p('joined_teams') . '<span>' . $iTotal . '</span></a>',
                'aPagesList' => $aPages,
                'sDefaultCoverPath' => Phpfox::getParam('teams.default_cover_photo')
            ]
        );

        return 'block';
    }

    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
        (($sPlugin = Phpfox_Plugin::get('teams.component_block_profile_clean')) ? eval($sPlugin) : false);
    }
}
