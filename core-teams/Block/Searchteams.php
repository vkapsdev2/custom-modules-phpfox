<?php
namespace Apps\PHPfox_Teams\Block;

use Phpfox;
use Phpfox_Pager;
use Phpfox_Plugin;

defined('PHPFOX') or exit('NO DICE!');

class Searchteams extends \Phpfox_Component
{
	/**
	 * Controller
	 */
	public function process()
	{
		$iPage = $this->getParam('page', 0);
		$iPageSize = 36;		
		$bIsOnline = false;		
		//$oDb = Phpfox_Database::instance();
		$aParams = array();
		$aConditions = array();
		$iListId = 0;
		
		//$aConditions[] = 'AND friend.is_page = 0';
		
		if ($this->getParam('type') != 'mail')
		{
			//$aConditions[] = 'AND friend.user_id = ' . Phpfox::getUserId();
		}
		
		if (($sFind = $this->getParam('find')))
		{
			$aConditions[] = 'AND (u.full_name LIKE \'%' . trim($sFind) . '%\' OR (u.email LIKE \'%' . trim($sFind) . '@%\' OR u.email = \'' . trim($sFind) . '\'))';	
		}		
		
		$aLetters = array(
			_p('all'), '#', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
		);			
		
		if (($sLetter = $this->getParam('letter')) && in_array($sLetter, $aLetters) && strtolower($sLetter) != 'all')
		{
			if ($sLetter == '#')
			{
				$sSubCondition = '';
				for ($i = 0; $i <= 9; $i++)
				{
					$sSubCondition .= "OR u.full_name LIKE '" . trim($i) . "%' ";
				}
				$sSubCondition = ltrim($sSubCondition, 'OR ');
				$aConditions[] = 'AND (' . $sSubCondition . ')';
			}
			else 
			{
				$aConditions[] = "AND u.full_name LIKE '" . trim($sLetter) . "%'";
			}
			
			$aParams['letter'] = $sLetter;
		}		
		
		if ($sView = $this->getParam('view'))
		{
			switch ($sView)
			{
				case 'top':
					$aConditions[] = 'AND is_top_friend = 1';
					break;
				case 'online':
					$bIsOnline = true;
					break;
				case 'all':
					
					break;
				default:					
					
					break;
			}
		}
		
		$aGameDetails = $this->getParam('games');
		$aConditions[] = 'AND pt.sports_id = ' . $aGameDetails['sports_id'].' AND pages.user_id != '.$aGameDetails['user_id'];
		list(, $aFriends) = Phpfox::getService('teams.games')->get($aConditions, 'u.full_name ASC', $iPage, $iPageSize, true, true, $bIsOnline, null, false, $iListId);
		
		(($sPlugin = Phpfox_Plugin::get('friend.component_block_search_get')) ? eval($sPlugin) : false);
		
		$aParams['input'] = $this->getParam('input');
		$aParams['friend_item_id'] = $this->getParam('friend_item_id');
		$aParams['friend_module_id'] = $this->getParam('friend_module_id');
		$aParams['type'] = $this->getParam('type');
		$bInForm = $this->getParam('in_form', false);
			
		$sFriendModuleId = $this->getParam('friend_module_id', '');

		$this->template()->assign(array(
				'aFriends' => $aFriends,
				'aLetters' => $aLetters,
				'sView' => $sView,
				'sActualLetter' => $sLetter,
				'sPrivacyInputName' => $this->getParam('input'),
				'bSearch' => $this->getParam('search'),
				'bIsForShare' => $this->getParam('friend_share', false),
				'sFriendItemId' => (int) $this->getParam('friend_item_id', '0'),
				'sFriendModuleId' => $sFriendModuleId,
				'sFriendType' => $this->getParam('type'),
				'bInForm' => $bInForm
			)
		);
		
		(($sPlugin = Phpfox_Plugin::get('friend.component_block_search_process')) ? eval($sPlugin) : false);
	}
	
	/**
	 * Garbage collector. Is executed after this class has completed
	 * its job and the template has also been displayed.
	 */
	public function clean()
	{
		(($sPlugin = Phpfox_Plugin::get('friend.component_block_search_clean')) ? eval($sPlugin) : false);
	}
}