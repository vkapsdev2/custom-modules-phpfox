<?php

namespace Apps\PHPfox_Teams\Block;

use Phpfox_Plugin;

defined('PHPFOX') or exit('NO DICE!');

class Pending extends \Phpfox_Component
{
    public function process()
    {
        $aTeam = $this->getParam('aPage', false);
        if (!$aTeam || $aTeam['view_id'] == 0) {
            return false;
        }

        \Phpfox::getService('teams')->getActionsPermission($aTeam, 'pending');
        $aActions = [];
        if ($aTeam['bCanApprove']) {
            $aActions['approve'] = [
                'is_ajax' => true,
                'action' => '$.ajaxCall(\'teams.approve\', \'page_id='. $aTeam['page_id'] .'\')',
                'label' => _p('approve')
            ];
        }
        if ($aTeam['bCanEdit']) {
            $aActions['edit'] = [
                'action' => url()->make('teams.add', ['id' => $aTeam['page_id']]),
                'label' => _p('edit')
            ];
        }
        if ($aTeam['bCanDelete']) {
            $aActions['delete'] = [
                'action' => url()->make('teams', ['delete' => $aTeam['page_id']]),
                'label' => _p('delete'),
                'is_confirm' => true,
                'confirm_message' => _p('are_you_sure_you_want_to_delete_this_team_permanently')
            ];
        }

        $this->template()->assign([
            'aPendingItem' => [
                'message' => _p('this_team_is_pending_approval'),
                'actions' => $aActions
            ]
        ]);

        return 'block';
    }

    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
        (($sPlugin = Phpfox_Plugin::get('teams.component_block_pending_clean')) ? eval($sPlugin) : false);
    }
}
