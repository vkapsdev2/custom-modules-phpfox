<?php

namespace Apps\PHPfox_Teams\Block;

use Phpfox;
use Phpfox_Plugin;

class RelatedTeams extends \Phpfox_Component
{
    public function process()
    {
        $aTeam = $this->getParam('aPage', false);
        $iLimit = $this->getParam('limit', 4);

        if (!$iLimit || !$aTeam || !$this->getParam('is_show', true)) {
            return false;
        }
        // get teams with the same category
        $aTeams = Phpfox::getService('teams')->getSameCategoryPages($aTeam['page_id'], $iLimit);

        if (!count($aTeams)) {
            return false;
        }

        $this->template()->assign([
            'aTeams' => $aTeams,
            'sDefaultCoverPath' => Phpfox::getParam('teams.default_cover_photo')
        ]);

        return 'block';
    }

    /**
     * Block settings
     * @return array
     */
    public function getSettings()
    {
        return [
            [
                'info' => _p('block_related_teams_show_info'),
                'description' => '',
                'value' => false,
                'var_name' => 'is_show',
                'type' => 'boolean'
            ],
            [
                'info' => _p('block_related_teams_info'),
                'description' => _p('block_related_teams_description'),
                'value' => 4,
                'var_name' => 'limit',
                'type' => 'integer'
            ]
        ];
    }

    /**
     * Validation
     *
     * @return array
     */
    public function getValidation()
    {
        return [
            'limit' => [
                'def' => 'int:required',
                'title' => _p('validator_teams_relatedteams_limit'),
                'min' => 0
            ]
        ];
    }

    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
        (($sPlugin = Phpfox_Plugin::get('teams.component_block_related_teams_clean')) ? eval($sPlugin) : false);
    }
}
