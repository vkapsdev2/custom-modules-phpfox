<?php

namespace Apps\PHPfox_Teams\Block;

use Phpfox;
use Phpfox_Component;
use Phpfox_Plugin;

defined('PHPFOX') or exit('NO DICE!');

class TeamPhoto extends Phpfox_Component
{
    public function process()
    {
        if (!defined('PHPFOX_IS_PAGES_VIEW') && !Phpfox_Component::__getParam('show_team_cover')) {
            return false;
        }

        $aTeam = $this->getParam('aPage');

        if (empty($aTeam['page_id'])) {
            return false;
        }

        $aCoverPhoto = ($aTeam['cover_photo_id'] ? Phpfox::getService('photo')->getCoverPhoto($aTeam['cover_photo_id']) : false);
        $iCoverPhotoPosition = $aTeam['cover_photo_position'];

        $aTeamMenus = Phpfox::getService('teams')->getMenu($aTeam);

        if ($oProfileImage = storage()->get('user/avatar/' . $aTeam['page_user_id'])) {
            $aProfileImage = Phpfox::getService('photo')->getPhoto($oProfileImage->value);
            $this->template()->assign('aProfileImage', $aProfileImage);
        }

        $this->template()->assign([
            'aCoverPhoto' => $aCoverPhoto,
            'iConverPhotoPosition' => $iCoverPhotoPosition,
            'aTeamMenus' => $aTeamMenus,
            'bCanChangePhoto' => Phpfox::getService('teams')->isAdmin($aTeam) || Phpfox::getUserParam('teams.can_edit_all_teams'),
            'sDefaultCoverPath' => Phpfox::getParam('teams.default_cover_photo'),
        ]);

        if (empty($this->template()->getVar('aPage'))) {
            $this->template()->assign('aPage', $aTeam);
        }

        return null;
    }

    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
        (($sPlugin = Phpfox_Plugin::get('teams.component_block_photo_clean')) ? eval($sPlugin) : false);
    }
}
