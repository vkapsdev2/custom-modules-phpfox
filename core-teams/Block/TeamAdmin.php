<?php

namespace Apps\PHPfox_Teams\Block;

use Phpfox;
use Phpfox_Component;
use Phpfox_Plugin;

defined('PHPFOX') or exit('NO DICE!');

class TeamAdmin extends Phpfox_Component
{
    public function process()
    {
        $iLimit = $this->getParam('limit', 4);
        $aTeam = $this->getParam('aPage');

        if (!$iLimit || empty($aTeam) || !Phpfox::getService('teams')->hasPerm($aTeam['page_id'], 'teams.view_admins')) {
            return false;
        }

        $this->template()->assign([
            'sHeader' => _p('Admins'),
            'aPageAdmins' => Phpfox::getService('teams')->getPageAdmins($aTeam['page_id'], 1, $iLimit),
        ]);

        return 'block';
    }

    public function getSettings()
    {
        return [
            [
                'info' => _p('teams_block_admin_limit_info'),
                'description' => _p('teams_block_admin_limit_description'),
                'value' => 4,
                'var_name' => 'limit',
                'type' => 'integer'
            ]
        ];
    }

    /**
     * Validation
     *
     * @return array
     */
    public function getValidation()
    {
        return [
            'limit' => [
                'def' => 'int:required',
                'title' => _p('validator_teams_teamadmin_limit'),
                'min' => 0
            ]
        ];
    }

    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
        (($sPlugin = Phpfox_Plugin::get('teams.component_block_admin_clean')) ? eval($sPlugin) : false);
    }
}
