<?php
namespace Apps\PHPfox_Teams\Block;

use Phpfox;
use Phpfox_Component;
use Phpfox_Plugin;

/**
 * Class FeedTeamBlock
 * @package Apps\PHPfox_Teams\Block
 */
class FeedTeamBlock extends Phpfox_Component
{
    public function process()
    {
        if($iFeedId = $this->getParam('this_feed_id'))
        {
            $aTeam = $this->getParam('custom_param_feed_team_' . $iFeedId);
            if(empty($aTeam))
            {
                return false;
            }
            $aTeam['text_parsed'] = strip_tags($aTeam['text_parsed']);
            $this->template()->assign([
                'aTeam' => $aTeam,
            ]);
        }
    }
}
