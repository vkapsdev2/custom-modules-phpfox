<?php

if (!empty($aVals['module_id']) && !empty($aVals['item_id']) && $aVals['module_id'] == 'teams' && Phpfox::isAppActive('PHPfox_Teams')) {
    if (!Phpfox::getService('teams.process')->setCoverPhoto($aVals['item_id'], $iId, true)) {
        return $this->error(_p('Cannot set cover photo for this team.'));
    }
}
