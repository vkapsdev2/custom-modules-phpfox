<?php
\Core\Queue\Manager::instance()
    ->addHandler('teams_member_join_notifications', '\Apps\PHPfox_Teams\Job\SendMemberJoinNotification')
    ->addHandler('teams_member_notifications', '\Apps\PHPfox_Teams\Job\SendMemberNotification')
    ->addHandler('teams_convert_old_team', '\Apps\PHPfox_Teams\Job\ConvertOldTeams');
