<?php
defined('PHPFOX') or exit('NO DICE!');

$iProfilePageId = Phpfox::getUserBy('profile_page_id');
if (!PHPFOX_IS_AJAX && $iProfilePageId > 0 && Phpfox::getLib('pages.facade')->getPageItemType($iProfilePageId) == 'teams') {
    $bSend = true;
    $aPage = \Phpfox::getService('teams')->getPage($iProfilePageId);
    $sReq1 = Phpfox_Request::instance()->get('req1');
    if (defined('PHPFOX_IS_PAGE_ADMIN')) {
        $bSend = false;
    }

    if ($bSend && !\Phpfox::getService('teams')->isInPage()) {
        Phpfox_Url::instance()->forward(Phpfox::getService('teams')->getUrl($aPage['page_id'], $aPage['title'],
            $aPage['vanity_url']));
    }
}
