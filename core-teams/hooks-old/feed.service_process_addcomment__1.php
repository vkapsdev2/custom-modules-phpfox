<?php
if (isset($this->_aCallback['module']) && $this->_aCallback['module'] == 'teams' && Phpfox::getUserId() != Phpfox::getService('teams')->getUserId($this->_aCallback['item_id'])) {
    $sLink = $this->_aCallback['link'] . 'comment-id_' . $iStatusId . '/';

    // get and send email/notification to all admins of location
    $aLocation = \Phpfox::getService('teams')->getPage($this->_aCallback['item_id']);
    $aAdmins = Phpfox::getService('teams')->getPageAdmins($this->_aCallback['item_id']);
    foreach ($aAdmins as $aAdmin) {
        if (Phpfox::getUserId() == $aAdmin['user_id']) {
            continue;
        }

        Phpfox::getLib('mail')->to($aAdmin['user_id'])
            ->subject(['full_name_wrote_a_comment_on_location_title', [
                'full_name' => \Phpfox::getUserBy('full_name'),
                'title' => $aLocation['title']
            ]])
            ->message(['full_name_wrote_a_comment_on_location_link', [
                'full_name' => \Phpfox::getUserBy('full_name'),
                'title' => $aLocation['title'],
                'link' => $sLink
            ]])
            ->notification('comment.add_new_comment')
            ->send();
    }
}
