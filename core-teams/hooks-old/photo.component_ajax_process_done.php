<?php

if (isset($aCallback['module']) && $aCallback['module'] == 'teams') {
    // temporary save content, because function send of mail clean all => cause issue when use plugin in ajax
    $content = ob_get_contents();
    ob_clean();

    // validate whom to send notification
    $aTeam = Phpfox::getService('teams')->getPage($aPhoto['team_id']);
    if ($aTeam) {
        $sLink = Phpfox::getService('teams')->getUrl($aTeam['page_id'], $aTeam['title'], $aTeam['vanity_url']);
        $postedUserFullName = Phpfox::getUserBy('full_name');

        $varPhraseTitle = 'full_name_post_some_images_on_team_title';
        $varPhraseLink = 'full_name_post_some_images_on_team_title_link';

        // get all admins (include owner) and send notification
        $aAdmins = Phpfox::getService('teams')->getPageAdmins($aTeam['page_id']);
        foreach ($aAdmins as $aAdmin) {
            if ($aPhoto['user_id'] == $aAdmin['user_id']) { // is owner of photo
                continue;
            }

            if ($aTeam['user_id'] == $aAdmin['user_id']) { // is owner of team
                $varPhraseTitle = 'full_name_post_some_images_on_your_team_title';
                $varPhraseLink = 'full_name_post_some_images_on_your_team_title_link';
            }

            Phpfox::getLib('mail')->to($aAdmin['user_id'])
                ->subject([$varPhraseTitle, [
                    'full_name' => $postedUserFullName,
                    'title' => $aTeam['title']
                ]])
                ->message([$varPhraseLink, [
                    'full_name' => $postedUserFullName,
                    'link' => $sLink,
                    'title' => $aTeam['title']
                ]])
                ->notification('comment.add_new_comment')
                ->send();

            if (Phpfox::isModule('notification')) {
                Phpfox::getService('notification.process')->add('teams_post_image', $aPhoto['photo_id'], $aAdmin['user_id']);
            }
        }
    }

    // return content
    echo $content;
}

