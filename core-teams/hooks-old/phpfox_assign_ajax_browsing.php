<?php

if (defined('PHPFOX_IS_PAGES_VIEW') && defined('PHPFOX_PAGES_ITEM_TYPE')) {

	$menuService = '';
	switch (PHPFOX_PAGES_ITEM_TYPE) {
		case 'teams':
			$menuService = 'teams.privacy';
			break;
	}
	
	$defModule = array('game');
	
	if(PHPFOX_PAGES_ITEM_TYPE == 'teams'){
		if (Phpfox::isModule($sModule) && in_array($sModule, $defModule)) {

			$aPage = $oTpl->getVar('aPage');
			
			$funcall = 'get'.$sModule.'SubMenu';
			
			
			$aMenu = Phpfox::getService($menuService)->$funcall($aPage);

			if (is_array($aMenu)) {
				foreach ($aMenu as $iKey => $aSubMenu) {
					$aMenu[$iKey]['module'] = $sModule;
					if (isset($aSubMenu['phrase'])) {
						if (Core\Lib::phrase()->isPhrase($sModule . '.' . $aSubMenu['phrase'])) {
							$aMenu[$iKey]['var_name'] = $aSubMenu['phrase'];
						} else {
							$aMenu[$iKey]['text'] = $aSubMenu['phrase'];
						}
						continue;
					}
					switch ($sModule) {
						case 'event':
							$aMenu[$iKey]['var_name'] = 'menu_create_new_' . $sModule;
							break;
						case 'forum':
							$aMenu[$iKey]['var_name'] = 'post_a_new_thread';
							break;
						case 'music':
							$aMenu[$iKey]['var_name'] = 'menu_upload_a_song';
							break;
						case 'photo':
							$aMenu[$iKey]['var_name'] = 'upload_a_new_image';
							break;
						case 'videochannel':
							$aMenu[$iKey]['var_name'] = 'can_create_ontrack';
							break;
						default:
							$aMenu[$iKey]['var_name'] = 'menu_add_new_' . $sModule;
					}
				}
			}
			$aSubMenus = $aMenu;
			$oTpl->assign([
                    'aSubMenus' => $aSubMenus,
                ]
            );
		}
	
	}

}
