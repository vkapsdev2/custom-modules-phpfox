<?php

if (array_key_exists('callback_module', $aVals) && $aVals['callback_module'] == 'teams') {
    // temporary save content, because function send of mail clean all => cause issue when use plugin in ajax
    $content = ob_get_contents();
    ob_clean();

    // validate whom to send notification
    $aTeam = Phpfox::getService('teams')->getPage($aVals['callback_item_id']);
    if ($aTeam) {
        $iLinkId = Phpfox::getService('link.process')->getInsertId();
        $aLink = Phpfox::getService('link')->getLinkById($iLinkId);
        if($aLink) {
            $sLinkUrl = $aLink['redirect_link'];
            $postedUserFullName = Phpfox::getUserBy('full_name');

            $varPhraseTitle = 'full_name_posted_a_link_on_team_title';
            $varPhraseLink = 'full_name_posted_a_link_on_team_link';

            // get all admins (include owner), send email and notification
            $aAdmins = Phpfox::getService('teams')->getPageAdmins($aVals['callback_item_id']);
            foreach ($aAdmins as $aAdmin) {
                if ($aLink['user_id'] == $aAdmin['user_id']) { // is owner of link
                    continue;
                }

                if ($aTeam['user_id'] == $aAdmin['user_id']) { // is owner of team
                    $varPhraseTitle = 'full_name_posted_a_link_on_your_team_title';
                    $varPhraseLink = 'full_name_posted_a_link_on_your_team_link';
                }

                $aSubjectPhrase = [$varPhraseTitle, [
                    'full_name' => $postedUserFullName,
                    'title' => $aTeam['title']
                ]];
                $aMessagePhrase = [$varPhraseLink, [
                    'full_name' => $postedUserFullName,
                    'title' => $aTeam['title'],
                    'link' => $sLinkUrl
                ]];

                Phpfox::getLib('mail')->to($aAdmin['user_id'])
                    ->subject($aSubjectPhrase)
                    ->message($aMessagePhrase)
                    ->notification('comment.add_new_comment')
                    ->send();

                if (Phpfox::isModule('notification')) {
                    Phpfox::getService('notification.process')->add('teams_comment_link', $iLinkId, $aAdmin['user_id']);
                }
            }
        }
    }

    // return content
    echo $content;
}

