<?php
$aValidation = [
    'teams_limit_per_category' => [
        'title' => _p('validator_teams_limit_per_category'),
        'def' => 'int:required',
        'min' => 0
    ],
];
