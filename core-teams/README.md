# Teams

## App Info

- `App name`: Teams
- `Version`: 4.7.8
- `Link on Store`: https://store.phpfox.com/product/2007/teams
- `Demo site`: https://v4.phpfox.com
- `Owner`: phpFox

## Installation Guide

Please follow below steps to install new phpFox Teams app:

1. Install the Teams app from the store.

2. Remove files no longer used (at Admin Panel > Maintenance > Remove files no longer used).

3. Clear `Cache` and `Rebuild Core Theme` on your site.

Congratulation! You have completed installation process.
