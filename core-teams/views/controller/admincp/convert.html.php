<?php
defined('PHPFOX') or exit('NO DICE!');
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title">{_p var='convert_old_teams'}</div>
    </div>
    <div class="panel-body">
        {if $iConvertedUserId}
        <div class="alert alert-info">
            {_p var="Teams are converting. Remaining teams now is:"} {$iNumberTeams}
        </div>
        {else}
            {if $iNumberTeams}
            <div class="well well-lg">
                <h2>
                    {_p var="you_have_number_old_teams_from_pages", number=$iNumberTeams}.
                    <br/>
                    {_p var="Do you want to convert to new teams system?"}:
                </h2>
                <div class="well-sm">
                    {if $iNumberTeams < 500}
                    <a class="btn-success btn" href="{url link='admincp.teams.convert', convert=true}">{_p var="Yes, convert directly"}</a>
                    {/if}
                    <a class="btn-danger btn" href="{url link='admincp.teams.convert', convert=true  cron=true}">{_p var="Yes, convert via cron job"}</a>
                </div>
            </div>
            <div class="well well-sm">
                {_p var="Learn more about this"} <a target="_blank" href="https://docs.phpfox.com/display/FOX4MAN/Enabling+and+Managing+the+Teams+App">{_p var="Visit here"}</a>
            </div>
            {/if}
        {/if}

        {if !$iNumberTeams}
        <div class="alert alert-info">
            {_p var="There is no old team to convert"}.
        </div>
        {/if}
    </div>
</div>