<?php
    defined('PHPFOX') or exit('NO DICE!');
?>
<div class="grousp-block-members teams-block-members">
    <div class="item-group-search-header item-team-search-header">
        <div class="item-group-search-member input-group item-team-search-member input-team">
            <input class="form-control" type="search" placeholder="{_p var='search_member'}"
                   data-app="core_teams" data-action-type="keyup" data-action="search_member"
                   data-result-container=".search-member-result" data-container=".search-member-result"
                   data-listing-container=".teams-member-listing" data-team-id="{$iTeamId}"
            />
            <span class="input-group-btn input-team-btn" aria-hidden="true">
                <button class="btn " type="submit">
                     <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="page_section_menu page_section_menu_header">
        <ul class="nav nav-tabs nav-justified">
            <li {if $sActiveTab == 'all'}class="active"{/if}>
                <a data-toggle="tab" href="#all" data-app="core_teams" data-action-type="click"
                   data-action="change_tab" data-tab="all" data-container=".teams-member-listing"
                   data-team-id="{$iTeamId}" data-result-container=".search-member-result"
                >
                    {_p var='all_members'} <span class="member-count" id="all-members-count">({$iTotalMembers})</span>
                </a>
            </li>
            {if $bIsAdmin}
                <li {if $sActiveTab == 'pending'}class="active"{/if}>
                    <a data-toggle="tab" href="#pending" data-app="core_teams" data-action-type="click"
                       data-action="change_tab" data-tab="pending" data-container=".teams-member-listing"
                       data-team-id="{$iTeamId}" data-result-container=".search-member-result"
                    >
                        {_p var='pending_requests'} <span class="member-count" id="pending-members-count">({$iTotalPendings})</span>
                    </a>
                </li>
            {/if}
            {if isset($iTotalAdmins) && $bCanViewAdmins}
                <li {if $sActiveTab == 'admin'}class="active"{/if}>
                    <a data-toggle="tab" href="#admin" data-app="core_teams" data-action-type="click"
                       data-action="change_tab" data-tab="admin" data-container=".teams-member-listing"
                       data-team-id="{$iTeamId}" data-result-container=".search-member-result"
                    >
                        {_p var='team_admins'} <span class="member-count" id="admin-members-count">({$iTotalAdmins})</span>
                    </a>
                </li>
            {/if}
        </ul>
    </div>

    <div class="tab-content groups-member-container groups-member-listing teams-member-container teams-member-listing">
        {module name='teams.search-member' tab=$sActiveTab team_id=$iTeamId container='.teams-member-listing'}
    </div>

    <div class="search-member-result groups-member-container teams-member-container hide"></div>
    <div class="groups-searching teams-searching hide">
        <i class="fa fa-spinner fa-spin"></i>
    </div>
</div>

{if $bIsAdmin && $iTotalMembers}
    {moderation}
{/if}