<?php
    defined('PHPFOX') or exit('NO DICE!');
?>
<div class="grousp-block-members teams-block-members">
{*<div class="item-group-search-header item-team-search-header">
        <div class="item-group-search-member input-group item-team-search-member input-team">
            <input class="form-control" type="search" placeholder="{_p var='search_member'}"
                   data-app="core_teams" data-action-type="keyup" data-action="search_member"
                   data-result-container=".search-member-result" data-container=".search-member-result"
                   data-listing-container=".teams-member-listing" data-team-id="{$iTeamId}"
            />
            <span class="input-group-btn input-team-btn" aria-hidden="true">
                <button class="btn " type="submit">
                     <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
    </div>
*}

    <div class="tab-content groups-member-container groups-member-listing teams-member-container teams-member-listing">
        {module name='teams.search-fanmember' tab=$sActiveTab team_id=$iTeamId container='.teams-member-listing'}
    </div>

    <div class="search-member-result groups-member-container teams-member-container hide"></div>
    <div class="groups-searching teams-searching hide">
        <i class="fa fa-spinner fa-spin"></i>
    </div>
</div>

{if $bIsAdmin && $iTotalMembers}
    {moderation}
{/if}