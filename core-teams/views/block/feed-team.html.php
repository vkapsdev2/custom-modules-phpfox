<div class="team-app core-feed-item">
    <div class="item-outer">
        <!-- image thumbnail -->
        <div class="item-avatar">
            {if !empty($aTeam.image_path)}
            <span class="item-media-src" style="background-image: url({img server_id=$aTeam.server_id title=$aTeam.title path='pages.url_image' file=$aTeam.image_path suffix='_200_square' no_default=false max_width=200 time_stamp=true return_url=true})"  alt="{$aTeam.title}"></span>
            {else}
            {img thickbox=true server_id=$aTeam.image_server_id title=$aTeam.title path='pages.url_image' file=$aTeam.pages_image_path suffix='_200_square' no_default=false max_width=200 time_stamp=true}
            {/if}
        </div>
        <div class="item-inner">
            <div class="item-title">      
                <a href="{$aTeam.team_url}" class="core-feed-title line-1" itemprop="url">{$aTeam.title}</a>
            </div>
            <div class="item-info-wrapper core-feed-minor">
                <span class="item-info">
                    {if $aTeam.parent_category_name}
                        <a href="{$aTeam.type_link}">
                            {if Phpfox::isPhrase($this->_aVars['aPage']['parent_category_name'])}
                            {_p var=$aTeam.parent_category_name}
                            {else}
                            {$aTeam.parent_category_name|convert}
                            {/if}
                        </a> »
                        {/if}
                        {if $aTeam.category_name}
                        <a href="{$aTeam.category_link}">
                            {if Phpfox::isPhrase($this->_aVars['aPage']['category_name'])}
                            {_p var=$aTeam.category_name}
                            {else}
                            {$aTeam.category_name|convert}
                            {/if}
                        </a>
                        {/if}
                </span>
                <span class="item-info">{$aTeam.total_like} {if $aTeam.total_like > 1}{_p var='teams_members'}{else}{_p var='teams_member'}{/if}</span>
            </div>
            <div class="item-desc-wrapper">
                <div class="item-desc core-feed-description line-2">
                    {$aTeam.text_parsed|parse}
                </div>
                <div class="item-action js_team_feed_action_content" data-team-id="{$aTeam.page_id}">
                    <!-- Please check and show only 1 button for suitable case -->
                    <span class="item-action-btn {if $aTeam.is_joined_team}hide{/if}">
                        <a href="javascript:void(0);" class="btn btn-default btn-sm" onclick="$Core.Teams.processTeamFeed(this);" data-type="like">{_p var='teams_join_team'}</a>
                    </span>
                    <span class="item-action-btn {if !$aTeam.is_joined_team}hide{/if}">
                        <a href="javascript:void(0);" class="btn btn-default btn-sm" onclick="$Core.Teams.processTeamFeed(this);" data-type="unlike">{_p var='teams_joined_team'}</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>