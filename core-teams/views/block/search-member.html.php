<!-- tab all members-->
{if $sTab == 'all'}
	
	<table id="tblplayers" class="dont-unbind-children" cellpadding="0" cellspacing="0" border="1">
		<tr>
		{if $bIsAdmin}<th style="width:10%;">Settings</th>{/if}
		<th>Name </th>
		<th>Status</th>
		<th>Position</th>
		</tr>
		
    {foreach from=$aMembers key=iKey item=aUser}
    <article class="groups-member teams-member" id="teams-member-{$aUser.user_id}">
		<tr>
			{if $bIsAdmin}
			<td style="width:10%;">
				<div class="dropdown item-bar-action">
					<a role="button" data-toggle="dropdown" class="btn btn-sm s-4" aria-expanded="true">
						<span class="ico ico-gear-o"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a role="button" data-app="core_teams" data-action-type="click" data-action="remove_member"
							   data-message="{_p var='are_you_sure_you_want_to_delete_this_user_from_the_team'}"
							   data-team-id="{$iTeamId}" data-user-id="{$aUser.user_id}"
							>
								<i class="fa fa-trash"></i> {_p var='delete'}
							</a>
						</li>
					</ul>
				</div>
			</td>
			{/if}
			<td>
				{img user=$aUser suffix='_50_square'}
				{$aUser|user}
			</td>
			<td><span id="player_status_{$aUser.like_id}">{$aUser.player_status}</span>
			{if $bIsOwner || $aUser.user_id == Phpfox::getUserId()}
				<div><label>Change Status</label>
				<select name="change_status" onchange="changePlayerStatus(this.value,{$aUser.like_id});">
					<option value="A" {if $aUser.player_status == 'A'}selected="selected" {/if}>A</option>
					<option value="I" {if $aUser.player_status == 'I'}selected="selected" {/if}>I</option>
					<option value="DL" {if $aUser.player_status == 'DL'}selected="selected" {/if}>DL</option>
					<option value="IR" {if $aUser.player_status == 'IR'}selected="selected" {/if}>IR</option>
				</select>
				</div>
			{/if}
			</td>
			<td>
			<span>{if $aUser.player_order == '9999999'}{else}{$aUser.player_order}{/if}</span>
			<input type="hidden" class="player_order" likid="{$aUser.like_id}" value="{$aUser.player_order}"/>
			</td>
		</tr>
        
    </article>
    {/foreach}
	 </table>
	 <div class="extra_info">* Drag and drop to change positions</div>
	{if $bIsAdmin}
		{literal}
		 <script type="text/javascript">
$Ready(function () {
    $("#tblplayers").sortable({
        items: 'tr:not(tr:first-child)',
        cursor: 'pointer',
        axis: 'y',
        dropOnEmpty: false,
        start: function (e, ui) {
            ui.item.addClass("selected");
        },
        stop: function (e, ui) {
            ui.item.removeClass("selected");
            $(this).find("tr").each(function (index) {
                if (index > 0) {
                    //$(this).find("td").eq(3).html(index);
                    $(this).find("td").eq(3).find("span").html(index);
                    $(this).find("td").eq(3).find("input").val(index);
                    
					$.ajaxCall('teams.changeposition', 'like_id='+$(this).find("td").eq(3).find("input").attr('likid')+'&player_order='+index);
                }
            });
			
        }
    });
});
</script>
		{/literal}
{/if}
{literal}
<script type="text/javascript">
function changePlayerStatus(val,likId)
{
	$("#player_status_"+likId).text(val);
	$.ajaxCall('teams.changePlayerStatus', 'like_id='+likId+'&player_status='+val);
}
</script>
<style type="text/css">
	#tblplayers
	{
		width:100%;
	}
	#tblplayers th, #tblplayers td
    {
        width: 25%;
        padding: 5px;
        border: 1px solid #ccc;
    }
    .selected
    {
        background-color: #666;
        color: #fff;
    }
</style>
{/literal}
<!-- tab pending members -->
{elseif $sTab == 'pending'}
    {if !count($aMembers)}
    <div class="container">
        <div class="alert alert-info">
            {_p var='there_is_no_pending_request'}
        </div>
    </div>
    {/if}
    {foreach from=$aMembers item=aUser}
    <article class="teams-member" id="teams-member-{$aUser.user_id}">
        {template file='user.block.rows'}

        <div class="dropdown item-bar-action">
            <a role="button" data-toggle="dropdown" class="btn btn-sm s-4" aria-expanded="true">
                <span class="ico ico-gear-o"></span>
            </a>

            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    <a role="button" onclick="$.ajaxCall('teams.approvePendingRequest', 'sign_up={$aUser.signup_id}&user_id={$aUser.user_id}')">
                        <i class="fa fa-check-square-o"></i> {_p var='approve'}
                    </a>
                </li>
                <li>
                    <a role="button" data-app="core_teams" data-action-type="click" data-action="remove_pending"
                       data-message="{_p var='are_you_sure_you_want_to_delete_this_user_from_the_team'}"
                       data-signup-id="{$aUser.signup_id}" data-user-id="{$aUser.user_id}"
                    >
                        <i class="fa fa-trash"></i> {_p var='delete'}
                    </a>
                </li>
            </ul>
        </div>
    </article>
    {/foreach}
<!-- tab admin -->
{elseif $sTab == 'admin'}
    {foreach from=$aMembers item=aUser}
    <article class="teams-member" id="teams-member-{$aUser.user_id}">
        {template file='user.block.rows'}

        {if $bIsOwner && $aUser.user_id != Phpfox::getUserId()}
        <div class="dropdown item-bar-action">
            <a role="button" data-toggle="dropdown" class="btn btn-sm s-4" aria-expanded="true">
                <span class="ico ico-gear-o"></span>
            </a>

            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    <a role="button" data-app="core_teams" data-action-type="click" data-action="remove_admin"
                       data-message="{_p var='are_you_sure_you_want_to_remove_admin_role_from_this_user'}"
                       data-team-id="{$iTeamId}" data-user-id="{$aUser.user_id}"
                    >
                        <i class="fa fa-trash"></i> {_p var='remove_admin'}
                    </a>
                </li>
                <li>
                    <a role="button" data-app="core_teams" data-action-type="click" data-action="remove_member"
                       data-message="{_p var='are_you_sure_you_want_to_delete_this_user_from_the_team'}"
                       data-team-id="{$iTeamId}" data-user-id="{$aUser.user_id}"
                    >
                        <i class="fa fa-trash"></i> {_p var='delete'}
                    </a>
                </li>
            </ul>
        </div>
        {/if}
    </article>
    {/foreach}
{/if}
<!-- not show pagination when search member -->
{if $sSearch && !count($aMembers) && $sTab != 'pending'}
<div class="container">
    <div class="alert alert-info">
        {_p var='there_is_no_members_found'}
    </div>
</div>
{/if}

{if !$sSearch}
{pager}
{/if}