<?php
defined('PHPFOX') or exit('NO DICE!');
?>
<div class="page-block-action block">
    <div class="title">
        {_p('Team Info')}
    </div>
    <div class="content item-group-about item-team-about {if !isset($aUser)}team-hide-founder{/if}">
        {if isset($aUser)}
        <div class="item-outer">
            <div class="user_rows_image">
                {img user=$aUser suffix='_50_square'}
            </div>
            <div class="item-inner">
                <div class="item-title">{$aUser|user}</div>
				<div class="item-info">{_p var='Manager'}</div>
            </div>
        </div>
        {/if}
		<div class="item-outer mt-2">
			<div class="item-title">
				Team Region
			</div>
			<div class="item-inner">
				<div class="item-info">{$aPage.team_region}</div>
			</div>
		</div>
		<div class="item-outer">
			<div class="item-title">
				Team Type
			</div>
			<div class="item-inner">
				<div class="item-info">{if $aPage.team_type == 1}Ladder{elseif $aPage.team_type == 2}Cashout{/if}</div>
			</div>
		</div>
			
        {if $hasPermToViewPublishDate}
        <div class="item-publish-date">
            <span class="item-title">{_p var='pages_publish_date_title'}: </span>
            <span class="item-date">{$aPage.time_stamp|convert_time}</span>
        </div>
        {/if}

        {if !empty($page.text_parsed)}
        <div class="item-desc item_view_content">
        {$page.text_parsed|parse|shorten:170:'more':true}
        </div>
        {/if}
    </div>
</div>