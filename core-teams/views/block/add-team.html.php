<?php
defined('PHPFOX') or exit('NO DICE!');
?>
<form class="form group-add-modal team-add-modal" method="post" data-app="core_teams" data-action-type="submit" data-action="add_team_process">
    <input type="hidden" name="val[type_id]" value="{$iTypeId}">
    <div class="group-add-modal-outer team-add-modal-outer">
        <div class="group-category-photo team-category-photo"
             {if !empty($aMainCategory.image_path)}
             style="background-image: url('{img server_id=$aMainCategory.image_server_id path='sports.url_image' file=$aMainCategory.image_path return_url=true}')"
             {else}
             style="background-image: url('{img path='core.path_actual' file='PF.Site/Apps/core-sports/assets/image/no_image.png' return_url=true}')"
             {/if}
        >
            <div class="group-category-inner team-category-inner">
                <div class="group-category-title team-category-title">
                    {_p var=$aMainCategory.name}
                </div>
                {if !$bNoSubCategories}
                    <div class="group-category-title-sub team-category-title-sub">
                        {_p var='you_can_choose_sub_category_optional'}
                    </div>
                    <div class="form-group form-team team-category-select group-category-select js_core_init_selectize_form_team">
                        <select name="val[category_id]" class="form-control" id="select_sub_category_id">
                            <option value="0" id="no-select-sub-category">{_p var='sub_category'}</option>
                            {foreach from=$aSubCategories item=aSubCategory}
                                <option value="{$aSubCategory.category_id}" class="select-category-{$aCategory.type_id}" {if $iTypeId != $aCategory.type_id}style="display: none"{/if}>{_p var=$aSubCategory.name}</option>
                            {/foreach}
                        </select>
                    </div>
                {/if}
            </div>
        </div>
    </div>

    <div class="alert alert-danger" id="add_team_error_messages" style="display: none;"></div>
	
	<div class="group-team form-team col-xs-12 group-category-input team-category-input">
        <label for="team_region" class="">{_p var="Team Region"}</label>
        <div>
            <input id="team_region" name="val[team_region]" class="form-control col-xs-9" maxlength="25" autofocus required>
        </div>
    </div>
	
	<div class="form-group form-team col-xs-12 group-privacy-select team-privacy-select">
        <label for="team-type" class="">{_p var="Team Type"}</label>
        <select name="val[team_type]" class="form-control" id="team-type" required>
            <option value="">{_p var='Select Type'}</option>
            <option value="1">{_p var='Ladder'}</option>
			<option value="2">{_p var='Cashout'}</option>
        </select>
    </div>
	
    <div class="group-team form-team col-xs-12 group-category-input team-category-input">
        <label for="title" class="">{_p var="team_name"}</label>
        <div>
            <input id="title" name="val[title]" class="form-control col-xs-9" maxlength="25" autofocus required>
            <span class="help-block">{_p var='Maximum 25 characters'}</span>
        </div>
    </div>
     
	
	
    <div class="form-group form-team col-xs-12 group-privacy-select team-privacy-select" style="display:none;">
        <label for="team-type" class="">{_p var="type_privacy"}</label>
        <select name="val[reg_method]" class="form-control" id="team-type">
            <option value="1">{_p var='closed_team'}</option>
			<option value="0">{_p var='public_team'}</option>
            <option value="2">{_p var='secret_team'}</option>
        </select>
    </div>

    <div class="group-category-button team-category-button">
        <input type="submit" class="btn btn-primary btn-round" value="{_p var='create_team'}">
        <a class="btn btn-default btn-round" onclick="return js_box_remove(this);">{_p var='cancel'}</a>
    </div>
</form>