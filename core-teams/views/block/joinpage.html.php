<?php
defined('PHPFOX') or exit('NO DICE!');
?>
{if !Phpfox::getUserBy('profile_page_id') && isset($aPage)}
    {if empty($aPage.is_reg)}
        {if !empty($aPage.is_liked)}
            <div class="dropdown">
                <a class="btn btn-default btn-icon item-icon-joined btn-round">
                    <span class="ico ico-check"></span>
                    {_p var='joined'}
                    
                </a>
				{* data-toggle="dropdown"
                <ul class="dropdown-menu dropdown-menu-right">
					<span class="ml-1 ico ico-caret-down"></span>
                    <li>
                        <a role="button" onclick="$.ajaxCall('like.delete', 'type_id=teams&amp;item_id={$aPage.page_id}&amp;reload=1'); return false;">
                            <span class="ico ico-close"></span>
                            {_p var='unjoin'}
                        </a>
                    </li>
                </ul>
				*}
            </div>
        {else}
			<div class="dropdown">
                <a class="btn btn-default btn-icon item-icon-joined btn-round" data-toggle="dropdown">
                    <span class="ico ico-check"></span>
                    {_p var='Join Option'}
					<span class="ml-1 ico ico-caret-down"></span>
                </a>
				<ul class="dropdown-menu dropdown-menu-right">
					
                    <li>
                       <a role="button" onclick="$(this).remove(); $.ajaxCall('teams.addFan', 'type_id=teams&amp;item_id={$aPage.page_id}&amp;reload=1'); return false;">
							<i class="fa fa-rss"></i> {_p var='Follow'}
						</a>
                    </li>
					<li>
                        <a role="button" onclick="$(this).remove(); $.ajaxCall('teams.signup', 'page_id={$aPage.page_id}'); return false;">
							<span class="ico ico-plus"></span>{_p var='Join as Player'}
						</a>
                    </li>
                </ul>
			</div>	
            
        {/if}
    {else}
        <div class="dropdown">
            <a class="btn btn-default btn-icon item-icon-joined btn-round">
                <span class="ico ico-sandclock-goingon-o"></span>
                {_p var='requested'}
                
            </a>
			{*data-toggle="dropdown" <span class="ml-1 ico ico-caret-down"></span>
            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    <a role="button" onclick="$.ajaxCall('teams.deleteRequest', 'page_id={$aPage.page_id}'); return false;">
                        <span class="ico ico-close"></span>
                        {_p var='delete_request'}
                    </a>
                </li>
            </ul>*}
        </div>
    {/if}
{/if}
