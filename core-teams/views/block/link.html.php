<?php
defined('PHPFOX') or exit('NO DICE!');
?>
{plugin call='teams.block_link_actions_1'}
{if $aPage.bCanApprove}
    <li><a href="#" onclick="$.ajaxCall('teams.approve', 'page_id={$aPage.page_id}');"><span class="ico ico-check-square-alt mr-1"></span>{_p var='approve'}</a></li>
{/if}
{if $aPage.bCanEdit}
    <li><a href="{url link='teams.add' id=$aPage.page_id}"><span class="ico ico-gear-form-o mr-1"></span>{_p('manage')}</a></li>
{/if}
{if $aPage.bCanFeature}
    <li><a id="js_feature_{$aPage.page_id}" {if $aPage.is_featured} style="display:none;"{/if} href="#" title="{_p var='feature'}" onclick="$(this).hide(); $('#js_unfeature_{$aPage.page_id}').show(); $.ajaxCall('teams.feature', 'page_id={$aPage.page_id}&amp;type=1'); return false;"><span class="ico ico-diamond mr-1"></span>{_p var='feature'}</a></li>
    <li><a id="js_unfeature_{$aPage.page_id}" {if !$aPage.is_featured} style="display:none;"{/if} href="#" title="{_p var='un_feature'}" onclick="$(this).hide(); $('#js_feature_{$aPage.page_id}').show(); $.ajaxCall('teams.feature', 'page_id={$aPage.page_id}&amp;type=0'); return false;"><span class="ico ico-diamond-o mr-1"></span>{_p var='unfeature'}</a></li>
{/if}
{if $aPage.bCanSponsor}
    <li>
        <a href="#" id="js_sponsor_{$aPage.page_id}" onclick="$('#js_sponsor_{$aPage.page_id}').hide();$('#js_unsponsor_{$aPage.page_id}').show();$.ajaxCall('teams.sponsor','page_id={$aPage.page_id}&type=0', 'GET'); return false;" style="{if $aPage.is_sponsor != 1}display:none;{/if}">
            <span class="ico ico-sponsor mr-1"></span>{_p var='unsponsor'}
        </a>
        <a href="#" id="js_unsponsor_{$aPage.page_id}" onclick="$('#js_sponsor_{$aPage.page_id}').show();$('#js_unsponsor_{$aPage.page_id}').hide();$.ajaxCall('teams.sponsor','page_id={$aPage.page_id}&type=1', 'GET'); return false;" style="{if $aPage.is_sponsor == 1}display:none;{/if}">
            <span class="ico ico-sponsor mr-1"></span>{_p var='sponsor'}
        </a>
    </li>
{elseif $aPage.bCanPurchaseSponsor}
    <li>
        <a id="js_unsponsor_{$aPage.page_id}" href="{permalink module='ad.sponsor' id=$aPage.page_id}section_teams/" style="{if $aPage.is_sponsor == 1}display:none;{/if}">
            <span class="ico ico-sponsor mr-1"></span>{_p var='sponsor_this_team'}
        </a>
        <a href="#" id="js_sponsor_{$aPage.page_id}" onclick="$('#js_sponsor_{$aPage.page_id}').hide();$('#js_unsponsor_{$aPage.page_id}').show();$.ajaxCall('teams.sponsor','page_id={$aPage.page_id}&type=0', 'GET'); return false;" style="{if $aPage.is_sponsor != 1}display:none;{/if}">
            <span class="ico ico-sponsor mr-1"></span>{_p var='unsponsor'}
        </a>
    </li>
{/if}
{plugin call='teams.block_link_actions_2'}
{if $aPage.bCanDelete}
    <li class="item_delete">
        <a href="{url link='teams' delete=$aPage.page_id}" class="no_ajax_link sJsConfirm">
            <span class="ico ico-trash-alt-o mr-1"></span>
            {_p('delete')}
        </a>
    </li>
{/if}
{plugin call='teams.block_link_actions_3'}