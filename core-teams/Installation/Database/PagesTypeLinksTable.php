<?php

namespace Apps\PHPfox_Teams\Installation\Database;

use Core\App\Install\Database\Field;
use Core\App\Install\Database\Table;

class PagesTypeLinksTable extends Table
{
    /**
     * Set name of this table, can't missing
     */
    protected function setTableName()
    {
        $this->_table_name = 'pages_type_links';
    }

    /**
     * Set all fields of table
     */
    protected function setFieldParams()
    {
        $this->_aFieldParams = [
            'type_links_id' => [
                'primary_key' => true,
                'auto_increment' => true,
                'type' => Field::TYPE_SMALLINT,
                'type_value' => 4,
                'other' => 'UNSIGNED NOT NULL'
            ],
            'type_id' => [
                'type' => Field::TYPE_INT,
                'type_value' => 10,
                'other' => 'NOT NULL DEFAULT 1'
            ],
			'user_id' => [
                'type' => Field::TYPE_INT,
                'type_value' => 10,
                'other' => 'NOT NULL DEFAULT 1'
            ],
            'time_stamp' => [
                'type' => Field::TYPE_INT,
                'type_value' => 10,
                'other' => 'UNSIGNED NOT NULL'
            ]
        ];
    }

    /**
     * Set keys of table
     */
    protected function setKeys()
    {
        $this->_key = [
        ];
    }
}
