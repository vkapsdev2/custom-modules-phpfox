<?php

namespace Apps\PHPfox_Teams\Installation\Version;

class v464
{
    public function process()
    {
        // remove duplicated menu
        db()->delete(':menu', [
            'm_connection' => 'main',
            'module_id'    => 'teams',
            'product_id'   => 'phpfox',
            'var_name'     => 'menu_teams',
            'url_value'    => '/teams'
        ]);
    }
}
