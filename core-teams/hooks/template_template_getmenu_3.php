<?php
if (!Phpfox::getUserParam('teams.pf_location_browse')) {
    foreach ($aMenus as $index => $aMenu) {
        if ($aMenu['m_connection'] == 'main' && $aMenu['module'] == 'teams') {
            unset($aMenus[$index]);
        }
    }
}
