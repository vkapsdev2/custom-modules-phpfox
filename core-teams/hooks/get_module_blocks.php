<?php
if (defined('PHPFOX_IS_PAGES_VIEW') && defined('PHPFOX_PAGES_ITEM_TYPE') && PHPFOX_PAGES_ITEM_TYPE == 'teams' && $iId == 4) {
    $aBlocks[2][] = ['type_id' => 0, 'component' => 'teams.pending', 'params' => []];
}

$sModuleName = Phpfox::getLib('module')->getModuleName();
if ($iId == 11 && Phpfox_Component::__getParam('show_location_cover')) {
    $aBlocks[11][] = ['type_id' => 0, 'component' => 'teams.photo', 'params' => [
        'aPage' => Phpfox_Component::__getParam('location_to_show_cover')
    ]];
}
