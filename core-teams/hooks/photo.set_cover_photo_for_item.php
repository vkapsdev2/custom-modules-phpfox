<?php

if (!empty($aVals['module_id']) && !empty($aVals['item_id']) && $aVals['module_id'] == 'teams' && Phpfox::isModule('teams')) {
    if (!Phpfox::getService('teams.process')->setCoverPhoto($aVals['item_id'], $iId, true)) {
        return $this->error(_p('Cannot set cover photo for this location.'));
    }
}
