<?php

if ($mUser) {
	$aRow = Phpfox::getService('user')->get($mUser, false);

	if ((isset($aRow['user_id']) && $aRow['profile_page_id'] > 0)) {
		if (Phpfox::isModule('teams') && Phpfox::getService('teams')->isPage($this->request()->get('req1'))) {
			return Phpfox_Module::instance()->setController('teams.view');
		}
	}
}