<?php
\Core\Queue\Manager::instance()
    ->addHandler('teams_member_join_notifications', '\Apps\Core_teams\Job\SendMemberJoinNotification')
    ->addHandler('teams_member_notifications', '\Apps\Core_teams\Job\SendMemberNotification')
    ->addHandler('teams_convert_old_location', '\Apps\Core_teams\Job\ConvertOldteams');
