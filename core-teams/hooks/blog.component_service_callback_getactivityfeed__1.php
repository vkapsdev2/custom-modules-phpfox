<?php

if (!defined('PHPFOX_IS_PAGES_VIEW') && ($aBlog['module_id'] == 'teams' && Phpfox::isModule('teams'))) {
		$aPage = db()->select('p.*, pu.vanity_url, ' . Phpfox::getUserField('u', 'parent_'))
			->from(':pages', 'p')
			->join(':user', 'u', 'p.page_id=u.profile_page_id')
			->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = p.page_id')
			->where('p.page_id=' . (int)$aBlog['item_id'])
			->execute('getSlaveRow');

		if (empty($aPage)) {
			return false;
		}

		$aRow['parent_user_name'] = Phpfox::getService($aBlog['module_id'])->getUrl($aPage['page_id'],
			$aPage['title'], $aPage['vanity_url']);
		$aRow['feed_table_prefix'] = 'pages_';
		if ($aBlog['user_id'] != $aPage['parent_user_id']) {
			$aRow['parent_user'] = Phpfox::getService('user')->getUserFields(true, $aPage, 'parent_');
			$aRow['feed_info'] ='';
		}
	}