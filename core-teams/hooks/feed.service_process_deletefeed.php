<?php
defined('PHPFOX') or exit('NO DICE!');

if (Phpfox_Request::instance()->get('module') == 'teams') {
    $aLocation = Phpfox::getService('teams')->getPage($aFeed['parent_user_id']);
    if (isset($aLocation['page_id']) && Phpfox::getService('teams')->isAdmin($aLocation)) {
        define('PHPFOX_FEED_CAN_DELETE', true);
    }
}

if(in_array($sType, ['teams_photo', 'teams_cover_photo']) && isset($aFeed['user_id'])) {
    $locationUser = Phpfox::getService('user')->getUser($aFeed['user_id'], 'profile_page_id');
    $aLocation = Phpfox::getService('teams')->getPage($locationUser['profile_page_id']);
    if (isset($aLocation['page_id']) && Phpfox::getService('teams')->isAdmin($aLocation)) {
        define('PHPFOX_FEED_CAN_DELETE', true);
    }
}
