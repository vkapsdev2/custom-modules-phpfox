<?php

		
if ($aRow['module_id'] == 'teams') {
	$aRow['parent_user_id'] = '';
	$aRow['parent_user_name'] = '';
}

$aReturnfeed_info = '';

if (empty($aRow['parent_user_id'])) {
	if ($bIsPhotoAlbum) {
		$aRow['name'] = Phpfox::getLib('locale')->convert($aRow['name']);
		$aReturn['feed_status'] = '';
		$aReturnfeed_info = _p('added_new_photos_to_gender_album_a_href_link_name_a', array(
			'gender' => Phpfox::getService('user')->gender($aItem['gender'], 1),
			'link' => Phpfox::permalink('photo.album', $aRow['album_id'], $aRow['name']),
			'name' => Phpfox::getLib('locale')->convert(Phpfox::getLib('parse.output')->shorten(htmlspecialchars($aRow['name']),
				(Phpfox::isModule('notification') ? Phpfox::getParam('notification.total_notification_title_length') : $this->_iFallbackLength),
				'...'))
		));
	} else {
		$aReturnfeed_info = (count($aListPhotos) > 1 ? _p('shared_a_few_photos') : _p('shared_a_photo'));
	}
}

		
if (!defined('PHPFOX_IS_PAGES_VIEW') && (($aRow['module_id'] == 'teams' && Phpfox::isModule('teams')))) {
		$aPage = db()->select('p.*, pu.vanity_url, ' . Phpfox::getUserField('u', 'parent_'))
			->from(':pages', 'p')
			->join(':user', 'u', 'p.page_id=u.profile_page_id')
			->leftJoin(Phpfox::getT('pages_url'), 'pu', 'pu.page_id = p.page_id')
			->where('p.page_id=' . (int)$aRow['group_id'])
			->execute('getSlaveRow');

		if (empty($aPage)) {
			return false;
		}

		$aReturn['parent_user_name'] = Phpfox::getService($aRow['module_id'])->getUrl($aPage['page_id'],
			$aPage['title'], $aPage['vanity_url']);
		$aReturn['feed_table_prefix'] = 'pages_';
		if ($aRow['user_id'] != $aPage['parent_user_id']) {
			$aReturn['parent_user'] = Phpfox::getService('user')->getUserFields(true, $aPage, 'parent_');
			
		}
	}
	
	$aReturn['feed_info'] = (!empty($aReturnfeed_info) ? $aReturnfeed_info : '');
	
	if (!empty($sExtraTitle)) {
		$aReturn['feed_info'] = $aReturn['feed_info'] . ' - ' . $sExtraTitle;
	}
	
	