<?php

if (!defined('PHPFOX_PAGE_ITEM_TYPE_1')) {
    define('PHPFOX_PAGE_ITEM_TYPE_1', 'teams');
}

\Phpfox_Module::instance()->addServiceNames([
    'teams' => '\Apps\PHPfox_Teams\Service\Teams',
    'teams.sports' => '\Apps\PHPfox_Teams\Service\Sports',
    'teams.privacy' => '\Apps\PHPfox_Teams\Service\Privacy',
    'teams.facade' => '\Apps\PHPfox_Teams\Service\Facade',
    'teams.category' => '\Apps\PHPfox_Teams\Service\Category',
    'teams.process' => '\Apps\PHPfox_Teams\Service\Process',
    'teams.games' => '\Apps\PHPfox_Teams\Service\Games',
    'teams.teamfriend' => '\Apps\PHPfox_Teams\Service\Teamfriend',
    'teams.type' => '\Apps\PHPfox_Teams\Service\Type',
    'teams.api' => '\Apps\PHPfox_Teams\Service\Api',
    'teams.browse' => '\Apps\PHPfox_Teams\Service\Browse',
    'teams.callback' => '\Apps\PHPfox_Teams\Service\Callback',
])->addComponentNames('block', [
    'teams.searchteams' => '\Apps\PHPfox_Teams\Block\Searchteams',
    'teams.searchteammembers' => '\Apps\PHPfox_Teams\Block\TeamSearchteammembers',
    'teams.about' => '\Apps\PHPfox_Teams\Block\TeamAbout',
    'teams.admin' => '\Apps\PHPfox_Teams\Block\TeamAdmin',
    'teams.category' => '\Apps\PHPfox_Teams\Block\TeamCategory',
    'teams.events' => '\Apps\PHPfox_Teams\Block\TeamEvents',
    'teams.members' => '\Apps\PHPfox_Teams\Block\TeamMembers',
    'teams.members' => '\Apps\PHPfox_Teams\Block\TeamMembers',
    'teams.menu' => '\Apps\PHPfox_Teams\Block\TeamMenu',
    'teams.photo' => '\Apps\PHPfox_Teams\Block\TeamPhoto',
    'teams.profile' => '\Apps\PHPfox_Teams\Block\TeamProfile',
    'teams.widget' => '\Apps\PHPfox_Teams\Block\TeamWidget',
    'teams.cropme' => '\Apps\PHPfox_Teams\Block\TeamCropme',
    'teams.delete-category' => '\Apps\PHPfox_Teams\Block\TeamDeleteCategory',
    'teams.add-team' => '\Apps\PHPfox_Teams\Block\AddTeam',
    'teams.related' => '\Apps\PHPfox_Teams\Block\RelatedTeams',
    'teams.sports' => '\Apps\PHPfox_Teams\Block\SportsTeams',
    'teams.top' => '\Apps\PHPfox_Teams\Block\TopTeams',
    'teams.pending' => '\Apps\PHPfox_Teams\Block\Pending',
    'teams.search-member' => '\Apps\PHPfox_Teams\Block\SearchMember',
    'teams.search-fanmember' => '\Apps\PHPfox_Teams\Block\SearchFanmember',
    'teams.featured' => '\Apps\PHPfox_Teams\Block\Featured',
    'teams.sponsored' => '\Apps\PHPfox_Teams\Block\Sponsored',
    'teams.reassign-owner' => '\Apps\PHPfox_Teams\Block\ReassignOwner',
    'teams.feed-team' => '\Apps\PHPfox_Teams\Block\FeedTeamBlock',
    'teams.cover' => '\Apps\PHPfox_Teams\Block\Cover'
])->addComponentNames('ajax', [
    'PHPfox_Teams.ajax' => '\Apps\PHPfox_Teams\Ajax\Ajax',
    'teams.ajax' => '\Apps\PHPfox_Teams\Ajax\Ajax',
	'PHPfox_Teams.photo.ajax' => '\Apps\PHPfox_Teams\Ajax\Photo\Ajax',
    'teams.photo.ajax' => '\Apps\PHPfox_Teams\Ajax\Photo\Ajax',
])->addComponentNames('controller', [
    'teams.admincp.category' => '\Apps\PHPfox_Teams\Controller\Admin\CategoryController',
    'teams.admincp.add-category' => '\Apps\PHPfox_Teams\Controller\Admin\AddCategoryController',
    'teams.admincp.convert' => '\Apps\PHPfox_Teams\Controller\Admin\ConvertController',
    'teams.admincp.integrate' => '\Apps\PHPfox_Teams\Controller\Admin\IntegrateController',
    'teams.index' => '\Apps\PHPfox_Teams\Controller\IndexController',
    'teams.add' => '\Apps\PHPfox_Teams\Controller\AddController',
    'teams.all' => '\Apps\PHPfox_Teams\Controller\AllController',
    'teams.view' => '\Apps\PHPfox_Teams\Controller\ViewController',
    'teams.profile' => '\Apps\PHPfox_Teams\Controller\ProfileController',
    'teams.widget' => '\Apps\PHPfox_Teams\Controller\WidgetController',
    'teams.frame' => '\Apps\PHPfox_Teams\Controller\FrameController',
    'teams.photo' => '\Apps\PHPfox_Teams\Controller\PhotoController',
    'teams.members' => '\Apps\PHPfox_Teams\Controller\MembersController',
    'teams.fans' => '\Apps\PHPfox_Teams\Controller\FansController',
	'teams.photo.frame' => '\Apps\PHPfox_Teams\Controller\Photo\FrameController', 
])->addTemplateDirs([
    'teams' => PHPFOX_DIR_SITE_APPS . 'core-teams' . PHPFOX_DS . 'views',
])->addAliasNames('teams', 'PHPfox_Teams');

route('/teams/admincp/convert', function () {
    auth()->isAdmin(true);
    Phpfox_Module::instance()->dispatch('teams.admincp.convert');

    return 'controller';
});

group('/teams', function () {
    route('/admincp', function () {
        auth()->isAdmin(true);
        Phpfox_Module::instance()->dispatch('teams.admincp.category');

        return 'controller';
    });

    route('/admincp/add-category', 'teams.admincp.add-category');

    route('/admincp/integrate', 'teams.admincp.integrate');

    route('/category/:id/:name/*', 'teams.index')
        ->where([':id' => '([0-9]+)']);
    route('/sub-category/:id/:name/*', 'teams.index')
        ->where([':id' => '([0-9]+)']);
    route('/', 'teams.index');

    route('/profile/*', 'teams.view');

    route('/add/*', 'teams.add');

    route('/photo', 'teams.photo');

    route('/frame/*', 'teams.frame');

    route('/:name/*', 'teams.view')
        ->where([':name' => '([0-9]+)'])
        ->filter(function () {
            return true;
        });
	route('/photo/frame', 'teams.photo.frame');
});

// default cover
$sDefaultCover = flavor()->active->default_photo('teams_cover_default', true);
if (!$sDefaultCover) {
    $sDefaultCover = setting('core.path_actual') . 'PF.Site/Apps/core-teams/assets/img/default_teamcover.png';
}
Phpfox::getLib('setting')->setParam('teams.default_cover_photo', $sDefaultCover);
