# Core Teams :: Changelog

## Version 4.7.8 ##

### Information ###

- **Release Date:** July 01, 2020
- **Best Compatibility:** phpFox >= 4.7.6

### Improvements ###

- Support PHP 7.4

### Changed files ###

- M Ajax/Ajax.php
- M Controller/ViewController.php
- M Installation/Version/v474.php
- M Service/Callback.php
- M views/block/photo.html.php


## Version 4.7.7 ##

### Information ###

- **Release Date:** November 21, 2019
- **Best Compatibility:** phpFox >= 4.7.6

### Improvements ###

- Add a new Joined Pages page.
- Show Joined Teams when enabling Friends Only Community setting.
- Team detail - Public Team - Should show Join button in detail page when guest access.

### Changed files ###

- M Block/TeamProfile.php
- M Controller/IndexController.php
- M Controller/ViewController.php
- M Install.php
- M Service/Browse.php
- M Service/Callback.php
- M Service/Category.php
- M Service/Teams.php
- M Service/Process.php
- M assets/main.less
- A hooks/admincp.service_maintain_delete_files_get_list.php
- D hooks/link.component_service_callback_getactivityfeed__1.php
- M phrase.json
- M views/block/joinpage.html.php
- M views/block/menu.html.php
- M views/block/photo.html.php
- M views/block/related.html.php
- M views/block/sponsored.html.php
- M views/block/widget.html.php
- M views/controller/all.html.php
- M views/controller/index.html.php


## Version 4.7.6 ##

### Information ###

- **Release Date:** July 30, 2019
- **Best Compatibility:** phpFox >= 4.7.6

### Improvements ###

- User could able to cancel pending request to a close team

### Bugs fixed ###

- Sharing a teams widget on Facebook shows a blank image [#2713](https://github.com/PHPfox-Official/phpfox-v4-issues/issues/2713)
- Pending Membership, when clicked on does not take us to pending [#2716](https://github.com/PHPfox-Official/phpfox-v4-issues/issues/2716)

### Changed files ###

- M Ajax/Ajax.php
- M Controller/MembersController.php
- M Controller/ViewController.php
- M Install.php
- M README.md
- M Service/Callback.php
- M Service/Category.php
- M Service/Teams.php
- M Service/Process.php
- M changelog.md
- M phrase.json
- M views/block/joinpage.html.php
- M views/controller/members.html.php
- M views/controller/view.html.php

## Version 4.7.5 ##

### Information ###

- **Release Date:** June 07, 2019
- **Best Compatibility:** phpFox >= 4.7.6

### Improvements ###

- Some improvements and bug fixes.

### Bugs fixed ###

- Shift+Break is not retained in widget CKEditor [#2677](https://github.com/PHPfox-Official/phpfox-v4-issues/issues/2677)

### Changed files ###

- M Block/FeedTeamBlock.php
- M Controller/IndexController.php
- M Install.php
- M README.md
- M Service/Callback.php
- M Service/Teams.php
- M Service/Process.php
- M assets/main.less
- M changelog.md
- M hooks/link.component_ajax_addviastatusupdate.php
- A hooks/photo.component_ajax_process_done.php
- M phrase.json
- M views/block/category.html.php
- M views/block/feed-team.html.php
- D views/block/link-listing.html.php
- M views/block/link.html.php
- M views/block/menu.html.php
- M views/block/photo.html.php
- M views/controller/all.html.php
- M views/controller/index.html.php


## Version 4.7.4 ##

### Information ###

- **Release Date:** April 12, 2019
- **Best Compatibility:** phpFox >= 4.7.4

### Improvements ###

- Support to add a comment on page profile/cover feed item.

### Bugs fixed ###
- Layout issues
- Sharing internal links to pages on status doesn't show any thumbnail image [#2054](https://github.com/PHPfox-Official/phpfox-v4-issues/issues/2054)

### Changed files ###

- M Controller/Admin/AddCategoryController.php
- M Controller/Admin/CategoryController.php
- M Controller/IndexController.php
- M Controller/PhotoController.php
- M Controller/ViewController.php
- M Install.php
- A Installation/Version/v474.php
- M README.md
- M Service/Callback.php
- M Service/Category.php
- M Service/Teams.php
- M Service/Process.php
- M changelog.md
- M hooks/link.component_service_callback_getactivityfeed__1.php
- M hooks/photo.set_cover_photo_for_item.php
- M installer.php
- M phrase.json
- M views/block/photo.html.php
- M views/controller/add.html.php
- M views/controller/index.html.php

## Version 4.7.3 ##

### Information ###

- **Release Date:** March 04, 2019
- **Best Compatibility:** phpFox >= 4.7.4

### Improvements ###

- Support editing status feeds with Links.

### Bugs fixed ###

- Number of members is not updated after site admin delete user account who is team's member [#2564](https://github.com/PHPfox-Official/phpfox-v4-issues/issues/2564).
- Changing page avatar will still apply with page cover photo [#2571](https://github.com/PHPfox-Official/phpfox-v4-issues/issues/2571).
- No have notifications to owner/admin when post status on Team detail.
- Missed team name in mail title when a user post link on team.
- Bootstrap - Team Detail - Publish date with wrong size.
- Deleting teams status with link does not go away until refreshed [#2580](https://github.com/PHPfox-Official/phpfox-v4-issues/issues/2580).

### Changed files ###

- M Ajax/Ajax.php
- M Block/TeamAbout.php
- M Install.php
- M README.md
- M Service/Callback.php
- M Service/Teams.php
- M Service/Process.php
- M assets/autoload.js
- M assets/main.less
- M changelog.md
- M hooks/feed.service_process_addcomment__1.php
- M hooks/feed.service_process_deletefeed.php
- M hooks/link.component_ajax_addviastatusupdate.php
- M phrase.json
- M views/block/about.html.php

## Version 4.7.2

### Information

- **Release Date:** January 18, 2019
- **Best Compatibility:** phpFox >= 4.7.1

### Bugs fixed

- Team Feeds - Not show the content in feed when user enter HTML tag.
- Teams: Site admin cannot reassign owner to site admin.
- Invite People via Email - Guest can view info and members in Secret team by url.

### Improvements

- No redirection to parent category page if deleting a sub-category.

### Changed files

- M Ajax/Ajax.php
- M Block/TeamEvents.php
- M Block/ReassignOwner.php
- M Controller/Admin/AddCategoryController.php
- M Controller/Admin/CategoryController.php
- M Controller/IndexController.php
- M Controller/ViewController.php
- M Install.php
- M Service/Callback.php
- M Service/Category.php
- M Service/Teams.php
- M Service/Process.php
- M phrase.json
- M views/block/reassign-owner.html.php
- M views/controller/add.html.php


## Version 4.7.1

### Information

- **Release Date:** December 14, 2018
- **Best Compatibility:** phpFox >= 4.7.1

### Bugs fixed

- Team avatar thumbnail not aligned when scrolling down.

### Changed files

- M views/block/photo.html.php

## Version 4.7.0

### Information

- **Release Date:** December 12, 2018
- **Best Compatibility:** phpFox >= 4.7.1

### Bugs fixed

- Not have notification email for team owner when others join team.
- Notification not show when others user posted status/video/photo in your team.
- Responsive - Member - Team title be hidden by Search box.
- Team status is not consistency.
- Team member load wrong when click on paging.
- Create new team - Load all sub categories.
- Create new team popup - Type field hide sub categories list.
- Delete categories - Not load parent categories on "Move to" categories.
- Manage - Photo -  Edit thumbnail issues.
- Show wrong feed after add a team's admin.
- Feed - Show team's info with not clear html format.
- Material - Pending Membership button be wrong style.
- Team Detail - Team's menu - Selected menu not change color.
- Can post photo from Team feed when user has no permission.
- Missed Share action on Team Mini Menu block.
- Enable "Friends Only Community setting" - Still show all teams.
- Teams list - Not update Member number after Reassign owner.
- Not has notification for team's owner and admin when a user join team.
- Manage - Invite - Not update status to Invited after sent invitations to friends.
- Show timestamp on sharing feed.
- Sharing team feed missed phrase for category (parent category).
- Not update members number after add team's admin.
- Show own teams in Friends' Teams.
- Login as Team's admin - After unjoin that team, still can manage team.
- Can not view content of "Sharing a team" until user join that team.
- Can send many request to join closed team.
- Responsive - Sharing team feed - Default Page Avatar be not in circle.

### Improvements

- Disable filter in homepage.

### New Features

- Feed - Update layout follow new design.

### Changed files

- M Ajax/Ajax.php
- M Block/AddTeam.php
- A Block/FeedTeamBlock.php
- M Block/TeamDeleteCategory.php
- M Block/TeamMenu.php
- M Block/SearchMember.php
- M Controller/ViewController.php
- M Controller/IndexController.php
- M Install.php
- A Installation/Version/v464.php
- A Installation/Version/v470.php
- M Service/Callback.php
- M Service/Category.php
- M Service/Teams.php
- M Service/Process.php
- M assets/autoload.js
- M assets/main.less
- M hooks/core.template_block_upload_form_action_1.php
- M hooks/friend.component_block_search_get.php
- M installer.php
- M phrase.json
- M start.php
- M views/block/add-team.html.php
- M views/block/cropme.html.php
- M views/block/delete-category.html.php
- A views/block/feed-team.html.php
- M views/block/menu.html.php
- M views/block/photo.html.php
- M views/controller/add.html.php
- M views/controller/admincp/add-category.html.php

## Version 4.6.3

### Information

- **Release Date:** October 11, 2018
- **Best Compatibility:** phpFox >= 4.7.0

### Bugs fixed

- This userteam can not manage teams which they are admins if admin turn off setting "Allow to create teams"
- Phrase is missing when joined a team
- Can send multiple request to join closed team
- Can't create new widget in team
- Teams - Team Detail - Showing 2 messages when searching a request on pending requests tab 
- Teams - Team Detail - Sort by category works incorrectly
- Teams - The site is redirected to page all teams after searching teams
- Teams -  Reassign owner function works incorrectly
- BE - Teams - Manage categories - 500 error when deleting a category
- Teams - User can see the team even though the teams privacy is secret
- Show error when search

### Improvements

- Improve performance
- When Create pages and teams, please only allow sub category drop down to show if we have created sub categories
- Able to delete feed in detail pages and teams

### Changed files

- D .gitignore
- M Ajax/Ajax.php
- M Block/AddTeam.php
- M Block/TeamCategory.php
- M Block/TeamDeleteCategory.php
- M Block/TeamMenu.php
- M Controller/AddController.php
- M Controller/FrameController.php
- M Controller/IndexController.php
- M Controller/ViewController.php
- M Install.php
- M Installation/Database/PagesAdminTable.php
- M Installation/Database/PagesPermTable.php
- M Installation/Database/PagesTextTable.php
- M Installation/Database/PagesUrlTable.php
- M Installation/Database/PagesWidgetTextTable.php
- M Job/ConvertOldTeams.php
- M README.md
- M Service/Browse.php
- M Service/Callback.php
- M Service/Category.php
- M Service/Teams.php
- M Service/Process.php
- M Service/Type.php
- D app.lock
- M assets/main.less
- M changelog.md
- M hooks/feed.service_process_deletefeed.php
- M phrase.json
- M start.php
- M views/block/about.html.php
- M views/block/add-team.html.php
- M views/block/delete-category.html.php
- M views/block/search-member.html.php
- M views/block/widget.html.php
- M views/controller/index.html.php

## Version 4.6.2

### Information

- **Release Date:** Aug 21, 2018
- **Best Compatibility:** phpFox >= 4.6.1


### Improvements
- Support fix profile menu on Material Template
- Teams Detail - Members - Team owner should be shown in the first as always

### Bugs fixed
- Profile picture in Created Feed does not rotate as following the current team Profile picture
- SEO - No phrase input for description and keywords
- Can't view teams member
- Can not view team detail if disable page module
- Title not unify with other module
- Delete feed from team but main feed still exist
- Team invite email shows incorrect format no link to click on
- Displaying Page not found after clicking on Team Photo on Photo Detail page
- Teams - Edit - Can Edit team by ID of page
- Show error page when disable feed module
- When selecting your team on Android phone, selecting drop down menu to add photo, video etc doesn't work
- User Profile - Team info still displaying banned word
- Edit photo and then save thumbnail does nothing in teams
- Giving administrator privileges for teams you can add 2 same users with admin privileges
- Owner of Teams received Email with wrong language when anyone have any actions (join team, comment, posted items...) in it
- Can not create team if does not install page app
- Approve bar not disappear after approved
- Edit Team: Not load sub-categories when changed Category
- Does not show "Friend's Teams" menu for non-login user
- Manage Categories in ACP: Layout issue when drag categories

### Changed files
- M Ajax/Ajax.php
- A Block/Featured.php
- M Block/TeamPhoto.php
- A Block/ReassignOwner.php
- M Block/SearchMember.php
- A Block/Sponsored.php
- M Controller/AddController.php
- M Controller/Admin/IntegrateController.php
- M Controller/IndexController.php
- M Controller/PhotoController.php
- M Controller/ViewController.php
- M Install.php
- M Installation/Database/PagesTable.php
- M Job/ConvertOldTeams.php
- M Job/SendMemberJoinNotification.php
- M Job/SendMemberNotification.php
- M README.md
- M Service/Browse.php
- M Service/Callback.php
- M Service/Facade.php
- M Service/Teams.php
- M Service/Process.php
- M assets/autoload.js
- M assets/main.less
- M changelog.md
- M hooks/feed.service_process_addcomment__1.php
- A hooks/feed.service_process_deletefeed_end.php
- M hooks/get_module_blocks.php
- M hooks/link.component_ajax_addviastatusupdate.php
- M phrase.json
- M start.php
- A views/block/featured.html.php
- M views/block/link-listing.html.php
- M views/block/link.html.php
- M views/block/photo.html.php
- A views/block/reassign-owner.html.php
- M views/block/related.html.php
- A views/block/sponsored.html.php
- M views/controller/add.html.php
- M views/controller/admincp/category.html.php
- M views/controller/all.html.php
- M views/controller/index.html.php

## Version 4.6.1

### Information

- **Release Date:** April 26, 2018
- **Best Compatibility:** phpFox >= 4.6.1

### Bugs fixed
- Show "Unable to find the page you are looking for." when click on profile photo.
- Can't upload profile image in manage team if using S3.
- Can't reorder sub-category.
- Lite package can not upload photo if enable debug mode.
- Missing save button when reposition cover photo.

### Changed files
- A hooks/link.component_service_callback_getactivityfeed__1.php
- M	Controller/IndexController.php
- M	Install.php
- M	Installation/Version/v460.php
- M	Service/Callback.php
- M	Service/Teams.php
- M	Service/Process.php
- M	assets/main.less
- M	phrase.json
- M	start.php
- M	views/block/about.html.php
- M	views/block/photo.html.php
- M	views/controller/admincp/category.html.php

## Version 4.6.0

### Information

- **Release Date:** January 9th, 2018
- **Best Compatibility:** phpFox >= 4.6.0

### Improvements

- Support teams admin can re order widgets.
- Support upload thumbnail photo for main categories.
- Add statistic information of Teams (total items, pending items...) into Sit Statistics.
- Users can select actions of teams on listing page same as on detail page.
- Count items on menu My Teams.
- Support drag/drop, preview, progress bar when users upload thumbnail photos for teams.
- Hide all buttons/links if users don't have permission to do.
- Support 3 styles for pagination.
- Allow admin can change default photos.
- Validate all settings, user team settings, and block settings.
- Update new layout for all blocks and pages.

### Removed Settings

| ID | Var name | Name | Reason |
| --- | -------- | ---- | --- |
| 1 | teams.pf_team_show_admins | Show Team Admins | Move to setting of block Admin |

### New Settings

| ID | Var name | Name | Description |
| --- | -------- | ---- | ---- |
| 1 | teams.teams_limit_per_category | Teams Limit Per Category | Define the limit of how many teams per category can be displayed when viewing All Teams page. |
| 2 | teams.pagination_at_search_teams | Paging Style |  |
| 3 | teams.display_teams_profile_photo_within_gallery | Display teams profile photo within gallery | Disable this feature if you do not want to display teams profile photos within the photo gallery. |
| 5 | teams.display_teams_cover_photo_within_gallery | Display teams cover photo within gallery | Disable this feature if you do not want to display teams cover photos within the photo gallery. |
| 6 | teams_meta_description | Teams Meta Description | Meta description added to teams related to the Teams app. |
| 7 | teams_meta_keywords | Teams Meta Keywords | Meta keywords that will be displayed on sections related to the Teams app. |

### Removed User Team Settings

| ID | Var name | Name | Reason |
| --- | -------- | ---- | --- |
| 1 | teams.pf_team_moderate | Can moderate teams? This will allow a user to edit/delete/approve teams added by other users. | Don't use anymore, split to 3 new user team settings "Can edit all teams?", "Can delete all teams?", "Can approve teams?" |

### New User Team Settings

| ID | Var name | Information |
| --- | -------- | ---- |
| 1 | teams.can_edit_all_teams | Can edit all teams? |
| 2 | teams.can_delete_all_teams | Can delete all teams? |
| 3 | teams.can_approve_teams | Can approve teams? |
| 4 | teams.flood_control| Define how many minutes this user team should wait before they can add new team. Note: Setting it to "0" (without quotes) is default and users will not have to wait. |
