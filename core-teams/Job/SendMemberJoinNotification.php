<?php

namespace Apps\PHPfox_Teams\Job;

use Core\Queue\JobAbstract;
use Phpfox;

class SendMemberJoinNotification extends JobAbstract
{
    public function perform()
    {
        $aParams = $this->getParams();

        // get team's admins
        $aAdmins = Phpfox::getService('teams')->getPageAdmins($aParams['aTeam']['page_id']);
        // team link
        $sLink = Phpfox::getService('teams')->getUrl($aParams['aTeam']['page_id'], $aParams['aTeam']['title'],
            $aParams['aTeam']['vanity_url']);

        $aUser = Phpfox::getService('user')->get($aParams['iUserId']);
        // send notification for admins of team
        foreach ($aAdmins as $aAdmin) {
            if ($aParams['iUserId'] != $aAdmin['user_id']) {
                Phpfox::getLib('mail')->to($aAdmin['user_id'])
                    ->subject(['{{ full_name }} joined your team "{{ title }}"',
                        ['full_name' => $aUser['full_name'], 'title' => $aParams['aTeam']['title']]])
                    ->message(['{{ full_name }} joined your team "<a href="{{ link }}">{{ title }}</a>" To view this team follow the link below: <a href="{{ link }}">{{ link }}</a>',
                        [
                            'full_name' => $aUser['full_name'],
                            'link' => $sLink,
                            'title' => $aParams['aTeam']['title']
                        ]])
                    ->notification('like.new_like')
                    ->send();

                Phpfox::getService('notification.process')->add('teams_like', $aParams['aTeam']['page_id'],
                    $aAdmin['user_id'], $aParams['iUserId'], true);
            }
        }

        $this->delete();
    }
}
