<?php

namespace Apps\PHPfox_Teams\Job;

use Core\Queue\JobAbstract;
use Phpfox;
use Phpfox_Queue;

/**
 * Class SendMemberNotification
 *
 * @package Apps\PHPfox_Teams\Job
 */
class  ConvertOldTeams extends JobAbstract
{
    /**
     * @inheritdoc
     */
    public function perform()
    {
        \Phpfox::getService('teams')->convertOldTeams();
        $iNumberTeams = \Phpfox::getService('teams')->getCountConvertibleTeams();
        $this->delete();
        if ($iNumberTeams == 0) {
            //Delete category of old teams
            db()->delete(':pages_category', 'page_type=1');
            $iUserId = storage()->get('phpfox_job_queue_convert_team_run')->value;
            storage()->del('phpfox_job_queue_convert_team_run');
            Phpfox::getLib('mail')->to($iUserId)
                ->subject('Teams converted')
                ->message("All old teams (page type) converted new teams")
                ->send();
            Phpfox::getService('notification.process')->add('teams_converted', 0, $iUserId, 1);
        }
        else {
            // add job again when total teams > 1000
            Phpfox_Queue::instance()->addJob('teams_convert_old_team', []);
        }
    }
}
