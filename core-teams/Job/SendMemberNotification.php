<?php

namespace Apps\PHPfox_Teams\Job;

use Core\Queue\JobAbstract;
use Phpfox;

/**
 * Class SendMemberNotification
 *
 * @package Apps\PHPfox_Teams\Job
 */
class SendMemberNotification extends JobAbstract
{
    /**
     * @inheritdoc
     */
    public function perform()
    {
        $aParams = $this->getParams();

        if (empty($aParams['owner_id'])) {
            $this->delete();

            return;
        }

        $aOwner = Phpfox::getService('user')->getUser($aParams['owner_id']);
        $aTeamPerms = Phpfox::getService('teams')->getPermsForPage($aParams['page_id']);
        $iPerm = isset($aTeamPerms[$aParams['item_perm']]) ? $aTeamPerms[$aParams['item_perm']] : 0;
        $aTeam = Phpfox::getService('teams')->getPage($aParams['page_id']);

        if (!empty($aParams['item_type']) && Phpfox::hasCallback($aParams['item_type'], 'getLink')) {
            $sLink = Phpfox::callback($aParams['item_type'] . '.getLink', [
                'item_id' => $aParams['item_id']
            ]);
        } else {
            $sLink = Phpfox::getService('teams')->getUrl($aTeam['page_id'], $aTeam['title'], $aTeam['vanity_url']);
        }
        if ($iPerm == 2) {
            $aUsers = Phpfox::getService('teams')->getPageAdmins($aParams['page_id']);
        } else {
            list(, $aUsers) = Phpfox::getService('teams')->getMembers($aParams['page_id']);
        }

        foreach ($aUsers as $aUser) {
            // do not send notification to owner if owner upload photo
            if (isset($aParams['owner_id']) && ($aUser['user_id'] == $aParams['owner_id'])) {
                continue;
            }
            // send notification
            Phpfox::getService('notification.process')->add($aParams['item_type'] . '_newItem_teams',
                $aParams['item_id'], $aUser['user_id'], $aParams['owner_id']);
            // send email
            Phpfox::getLib('mail')->to($aUser['user_id'])
                ->subject(['full_name_post_some_items_on_your_team_title', [
                    'full_name' => $aOwner['full_name'],
                    'title' => $aTeam['title'],
                    'items' => $aParams['items_phrase']
                ]])
                ->message(['full_name_post_some_items_on_your_team_title_link', [
                    'full_name' => $aOwner['full_name'],
                    'link' => $sLink,
                    'title' => $aTeam['title'],
                    'items' => $aParams['items_phrase']
                ]])
                ->notification('comment.add_new_comment')
                ->send();
        }

        $this->delete();
    }
}
