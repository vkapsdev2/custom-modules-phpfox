<?php

namespace Apps\PHPfox_Teams\Controller;

use Phpfox;
use Phpfox_Plugin;

class FansController extends \Phpfox_Component
{
    public function process()
    {
        $aTeam = $this->getParam('aPage');
        list($iTotalMembers,) = Phpfox::getService('teams')->getFanMembers($aTeam['page_id']);
        $iTotalPendings = Phpfox::getService('teams')->getPendingUsers($aTeam['page_id'], true);
        $bIsAdmin = Phpfox::getService('teams')->isAdmin($aTeam);
        $bCanViewAdmins = Phpfox::getService('teams')->hasPerm($aTeam['page_id'], 'teams.view_admins');
        $sActiveTab = $this->request()->get('tab', 'all');

        $this->template()
            ->clearBreadCrumb()
            ->setBreadCrumb($aTeam['title'], url('teams/' . $aTeam['page_id']))
            ->setBreadCrumb(_p('Fans'), url('teams/' . $aTeam['page_id'] . '/fans'))
            ->assign([
                'iTotalMembers' => $iTotalMembers,
                'iTotalPendings' => $iTotalPendings,
                'bShowFriendInfo' => true,
                'iTeamId' => $aTeam['page_id'],
                'bIsAdmin' => $bIsAdmin,
                'bIsOwner' => Phpfox::getService('teams')->getPageOwnerId($aTeam['page_id']) == Phpfox::getUserId(),
                'iTotalAdmins' => Phpfox::getService('teams')->getTeamAdminsCount($aTeam['page_id']),
                'bCanViewAdmins' => $bCanViewAdmins,
                'sActiveTab' => $sActiveTab
            ]);

        $this->setParam(['mutual_list' => true]);

        // moderation
        if ($bIsAdmin && $iTotalMembers) {
            $aModerations[] = [
                'phrase' => _p('delete'),
                'action' => 'delete'
            ];
            $this->setParam('global_moderation', array(
                    'name' => 'teams',
                    'ajax' => 'teams.memberModeration',
                    'menu' => $aModerations,
                    'custom_fields' => '<input type="hidden" name="page_id" value="' . $aTeam['page_id'] . '"/>'
                )
            );
        }
    }

    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
        (($sPlugin = Phpfox_Plugin::get('teams.component_controller_members_clean')) ? eval($sPlugin) : false);
    }
}
