<?php

namespace Apps\PHPfox_Teams\Controller\Admin;

use Admincp_Component_Controller_App_Index;
use Apps\PHPfox_Teams\Job\ConvertOldTeams;
use Phpfox;
use Phpfox_Plugin;
use Phpfox_Queue;

defined('PHPFOX') or exit('NO DICE!');

class ConvertController extends Admincp_Component_Controller_App_Index
{
    public function process()
    {
        parent::process();
        $iNumberTeams = \Phpfox::getService('teams')->getCountConvertibleTeams();
        $iConvert = $this->request()->getInt('convert');
        if ($iNumberTeams && $iConvert) {
            $iCron = $this->request()->getInt('cron');
            //Map old teams to new teams
            if ($iCron) {
                Phpfox_Queue::instance()->addJob('teams_convert_old_team', []);
            } else {
                (new ConvertOldTeams(0, 0, '', []))
                    ->perform();
            }
            Phpfox::addMessage(_p('Your job is running. You will receive notice when it done'));
            storage()->set('phpfox_job_queue_convert_team_run', Phpfox::getUserId());
            $this->url()->send('admincp.app', [
                'id' => 'PHPfox_Teams'
            ]);
        }
        $store_data = storage()->get('phpfox_job_queue_convert_team_run');
        $iConvertedUserId = isset($store_data->value) ? $store_data->value : 0;
        $this->template()
            ->setBreadCrumb(_p('convert_old_teams'))
            ->assign([
                'iNumberTeams' => $iNumberTeams,
                'iConvertedUserId' => $iConvertedUserId
            ]);
    }

    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
        (($sPlugin = Phpfox_Plugin::get('teams.component_controller_admincp_convert_clean')) ? eval($sPlugin) : false);
    }
}
