<?php

namespace Apps\PHPfox_Teams\Ajax\Photo;

use Core_Service_Process;
use Phpfox;
use Phpfox_Ajax;
use Phpfox_File;
use Phpfox_Image;
use Phpfox_Error;
use Phpfox_Module;
use Phpfox_Image_Helper;
use Phpfox_Plugin;
use Phpfox_Template;

/**
 * Class Ajax
 *
 * @package Apps\PHPfox_Teams\Ajax
 */
class Ajax extends Phpfox_Ajax
{
	
	
	public function logo()
    {
        $this->setTitle(_p('cover_photo'));
        $aParams = array(
            'page_id' => $this->get('page_id'),
            'groups_id' => $this->get('groups_id'),
            'teams_id' => $this->get('teams_id')
        );

        Phpfox::getBlock('teams.cover', $aParams);
    } 
	
	public function process()
    {
        $aPostPhotos = $this->get('photos');
        $iTimeStamp = $this->get('timestamp', 0);
        $aVals = $this->get('val');

        if (is_array($aPostPhotos)) {
            $aImages = array();
            foreach ($aPostPhotos as $aPostPhoto) {
                $aPart = json_decode(urldecode($aPostPhoto), true);
                $aImages[] = $aPart[0];
            }
        } else {
            $aImages = json_decode(urldecode($aPostPhotos), true);
        }

        $oImage = Phpfox_Image::instance();
        $aPhoto = [];
        $aImage = [];

        foreach ($aImages as $iKey => $aImage) {
            $aImage['destination'] = urldecode($aImage['destination']);
            if ($aImage['completed'] == 'false') {
                $aPhoto = Phpfox::getService('photo')->getForProcess($aImage['photo_id'], $this->get('user_id', 0));
                if (isset($aPhoto['photo_id'])) {
                    if (Phpfox::getParam('core.allow_cdn')) {
                        Phpfox::getLib('cdn')->setServerId($aPhoto['server_id']);
                    }

                    $sFileName = $aPhoto['destination'];
                    $sFile = Phpfox::getParam('photo.dir_photo') . sprintf($sFileName, '');
                    if (!file_exists(Phpfox::getParam('photo.dir_photo') . sprintf($sFileName, ''))
                        && Phpfox::getParam('core.allow_cdn')
                        && !Phpfox::getParam('core.keep_files_in_server')
                    ) {
                        if (Phpfox::getParam('core.allow_cdn') && $aPhoto['server_id'] > 0) {
                            $sActualFile = Phpfox::getLib('image.helper')->display(array(
                                    'server_id' => $aPhoto['server_id'],
                                    'path' => 'photo.url_photo',
                                    'file' => $aPhoto['destination'],
                                    'suffix' => '',
                                    'return_url' => true
                                )
                            );

                            $aExts = preg_split("/[\/\\.]/", $sActualFile);
                            $iCnt = count($aExts) - 1;
                            $sExt = strtolower($aExts[$iCnt]);

                            $aParts = explode('/', $aPhoto['destination']);
                            $sFile = Phpfox::getParam('photo.dir_photo') . $aParts[0] . '/' . $aParts[1] . '/' . md5($aPhoto['destination']) . '.' . $sExt;

                            // Create a temp copy of the original file in local server
                            if (filter_var($sActualFile, FILTER_VALIDATE_URL) !== false) {
                                file_put_contents($sFile, fox_get_contents($sActualFile));
                            } else {
                                copy($sActualFile, $sFile);
                            }
                            //Delete file in local server
                            register_shutdown_function(function () use ($sFile) {
                                @unlink($sFile);
                            });
                        }
                    }
                    list($width, $height, ,) = getimagesize($sFile);
                    foreach (Phpfox::getService('photo')->getPhotoPicSizes() as $iSize) {
                        // Create the thumbnail
                        if ($oImage->createThumbnail($sFile,
                                Phpfox::getParam('photo.dir_photo') . sprintf($sFileName, '_' . $iSize), $iSize,
                                $height, true,
                                ((Phpfox::getParam('core.watermark_option') != 'none') ? (Phpfox::getParam('core.watermark_option') == 'image' ? 'force_skip' : true) : false)) === false
                        ) {
                            continue;
                        }

                        if (defined('PHPFOX_IS_HOSTED_SCRIPT')) {
                            unlink(Phpfox::getParam('photo.dir_photo') . sprintf($sFileName, '_' . $iSize));
                        }
                    }
                    //Crop original image
                    $iWidth = (int)Phpfox::getUserParam('photo.maximum_image_width_keeps_in_server');
                    if ($iWidth < $width) {
                        $bIsCropped = $oImage->createThumbnail(Phpfox::getParam('photo.dir_photo') . sprintf($sFileName,
                                ''), Phpfox::getParam('photo.dir_photo') . sprintf($sFileName, ''), $iWidth, $height,
                            true,
                            ((Phpfox::getParam('core.watermark_option') != 'none') ? (Phpfox::getParam('core.watermark_option') == 'image' ? 'force_skip' : true) : false));
                        if ($bIsCropped !== false) {
                            //Rename file
                            if (defined('PHPFOX_IS_HOSTED_SCRIPT')) {
                                unlink(Phpfox::getParam('photo.dir_photo') . sprintf($sFileName, ''));
                            }
                        }

                        @clearstatcache();
                        $iNewFileSize = filesize(Phpfox::getParam('photo.dir_photo') . sprintf($sFileName, ''));
                        list($iNewWidth, $iNewHeight, ,) = getimagesize(Phpfox::getParam('photo.dir_photo') . sprintf($sFileName, ''));
                        Phpfox::getService('photo.process')->updatePhotoInfo($aImage['photo_id'], ['file_size' => $iNewFileSize, 'width' => $iNewWidth, 'height' => $iNewHeight]);
                    }
                    //End Crop
                    $aImages[$iKey]['completed'] = 'true';

                    (($sPlugin = Phpfox_Plugin::get('photo.component_ajax_ajax_process__1')) ? eval($sPlugin) : false);

                    break;
                }
            }
        }

        $iNotCompleted = 0;
        foreach ($aImages as $iKey => $aImage) {
            if ($aImage['completed'] == 'false') {
                $iNotCompleted++;
            } else {
                $aPhoto = Phpfox::getService('photo')->getForProcess($aImage['photo_id'], $this->get('user_id', 0));
            }
        }
        if ($iNotCompleted === 0) {
            $aCallback = ($this->get('callback_module') ? Phpfox::callback($this->get('callback_module') . '.addPhoto',
                $this->get('callback_item_id')) : null);
            $iFeedId = 0;
            $bNewFeed = false;
            if (!Phpfox::getUserParam('photo.photo_must_be_approved') && !$this->get('is_cover_photo') && !$this->get('no_feed')) {
                if (Phpfox::isModule('feed')) {
                    if ($iTimeStamp && !empty($_SESSION['upload_photo_' . $iTimeStamp . '_' . $aPhoto['album_id']])) {
                        $iFeedId = $_SESSION['upload_photo_' . $iTimeStamp . '_' . $aPhoto['album_id']];
                    } else {
                        if((isset($aVals['action']) && $aVals['action'] == 'upload_photo_via_share') || ((!isset($aVals['action']) || (isset($aVals['action']) && $aVals['action'] != 'upload_photo_via_share')) && Phpfox::getParam('photo.photo_allow_create_feed_when_add_new_item', 1)))
                        {
                            $iFeedId = Phpfox::getService('feed.process')->callback($aCallback)->add('photo',
                                $aPhoto['photo_id'], $aPhoto['privacy'], $aPhoto['privacy_comment'],
                                (int)$this->get('parent_user_id', 0));
                        }

                        if ($aCallback && defined('PHPFOX_NEW_FEED_LOOP_ID') && PHPFOX_NEW_FEED_LOOP_ID) {
                            storage()->set('photo_parent_feed_' . PHPFOX_NEW_FEED_LOOP_ID, $iFeedId);
                        }

                        $bNewFeed = true;
                        if ($iTimeStamp) {
                            $_SESSION['upload_photo_' . $iTimeStamp . '_' . $aPhoto['album_id']] = $iFeedId;
                        }
                        if (isset($aVals['action']) && $aVals['action'] == 'upload_photo_via_share') {
                            Phpfox::getService('photo.process')->notifyTaggedInFeed($aVals['status_info'], $aPhoto['photo_id'], $aPhoto['user_id'], $iFeedId, $aVals['tagged_friends'], $aVals['privacy'], (int)$this->get('parent_user_id', 0));
                        }
                        if ($aCallback && Phpfox::isModule('notification') && Phpfox::isModule($aCallback['module']) && Phpfox::hasCallback($aCallback['module'],
                                'addItemNotification')
                        ) {
                            Phpfox::callback($aCallback['module'] . '.addItemNotification', [
                                'page_id' => $aCallback['item_id'],
                                'item_perm' => 'photo.view_browse_photos',
                                'item_type' => 'photo',
                                'item_id' => $aPhoto['photo_id'],
                                'owner_id' => $aPhoto['user_id'],
                                'items_phrase' => _p('photos__l')
                            ]);
                        }
                    }

                }
                if (count($aImages)) {
                    foreach ($aImages as $aImage) {
                        if ($aImage['photo_id'] == $aPhoto['photo_id'] && $bNewFeed) {
                            continue;
                        }

                        db()->insert(Phpfox::getT('photo_feed'), array(
                                'feed_id' => $iFeedId,
                                'photo_id' => $aImage['photo_id'],
                                'feed_table' => (empty($aCallback['table_prefix']) ? 'feed' : $aCallback['table_prefix'] . 'feed')
                            )
                        );
                    }
                }
            }

            // this next if is the one you will have to bypass if they come from sharing a photo in the activity feed.
            if (($this->get('page_id') > 0)) {
                if ($this->get('is_cover_photo')) {
                    Phpfox::getService('pages.process')->updateCoverPhoto($aImage['photo_id'], $this->get('page_id'));
                }
                $this->call('window.location.href = "' . Phpfox::getLib('url')->permalink('pages',
                        $this->get('page_id'), '') . 'coverupdate_1";');
            } else {
                if (($this->get('groups_id') > 0)) {
                    if ($this->get('is_cover_photo')) {
                        Phpfox::getService('groups.process')->updateCoverPhoto($aImage['photo_id'], $this->get('groups_id'));
                    }
                    $this->call('window.location.href = "' . Phpfox::getLib('url')->permalink('groups',
                            $this->get('groups_id'), '') . 'coverupdate_1";');
                }elseif (($this->get('teams_id') > 0)) {
                    if ($this->get('is_cover_photo')) {
                        Phpfox::getService('teams.process')->updateCoverPhoto($aImage['photo_id'], $this->get('teams_id'));
                    }
                    $this->call('window.location.href = "' . Phpfox::getLib('url')->permalink('teams',
                            $this->get('teams_id'), '') . 'coverupdate_1";');
                } else {
                    if ($this->get('action') == 'upload_photo_via_share') {
                        if ($this->get('is_cover_photo')) {
                            Phpfox::getService('user.process')->updateCoverPhoto($aImage['photo_id']);

                            $this->call('window.location.href = \'' . Phpfox::getLib('url')->makeUrl('profile',
                                    array('coverupdate' => '1')) . '\';');
                        } else {
                            if ($aCallback && in_array($aCallback['module'], ['groups', 'pages', 'teams']) && Phpfox::getLib('pages.facade')->getPageItemType($aCallback['item_id']) !== false && !defined('PHPFOX_IS_PAGES_VIEW')) {
                                define('PHPFOX_IS_PAGES_VIEW', true);
                            }
                            if(Phpfox::isModule('feed')) {
                                Phpfox::getService('feed')->callback($aCallback)->processAjax($iFeedId);
                            }

                            (($sPlugin = Phpfox_Plugin::get('photo.component_ajax_process_done')) ? eval($sPlugin) : false);
                        }
                    } else {
                        foreach ($aImages as $aImage) {
                            // use the JS var set at progress.js
                            $this->call('sImages += "&photos[]=' . $aImage['photo_id'] . '";');
                        }
                        if (Phpfox::getParam('photo.photo_upload_process', 0)) {
                            if ($aCallback !== null) {
                                $sModule = isset($aCallback['module']) ? $aCallback['module'] : 'pages';
                                $this->call('var sCurrentProgressClub = \'' . Phpfox::getLib('url')->makeUrl($sModule . '.' . $aCallback['item_id'] . '.photo',
                                        ['view' => 'my', 'mode' => 'edit']) . '\';');
                            } else {
                                $this->call('var sCurrentProgressClub = \'' . Phpfox::getLib('url')->makeUrl('photo',
                                        array('view' => 'my', 'mode' => 'edit')) . '\';');
                            }
                            $this->call('var edit_after_upload = true;');
                        } else {
                            $this->call('sImages = "";');
                            $this->call('var sCurrentProgressClub = \'' . Phpfox::getLib('url')->permalink('photo',
                                    $aPhoto['photo_id'],
                                    $aPhoto['title']) . '/\';');
                        }
                        $this->call('hasUploaded++; if ((hasUploaded + hasErrors) == iTotalUploadedFiles) completeProgress();');
                    }
                }
            }
        } else {
            $this->call('$(\'#js_progress_cache_holder\').html(\'\' + $.ajaxProcess(\'' . _p('processing_image_current_total',
                    array(
                        'phpfox_squote' => true,
                        'current' => (count($aImages) - $iNotCompleted),
                        'total' => count($aImages)
                    )) . '\', \'large\') + \'\');');
            $this->html('#js_photo_upload_process_cnt', (count($aImages) - $iNotCompleted));

            $sExtra = '';
            if ($this->get('callback_module')) {
                $sExtra .= '&callback_module=' . $this->get('callback_module') . '&callback_item_id=' . $this->get('callback_item_id') . '';
            }
            if ($this->get('parent_user_id')) {
                $sExtra .= '&parent_user_id=' . $this->get('parent_user_id');
            }

            if ($this->get('start_year') && $this->get('start_month') && $this->get('start_day')) {
                $sExtra .= '&start_year= ' . $this->get('start_year') . '&start_month= ' . $this->get('start_month') . '&start_day= ' . $this->get('start_day') . '';
            }

            if ($this->get('custom_pages_post_as_page')) {
                $sExtra .= '&custom_pages_post_as_page= ' . $this->get('custom_pages_post_as_page');
            }
            if (isset($aVals['action']) && $aVals['action'] == 'upload_photo_via_share') {
                $sExtra .= '&val[action]=' . $aVals['action'] . '&val[status_info]=' . $aVals['status_info'];
            }
            $sExtra .= '&is_cover_photo=' . $this->get('is_cover_photo');
            $this->call('$.ajaxCall(\'teams.photo.process\', \'&action=' . $this->get('action') . '&js_disable_ajax_restart=true&photos=' . json_encode($aImages) . $sExtra . '\');');
        }

        $aVals = $this->get('core');

        if (isset($aVals['profile_user_id']) && !empty($aVals['profile_user_id']) && $aVals['profile_user_id'] != Phpfox::getUserId() && Phpfox::isModule('notification')) {
            Phpfox::getService('notification.process')->add('photo_feed_profile', $aPhoto['photo_id'],
                $aVals['profile_user_id']);
        }
    }
   

}
