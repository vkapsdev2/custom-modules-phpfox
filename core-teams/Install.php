<?php

namespace Apps\PHPfox_Teams;

use Core\App;
use Core\App\Install\Setting;
use Phpfox;
/**
 * Class Install
 * @author  phpFox
 * @package Apps\PHPfox_Teams
 */
class Install extends App\App
{
    private $_app_phrases = [

    ];

    public $store_id = '';

    protected function setId()
    {
        $this->id = 'PHPfox_Teams';
    }

    /**
     * Set start and end support version of your App.
     */
    protected function setSupportVersion()
    {
        $this->start_support_version = '4.7.6';
    }

    protected function setAlias()
    {
        $this->alias = 'teams';
    }

    public function setName()
    {
        $this->name = _p('Teams');
    }

    public function setVersion()
    {
        $this->version = '1.0';
    }

    public function setSettings()
    {
        $this->settings = [
            'teams_limit_per_category' => [
                'info' => 'Teams Limit Per Category',
                'description' => 'Define the limit of how many teams per category can be displayed when viewing All Teams page.',
                'type' => 'integer',
                'value' => 6,
                'ordering' => 1
            ],
            'pagination_at_search_teams' => [
                'info' => 'Paging Type',
                'description' => '',
                'type' => 'select',
                'options' => [
                    'loadmore' => 'Scrolling down to load more items',
                    'next_prev' => 'Use Next and Pre buttons',
                    'pagination' => 'Use pagination with page number'
                ],
                'value' => 'loadmore',
                'ordering' => 0
            ],
            'display_teams_profile_photo_within_gallery' => [
                'info' => 'Display teams profile photo within gallery',
                'description' => 'Disable this feature if you do not want to display teams profile photos within the photo gallery.',
                'type' => 'boolean',
                'value' => 0,
                'ordering' => 3
            ],
            'display_teams_cover_photo_within_gallery' => [
                'info' => 'Display teams cover photo within gallery',
                'description' => 'Disable this feature if you do not want to display teams cover photos within the photo gallery.',
                'type' => 'boolean',
                'value' => 0,
                'ordering' => 4
            ],
            'teams_setting_meta_description' => [
                'info' => 'Teams Meta Description',
                'description' => 'Meta description added to teams related to the Teams app. <a role="button" onclick="$Core.editMeta(\'seo_teams_meta_description\', true)">Click here</a> to edit meta description.<span style="float:right;">(SEO) <input style="width:150px;" readonly value="seo_teams_meta_description"></span>',
                'type' => '',
                'value' => '{_p var="seo_teams_meta_description"}',
                'ordering' => 5,
                'team_id' => 'seo'
            ],
            'teams_setting_meta_keywords' => [
                'info' => 'Teams Meta Keywords',
                'description' => 'Meta keywords that will be displayed on sections related to the Teams app. <a role="button" onclick="$Core.editMeta(\'seo_teams_meta_keywords\', true)">Click here</a> to edit meta keywords.<span style="float:right;">(SEO) <input style="width:150px;" readonly value="seo_teams_meta_keywords"></span>',
                'type' => '',
                'value' => '{_p var="seo_teams_meta_keywords"}',
                'ordering' => 6,
                'team_id' => 'seo'
            ],
        ];
    }

    public function setUserGroupSettings()
    {
        $this->user_group_settings = [
            'pf_team_browse' => [
                'var_name' => 'pf_team_browse',
                'info' => 'Can browse teams?',
                'type' => Setting\Groups::TYPE_RADIO,
                'value' => [
                    "1" => "1",
                    "2" => "1",
                    "3" => "1",
                    "4" => "1",
                    "5" => "0"
                ],
                'options' => Setting\Groups::$OPTION_YES_NO
            ],
            'pf_team_add_cover_photo' => [
                'var_name' => 'pf_team_add_cover_photo',
                'info' => 'Can add a cover photo on teams?',
                'type' => Setting\Groups::TYPE_RADIO,
                'value' => [
                    "1" => "1",
                    "2" => "1",
                    "3" => "1",
                    "4" => "1",
                    "5" => "0"
                ],
                'options' => Setting\Groups::$OPTION_YES_NO
            ],
            'can_edit_all_teams' => [
                'info' => 'Can edit all teams?',
                'type' => 'boolean',
                'value' => [
                    1 => 1,
                    2 => 0,
                    3 => 0,
                    4 => 1,
                    5 => 0
                ],
                'ordering' => 3
            ],
            'can_delete_all_teams' => [
                'info' => 'Can delete all teams?',
                'type' => 'boolean',
                'value' => [
                    1 => 1,
                    2 => 0,
                    3 => 0,
                    4 => 1,
                    5 => 0
                ],
                'ordering' => 4
            ],
            'can_approve_teams' => [
                'info' => 'Can approve teams?',
                'type' => 'boolean',
                'value' => [
                    1 => 1,
                    2 => 0,
                    3 => 0,
                    4 => 1,
                    5 => 0
                ],
                'ordering' => 5
            ],
            'pf_team_add' => [
                'var_name' => 'pf_team_add',
                'info' => 'Can add teams?',
                'type' => Setting\Groups::TYPE_RADIO,
                'value' => [
                    "1" => "1",
                    "2" => "1",
                    "3" => "0",
                    "4" => "1",
                    "5" => "0"
                ],
                'options' => Setting\Groups::$OPTION_YES_NO
            ],
            'pf_team_max_upload_size' => [
                'var_name' => 'pf_team_max_upload_size',
                'info' => 'Max file size for upload files in kilobytes (kb). For unlimited add "0" without quotes.',
                'type' => Setting\Groups::TYPE_TEXT,
                'value' => 8192,
            ],
            'pf_team_approve_teams' => [
                'var_name' => 'pf_team_approve_teams',
                'info' => 'Teams must be approved first before they are displayed publicly?',
                'type' => Setting\Groups::TYPE_RADIO,
                'value' => 0,
                'options' => Setting\Groups::$OPTION_YES_NO
            ],
            'points_teams' => [
                'var_name' => 'points_teams',
                'info' => 'Activity points received when creating a new team.',
                'type' => Setting\Groups::TYPE_TEXT,
                'value' => 1
            ],
            'flood_control' => [
                'info' => 'Define how many minutes this user team should wait before they can add new team. Note: Setting it to "0" (without quotes) is default and users will not have to wait.',
                'type' => 'integer',
                'value' => [
                    1 => 0,
                    2 => 0,
                    3 => 0,
                    4 => 0,
                    5 => 0
                ]
            ],
            'can_feature_team' => [
                'var_name' => 'can_feature_team',
                'info' => 'Can feature a team?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '0',
                    '3' => '0',
                    '4' => '1',
                    '5' => '0'
                ]
            ],
            'can_sponsor_teams' => [
                'var_name' => 'can_sponsor_teams',
                'info' => 'Can members of this user team mark a team as Sponsor without paying fee?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '0',
                    '3' => '0',
                    '4' => '0',
                    '5' => '0'
                ]
            ],
            'can_purchase_sponsor_teams' => [
                'var_name' => 'can_purchase_sponsor_teams',
                'info' => 'Can members of this user team purchase a sponsored ad space?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '0',
                    '3' => '0',
                    '4' => '0',
                    '5' => '0'
                ]
            ],
            'teams_sponsor_price' => [
                'var_name' => 'teams_sponsor_price',
                'info' => 'How much is the sponsor space worth for teams? This works in a CPM basis.',
                'description' => '',
                'type' => 'currency'
            ],
            'auto_publish_sponsored_item' => [
                'var_name' => 'auto_publish_sponsored_item',
                'info' => 'Auto publish sponsored item?',
                'description' => 'After the user has purchased a sponsored space, should the item be published right away? 
If set to No, the admin will have to approve each new purchased sponsored item space before it is shown in the site.',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '0',
                    '3' => '0',
                    '4' => '0',
                    '5' => '0'
                ],
            ],
        ];
    }

    public function setComponent()
    {
        $this->component = [
            "block" => [
                "about" => "",
                "admin" => "",
                "category" => "",
                "events" => "",
                "members" => "",
                "menu" => "",
                "photo" => "",
                "profile" => "",
                "widget" => "",
                "cropme" => "",
                "related" => "",
                "featured" => "",
                "sponsored" => ""
            ],
            "controller" => [
                "index" => "teams.index",
                "add" => "teams.add",
                "all" => "teams.all",
                "view" => "teams.view",
                "profile" => "teams.profile"
            ]
        ];
    }

    public function setComponentBlock()
    {
        $this->component_block = [
            "Teams Likes/Members" => [
                "type_id" => "0",
                "m_connection" => "teams.view",
                "component" => "members",
                "location" => "3",
                "is_active" => "1",
                "ordering" => "3"
            ],
            "Teams Info" => [
                "type_id" => "0",
                "m_connection" => "teams.view",
                "component" => "about",
                "location" => "1",
                "is_active" => "1",
                "ordering" => "3"
            ],
            "Teams Mini Menu" => [
                "type_id" => "0",
                "m_connection" => "teams.view",
                "component" => "menu",
                "location" => "1",
                "is_active" => "0",
                "ordering" => "4"
            ],
            "Teams Widget" => [
                "type_id" => "0",
                "m_connection" => "teams.view",
                "component" => "widget",
                "location" => "1",
                "is_active" => "1",
                "ordering" => "5"
            ],
            "Teams" => [
                "type_id" => "0",
                "m_connection" => "profile.index",
                "component" => "profile",
                "location" => "1",
                "is_active" => "1",
                "ordering" => "4"
            ],
            "Teams Admin" => [
                "type_id" => "0",
                "m_connection" => "teams.view",
                "component" => "admin",
                "location" => "3",
                "is_active" => "1",
                "ordering" => "6"
            ],
            "Categories" => [
                "type_id" => "0",
                "m_connection" => "teams.index",
                "component" => "category",
                "location" => "1",
                "is_active" => "1",
                "ordering" => "10"
            ],
            "Feed display" => [
                "type_id" => "0",
                "m_connection" => "teams.view",
                "component" => "display",
                "location" => "2",
                "is_active" => "1",
                "ordering" => "10",
                "module_id" => "feed"
            ],
            "Team Events" => [
                "type_id" => "0",
                "m_connection" => "teams.view",
                "component" => "events",
                "location" => "3",
                "is_active" => "1",
                "ordering" => "7"
            ],
            "Related Teams" => [
                "type_id" => "0",
                "m_connection" => "teams.view",
                "component" => "related",
                "location" => "1",
                "is_active" => "1",
                "ordering" => "8"
            ],
            'Featured Teams' => [
                'type_id' => '0',
                'm_connection' => 'teams.index',
                'component' => 'featured',
                'location' => '3',
                'is_active' => '1',
                'ordering' => '1',
            ],
            'Sponsored Teams' => [
                'type_id' => '0',
                'm_connection' => 'teams.index',
                'component' => 'sponsored',
                'location' => '3',
                'is_active' => '1',
                'ordering' => '2',
            ],
        ];
    }

    protected function setPhrase()
    {
        $this->phrase = $this->_app_phrases;
    }

    protected function setOthers()
    {
        //$this->admincp_route = "/teams/admincp";
		$this->admincp_route = Phpfox::getLib('url')->makeUrl('admincp.app.settings', ['id' => 'PHPfox_Teams']);
        $this->admincp_menu = [
            'Settings' => '#'
        ];
        $this->menu = [
            "phrase_var_name" => "menu_teams",
            "url" => "teams",
            "icon" => "users"
        ];
        $this->_writable_dirs = [
            'PF.Base/file/pic/pages/'
        ];
        $this->_publisher = 'phpmasterminds';
        $this->_publisher_url = '';
        $this->_admin_cp_menu_ajax = false;
        $this->_apps_dir = 'core-teams';
        // database tables
        $this->database = [
            'PagesAdminTable',
            'PagesCategoryTable',
            'PagesClaimTable',
            'PagesFeedCommentTable',
            'PagesFeedTable',
            'PagesInviteTable',
            'PagesLoginTable',
            'PagesPermTable',
            'PagesSignupTable',
            'PagesTable',
            'PagesTextTable',
            'PagesTypeTable',
            'PagesTypeLinksTable',
            'PagesUrlTable',
            'PagesWidgetTable',
            'PagesWidgetTextTable'
        ];
        $this->allow_remove_database = false; // do not allow user to remove database
    }
}
