<?php

namespace Apps\Core_Sports\Block;

use Phpfox;
use Phpfox_Component;
use Phpfox_Database;

defined('PHPFOX') or exit('NO DICE!');

class JoinsportsBlock extends Phpfox_Component
{

    public function process()
    {
		
		$aSportsId = $this->getParam('id');
		$this->template()->assign(array(
                'aSportsId' => $aSportsId,
				'aSportsOptions'=> Phpfox::getService('sports')->getSportsOptionsDropDown()
            )
        );
    }
}