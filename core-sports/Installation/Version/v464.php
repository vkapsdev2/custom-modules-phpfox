<?php
namespace Apps\Core_Sports\Installation\Version;

use Phpfox_Queue;

/**
 * Class v464
 * @package Apps\Core_Sports\Installation\Version
 */
class v464
{
    public function process()
    {
        db()->delete(':block', 'component = \'related\' AND module_id = \'sports\' AND m_connection = \'sports.view\'');
        Phpfox_Queue::instance()->addJob('sports_convert_old_location', [], null, 3600);
    }
}
