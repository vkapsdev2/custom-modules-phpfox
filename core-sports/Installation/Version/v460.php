<?php

namespace Apps\Core_Sports\Installation\Version;

use Phpfox;

class v460
{

    private $_aSportsCategories;

    public function __construct()
    {
        $this->_aSportsCategories = array(
            'Community',
            'Houses',
            'Jobs',
            'Pets',
            'Rentals',
            'Services',
            'Stuff',
            'Tickets',
            'Vehicle'
        );
    }

    public function process()
    {
        // add activity field
        if (!db()->isField(':user_activity', 'activity_sports')) {
            db()->query("ALTER TABLE  `" . Phpfox::getT('user_activity') . "` ADD `activity_sports` INT( 10 ) UNSIGNED NOT NULL DEFAULT '0'");
        }

        // add statistics total blog field
        if (!db()->isField(':user_field', 'total_listing')) {
            db()->query("ALTER TABLE  `" . Phpfox::getT('user_field') . "` ADD `total_listing` INT( 10 ) UNSIGNED NOT NULL DEFAULT '0'");
        }
        // add default category
        $iTotalCategory = db()
            ->select('COUNT(category_id)')
            ->from(':sports_category')
            ->execute('getField');
        if ($iTotalCategory == 0) {
            foreach ($this->_aSportsCategories as $iCategoryOrder => $sCategory) {
                db()->insert(':sports_category', array(
                        'parent_id' => 0,
                        'name' => $sCategory,
                        'time_stamp' => PHPFOX_TIME,
                        'ordering' => $iCategoryOrder,
                        'used' => 0,
                        'is_active' => 1
                    )
                );
            }
        }

        // remove settings
        db()->delete(':setting', 'module_id="sports" AND var_name="sports_view_time_stamp"');
        // remove menu add
        db()->delete(':menu', "`module_id` = 'sports' AND `url_value` = 'sports.add'");

        db()->update(':module', ['phrase_var_name' => 'module_apps','is_active' => 1], ['module_id' => 'sports']);

        //Add cron
        $iCron = db()->select('COUNT(*)')
            ->from(':cron')
            ->where('module_id = \'sports\'')
            ->execute('getSlaveField');
        if (!$iCron) {
            db()->insert(Phpfox::getT('cron'), [
                'module_id' => 'sports',
                'product_id' => 'phpfox',
                'type_id' => 2,
                'every' => 1,
                'is_active' => 1,
                'php_code' => 'Phpfox::getService(\'sports.process\')->sendExpireNotifications();'
            ]);
        }
        // Update old settings
        $aSettingsMoreFrom = array(
            'limit' => Phpfox::getParam('sports.total_listing_more_from', 4),
            'cache_time' => Phpfox::getParam('core.cache_time_default'),
        );
        db()->update(':block', array('params' => json_encode($aSettingsMoreFrom)),
            'component = \'my\' AND module_id = \'sports\' AND params IS NULL');
        db()->delete(':setting', 'module_id="sports" AND var_name="total_listing_more_from"');

        $aSettingsSponsor = array(
            'limit' => Phpfox::getParam('sports.how_many_sponsored_listings', 4),
            'cache_time' => Phpfox::getParam('core.cache_time_default'),
        );
        db()->update(':block', array('params' => json_encode($aSettingsSponsor)),
            'component = \'sponsored\' AND module_id = \'sports\' AND params IS NULL');
        db()->delete(':setting', 'module_id="sports" AND var_name="how_many_sponsored_listings"');
    }
}