<?php
namespace Apps\Core_Sports\Installation\Database;

use Core\App\Install\Database\Table as Table;

/**
 * Class Sports_Category_Data
 * @package Apps\Core_Sports\Installation\Database
 */
class Sports_Category_Data extends Table
{
    /**
     *
     */
    protected function setTableName()
    {
        $this->_table_name = 'sports_category_data';
    }

    /**
     *
     */
    protected function setFieldParams()
    {
        $this->_aFieldParams = [
            'listing_id' => [
                'type' => 'int',
                'type_value' => '10',
                'other' => 'UNSIGNED NOT NULL',
                'primary_key' => true,
            ],
            'category_id' => [
                'type' => 'int',
                'type_value' => '10',
                'other' => 'UNSIGNED NOT NULL',
                'primary_key' => true,
            ],
        ];
    }

    /**
     * Set keys of table
     */
    protected function setKeys()
    {
        $this->_key = [
            'category_id' => ['category_id'],
            'listing_id' => ['listing_id'],
        ];
    }
}