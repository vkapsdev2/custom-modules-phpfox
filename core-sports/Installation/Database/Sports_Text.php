<?php
namespace Apps\Core_Sports\Installation\Database;

use Core\App\Install\Database\Table as Table;

/**
 * Class Sports_Text
 * @package Apps\Core_Sports\Installation\Database
 */
class Sports_Text extends Table
{
    /**
     *
     */
    protected function setTableName()
    {
        $this->_table_name = 'sports_text';
    }

    /**
     *
     */
    protected function setFieldParams()
    {
        $this->_aFieldParams = [
            'listing_id' => [
                'type' => 'int',
                'type_value' => '10',
                'other' => 'UNSIGNED NOT NULL',
                'primary_key' => true,
            ],
            'description' => [
                'type' => 'mediumtext',
                'other' => 'NULL',
            ],
            'description_parsed' => [
                'type' => 'mediumtext',
                'other' => 'NULL',
            ],

        ];
    }

    /**
     * Set keys of table
     */
    protected function setKeys()
    {
        $this->_key = [];
    }
}