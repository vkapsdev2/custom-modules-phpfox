<?php
namespace Apps\Core_Sports;

use Core\App;
use Phpfox_Url;

/**
 * Class Install
 * @author  phpFox
 * @package Apps\Core_Sports
 */
class Install extends App\App
{
    private $_app_phrases = [

    ];

    public $store_id = '';

    protected function setId()
    {
        $this->id = 'Core_Sports';
    }

    protected function setAlias()
    {
        $this->alias = 'sports';
    }

    protected function setName()
    {
        $this->name = _p('Sports');
    }

    protected function setVersion()
    {
        $this->version = '1.0';
    }

    protected function setSupportVersion()
    {
        $this->start_support_version = '4.7.8';
    }

    protected function setSettings()
    {
        $iIndex = 1;
        $this->settings = [
            'sports_paging_mode' => [
                'var_name' => 'sports_paging_mode',
                'info' => 'Pagination Style',
                'description' => 'Select Pagination Style at Search Page.',
                'type' => 'select',
                'value' => 'loadmore',
                'options' => [
                    'loadmore' => 'Scrolling down to Load More items',
                    'next_prev' => 'Use Next and Prev buttons',
                    'pagination' => 'Use Pagination with page number'
                ],
                'ordering' => $iIndex++,
            ],
            'sports_meta_description' => [
                'var_name' => 'sports_meta_description',
                'info' => 'Sports Meta Description',
                'description' => 'Meta description added to pages related to the Sports app. <a role="button" onclick="$Core.editMeta(\'seo_sports_meta_description\', true)">Click here</a> to edit meta description.<span style="float:right;">(SEO) <input style="width:150px;" readonly value="seo_sports_meta_description"></span>',
                'type' => '',
                'value' => '{_p var=\'seo_sports_meta_description\'}',
                'group_id' => 'seo',
                'ordering' => $iIndex++,
            ],
            'sports_meta_keywords' => [
                'var_name' => 'sports_setting_meta_keywords',
                'info' => 'Sports Meta Keywords',
                'description' => 'Meta keywords that will be displayed on sections related to the Sports app. <a role="button" onclick="$Core.editMeta(\'seo_sports_meta_keywords\', true)">Click here</a> to edit meta keywords.<span style="float:right;">(SEO) <input style="width:150px;" readonly value="seo_sports_meta_keywords"></span>',
                'type' => '',
                'value' => '{_p var=\'seo_sports_meta_keywords\'}',
                'group_id' => 'seo',
                'ordering' => $iIndex++
            ],
            'sports_allow_create_feed_when_add_new_item' => [
                'var_name' => 'sports_allow_posting_on_main_feed',
                'info' => 'Allow posting on Main Feed',
                'description' => 'Allow posting on Main feed when adding a new listing.',
                'type' => 'boolean',
                'value' => '1',
                'ordering' => $iIndex++
            ],
        ];
        unset($iIndex);
    }

    protected function setUserGroupSettings()
    {
        $iIndex = 1;
        $this->user_group_settings = [
            'can_post_comment_on_sports' => [
                'var_name' => 'can_post_comment_on_sports',
                'info' => 'Can members of this user group post a comment on sports listings?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '1',
                    '3' => '0',
                    '4' => '1',
                    '5' => '1'
                ],
                'ordering' => $iIndex++
            ],
            'can_access_sports' => [
                'var_name' => 'can_access_sports',
                'info' => 'Can members of this user group browse and view Sports Listings?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '1',
                    '3' => '1',
                    '4' => '1',
                    '5' => '0'
                ],
                'ordering' => $iIndex++
            ],
            'can_create_sports' => [
                'var_name' => 'can_create_sports',
                'info' => 'Can members of this user group create a sports?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '1',
                    '3' => '0',
                    '4' => '1',
                    '5' => '0'
                ],
                'ordering' => $iIndex++
            ],
            'can_edit_own_listing' => [
                'var_name' => 'can_edit_own_listing',
                'info' => 'Can members of this user group edit own sports listings?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '1',
                    '3' => '0',
                    '4' => '1',
                    '5' => '1'
                ],
                'ordering' => $iIndex++
            ],
            'can_edit_other_listing' => [
                'var_name' => 'can_edit_other_listing',
                'info' => 'Can members of this user group edit all sports listings?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '0',
                    '3' => '0',
                    '4' => '1',
                    '5' => '0'
                ],
                'ordering' => $iIndex++
            ],
            'can_delete_own_listing' => [
                'var_name' => 'can_delete_own_listing',
                'info' => 'Can members of this user group delete own sports listings?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '1',
                    '3' => '0',
                    '4' => '1',
                    '5' => '1'
                ],
                'ordering' => $iIndex++
            ],
            'can_delete_other_listings' => [
                'var_name' => 'can_delete_other_listings',
                'info' => 'Can members of this user group delete all sports listings?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '0',
                    '3' => '0',
                    '4' => '1',
                    '5' => '0'
                ],
                'ordering' => $iIndex++
            ],
            'max_upload_size_listing' => [
                'var_name' => 'max_upload_size_listing',
                'info' => 'Max file size for photos upload in kilobytes (kb), (1024 kb = 1 mb). For unlimited add "0" without quotes.',
                'description' => '',
                'type' => 'integer',
                'value' => [
                    '1' => '8192',
                    '2' => '8192',
                    '3' => '8192',
                    '4' => '8192',
                    '5' => '8192'
                ],
                'ordering' => $iIndex++
            ],
            'can_feature_listings' => [
                'var_name' => 'can_feature_listings',
                'info' => 'Can members of this user group feature a sports listings?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '0',
                    '3' => '0',
                    '4' => '1',
                    '5' => '0'
                ],
                'ordering' => $iIndex++
            ],
            'listing_approve' => [
                'var_name' => 'listing_approve',
                'info' => 'Listings must be approved first before they are displayed publicly?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '0',
                    '2' => '0',
                    '3' => '0',
                    '4' => '0',
                    '5' => '0'
                ],
                'ordering' => $iIndex++
            ],
            'can_approve_listings' => [
                'var_name' => 'can_approve_listings',
                'info' => 'Can members of this user group approve sports listings?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '0',
                    '3' => '0',
                    '4' => '1',
                    '5' => '0'
                ],
                'ordering' => $iIndex++
            ],
            'can_sponsor_sports' => [
                'var_name' => 'can_sponsor_sports',
                'info' => 'Can members of this user group mark a sports listing as Sponsor without paying fee?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '0',
                    '3' => '0',
                    '4' => '0',
                    '5' => '0'
                ],
                'ordering' => $iIndex++
            ],
            'can_purchase_sponsor' => [
                'var_name' => 'can_purchase_sponsor',
                'info' => 'Can members of this user group purchase a sponsored ad space for their items?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '0',
                    '3' => '0',
                    '4' => '0',
                    '5' => '0'
                ],
                'ordering' => $iIndex++
            ],
            'sports_sponsor_price' => [
                'var_name' => 'sports_sponsor_price',
                'info' => 'How much is the sponsor space worth for sports listings? This works in a CPM basis.',
                'description' => '',
                'type' => 'currency',
                'ordering' => $iIndex++
            ],
            'auto_publish_sponsored_item' => [
                'var_name' => 'auto_publish_sponsored_item',
                'info' => 'Auto publish sponsored item?',
                'description' => 'After the user has purchased a sponsored space, should the item be published right away? 
If set to No, the admin will have to approve each new purchased sponsored item space before it is shown in the site.',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '0',
                    '3' => '0',
                    '4' => '0',
                    '5' => '0'
                ],
                'ordering' => $iIndex++
            ],
            'flood_control_sports' => [
                'var_name' => 'flood_control_sports',
                'info' => 'How many minutes should a user wait before they can create another sports listing? Note: Setting it to "0" (without quotes) is default and users will not have to wait.',
                'description' => '',
                'type' => 'integer',
                'value' => [
                    '1' => '0',
                    '2' => '0',
                    '3' => '0',
                    '4' => '0',
                    '5' => '0'
                ],
                'ordering' => $iIndex++
            ],
            'points_sports' => [
                'var_name' => 'points_sports',
                'info' => 'How many activity points should the user get when adding a new listing?',
                'type' => 'integer',
                'value' => [
                    '1' => '1',
                    '2' => '1',
                    '3' => '1',
                    '4' => '1',
                    '5' => '1'
                ],
                'ordering' => $iIndex++
            ],
            'total_photo_upload_limit' => [
                'var_name' => 'total_photo_upload_limit',
                'info' => 'Control how many photos a user can upload to a sports listing.',
                'description' => '',
                'type' => 'integer',
                'value' => [
                    '1' => '6',
                    '2' => '6',
                    '3' => '6',
                    '4' => '6',
                    '5' => '6'
                ],
                'ordering' => $iIndex++
            ],
			'can_post_comment_on_sports_viewpage' => [
                'var_name' => 'can_post_comment_on_sports_viewpage',
                'info' => 'Can members of this user group post a comment on sports listings view page?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    '1' => '1',
                    '2' => '1',
                    '3' => '0',
                    '4' => '1',
                    '5' => '1'
                ],
                'ordering' => $iIndex++
            ],
        ];
        unset($iIndex);
    }

    protected function setComponent()
    {
        $this->component = [
            'block' => [
                'menu' => '',
                'profile' => '',
                'info' => '',
                'my' => '',
                'category' => '',
                'sponsored' => '',
                'featured' => '',
                'invite' => '',
                'related' => ''
            ],
            'controller' => [
                'index' => 'sports.index',
                'view' => 'sports.view',
                'invoice' => 'sports.invoice',
                'profile' => 'sports.profile',
            ]
        ];
    }

    protected function setComponentBlock()
    {
        $this->component_block = [
            'Map View' => [
                'type_id' => '0',
                'm_connection' => 'sports.index',
                'component' => 'gmap-block',
                'module_id' => 'core',
                'location' => '1',
                'is_active' => '1',
                'ordering' => '1',
            ],
            'Sponsored' => [
                'type_id' => '0',
                'm_connection' => 'sports.index',
                'component' => 'sponsored',
                'location' => '3',
                'is_active' => '1',
                'ordering' => '1',
            ],
            'Users Invites' => [
                'type_id' => '0',
                'm_connection' => 'sports.index',
                'component' => 'invite',
                'location' => '3',
                'is_active' => '1',
                'ordering' => '2',
            ],
            'Featured Listings' => [
                'type_id' => '0',
                'm_connection' => 'sports.index',
                'component' => 'featured',
                'location' => '3',
                'is_active' => '1',
                'ordering' => '3',
            ],
            'Category' => [
                'type_id' => '0',
                'm_connection' => 'sports.index',
                'component' => 'category',
                'location' => '1',
                'is_active' => '1',
                'ordering' => '2',
            ],
            'Sponsored ' => [
                'type_id' => '0',
                'm_connection' => 'sports.invoice',
                'component' => 'sponsored',
                'location' => '3',
                'is_active' => '1',
                'ordering' => '1',
            ],
            'Users Invites ' => [
                'type_id' => '0',
                'm_connection' => 'sports.invoice',
                'component' => 'invite',
                'location' => '3',
                'is_active' => '1',
                'ordering' => '2',
            ],
            'Featured Listings ' => [
                'type_id' => '0',
                'm_connection' => 'sports.invoice',
                'component' => 'featured',
                'location' => '3',
                'is_active' => '1',
                'ordering' => '3',
            ],
            'Category ' => [
                'type_id' => '0',
                'm_connection' => 'sports.invoice',
                'component' => 'category',
                'location' => '1',
                'is_active' => '1',
                'ordering' => '2',
            ],
            'More From Seller' => [
                'type_id' => '0',
                'm_connection' => 'sports.view',
                'component' => 'my',
                'location' => '3',
                'is_active' => '1',
                'ordering' => '1',
            ],
            'Categories' => [
                'type_id' => '0',
                'm_connection' => 'sports.view',
                'component' => 'category',
                'location' => '1',
                'is_active' => '1',
                'ordering' => '1',
            ]
        ];
    }

    protected function setPhrase()
    {
        $this->phrase = $this->_app_phrases;
    }

    protected function setOthers()
    {
        $this->admincp_route = '/sports/admincp';
        $this->admincp_menu = [
            _p('Manage Sports') => '/sports/listsports',
            _p('Add Sports') => '/sports/addnew',
            _p('Sports Option') => '/sports/listoptions',
            _p('Add Sports Options') => '/sports/sportoption',
        ];
        $this->map = [];
        $this->menu = [
            'phrase_var_name' => 'menu_sports',
            'url' => 'sports',
            'icon' => 'usd'
        ];
        $this->database = [
            'Sports',
            'Sports_Text',
            'Sports_Image',
            'Sports_Category',
            'Sports_Category_Data',
            'Sports_Invite',
            'Sports_Invoice',
        ];
        $this->_apps_dir = 'core-sports';
        $this->_admin_cp_menu_ajax = false;
        $this->_publisher = 'phpmasterminds';
        $this->_publisher_url = '';
        $this->_writable_dirs = [
            'PF.Base/file/pic/sports/'
        ];
		$this->allow_remove_database = false; // do not allow user to remove database
    }
}
