<?php
namespace Apps\Core_Sports;

use Phpfox;
use Phpfox_Module;

Phpfox_Module::instance()
    ->addAliasNames('sports', 'Core_Sports')
    ->addServiceNames([
        'sports.category' => Service\Category\Category::class,
        'sports.category.process' => Service\Category\Process::class,
        'sports' => Service\Sports::class,
        'sports.process' => Service\Process::class,
        'sports.callback' => Service\Callback::class,
        'sports.browse' => Service\Browse::class
    ])
    ->addTemplateDirs([
        'sports' => PHPFOX_DIR_SITE_APPS . 'core-sports' . PHPFOX_DS . 'views'
    ])
    ->addComponentNames('controller', [
        'sports.admincp.add' => Controller\Admin\AddController::class,
        'sports.admincp.addnew' => Controller\Admin\AddnewController::class,
        'sports.admincp.listoptions' => Controller\Admin\ListoptionsController::class,
        'sports.admincp.sportoption' => Controller\Admin\SportoptionController::class,
        'sports.admincp.index' => Controller\Admin\IndexController::class,
        'sports.admincp.delete' => Controller\Admin\DeleteController::class,
        'sports.admincp.listsports' => Controller\Admin\ListsportsController::class,
        'sports.invoice.index' => Controller\Invoice\IndexController::class,
        'sports.add' => Controller\AddController::class,
        'sports.index' => Controller\IndexController::class,
        'sports.profile' => Controller\ProfileController::class,
        'sports.purchase' => Controller\PurchaseController::class,
        'sports.view' => Controller\ViewController::class,
        'sports.frame-upload' => Controller\FrameUploadController::class
    ])
    ->addComponentNames('ajax', [
        'sports.ajax' => Ajax\Ajax::class
    ])
    ->addComponentNames('block', [
        'sports.category' => Block\CategoryBlock::class,
        'sports.featured' => Block\FeaturedBlock::class,
        'sports.feed' => Block\FeedBlock::class,
        'sports.info' => Block\InfoBlock::class,
        'sports.invite' => Block\InviteBlock::class,
        'sports.list' => Block\ListBlock::class,
        'sports.menu' => Block\MenuBlock::class,
        'sports.my' => Block\MyBlock::class,
        'sports.related' => Block\RelatedBlock::class,
        'sports.photo' => Block\PhotoBlock::class,
        'sports.profile' => Block\ProfileBlock::class,
        'sports.rows' => Block\RowsBlock::class,
        'sports.joinsports' => Block\JoinsportsBlock::class,
        'sports.sponsored' => Block\SponsoredBlock::class
    ]);
group('/sports', function () {
    // BackEnd routes
    route('/admincp', function () {
        auth()->isAdmin(true);
        Phpfox::getLib('module')->dispatch('sports.admincp.index');
        return 'controller';
    });

    route('/admincp/category/order', function () {
        auth()->isAdmin(true);
        $ids = request()->get('ids');
        $ids = trim($ids, ',');
        $ids = explode(',', $ids);
        $values = [];
        foreach ($ids as $key => $id) {
            $values[$id] = $key + 1;
        }
        Phpfox::getService('core.process')->updateOrdering([
                'table' => 'sports_category',
                'key' => 'category_id',
                'values' => $values,
            ]
        );
        Phpfox::getLib('cache')->removeGroup('sports_category');
        return true;
    });
    route('/', 'sports.index');
    route('/invoice/*', 'sports.invoice.index');
    route('/purchase/*', 'sports.purchase');
    route('/category/:id/:name/*', 'sports.index')->where([':id' => '([0-9]+)']);
    route('/:id/*', 'sports.view')->where([':id' => '([0-9]+)']);
    route('/add/*', 'sports.add');
    route('/frame-upload', 'sports.frame-upload');
    route('/map', function(){
        if (Phpfox::getUserParam('sports.can_create_sports')) {
            sectionMenu(_p('menu_add_new_listing'), 'sports.add');
        }
        Phpfox::getService('sports')->buildSectionMenu();
        Phpfox::getLib('module')->dispatch('core.gmap');
        return 'controller';
    });
});

$sDefaultListingPhoto = flavor()->active->default_photo('sports_default_photo', true);
if (!$sDefaultListingPhoto) {
    $sDefaultListingPhoto = setting('core.path_actual') . 'PF.Site/Apps/core-sports/assets/image/no_image.png';
}

Phpfox::getLib('setting')->setParam('sports.sports_default_photo', $sDefaultListingPhoto);

Phpfox::getLib('setting')->setParam('sports.thumbnail_sizes', array(50, 120, 200, 400));

Phpfox::getLib('setting')->setParam('sports.dir_image', Phpfox::getParam('core.dir_pic') . 'sports/');
Phpfox::getLib('setting')->setParam('sports.url_image', Phpfox::getParam('core.url_pic') . 'sports/');
