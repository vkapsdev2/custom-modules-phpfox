<?php
/**
 * [PHPFOX_HEADER]
 */

namespace Apps\Core_Sports\Controller\Admin;

use Phpfox;
use Phpfox_Component;
use Phpfox_Error;
use Phpfox_Plugin;
use Phpfox_Validator;

defined('PHPFOX') or exit('NO DICE!');


class SportoptionController extends Phpfox_Component
{
    /**
     * Controller
     */
    public function process()
    {
        Phpfox::isUser(true);
        $bIsEdit = false;

        if ($iEditId = $this->request()->getInt('id')) {
            if (($aListing = Phpfox::getService('sports')->getOptionsForEdit($iEditId))) {
                $bIsEdit = true;
                $this->template()
                    ->assign([
                            'aForms' => $aListing
                        ]
                    );
            }
        } else {
        }

        $aValidation = [
            'title' => _p('Provide title'),
        ];

        $oValidator = Phpfox_Validator::instance()->set([
                'sFormName' => 'js_sports_form',
                'aParams' => $aValidation
            ]
        );

        
		
		$this->template()
			->setBreadCrumb(($bIsEdit ? _p('Edit Sport option') . ': ' . $aListing['title'] : _p('Sport Options')), $bIsEdit ? $this->url()->makeUrl('sports.sportoption', ['id' => $iEditId]) : $this->url()->makeUrl('sports.sportoption'), true);
        

        if ($aVals = $this->request()->get('val')) {
            if ($oValidator->isValid($aVals)) {
                if ($bIsEdit) {
                    if (Phpfox::getService('sports.process')->updateSportOption($aListing['sports_option_id'], $aVals)) {
                        $this->url()->send('admincp.sports.listoptions', null,
                                    _p('Sports Option Successfully Updated.'));
                    }
                } else {
                   

                    if (Phpfox_Error::isPassed()) {
                        if ($iId = Phpfox::getService('sports.process')->addSportOption($aVals)) {
                           $this->url()->send('admincp.sports.listoptions', null,
                                    _p('Sports Option Successfully Added.'));
                        }
                    }
                }
            }
        }

       
	
       
        $this->template()->setTitle(_p('sports'))
            ->setTitle(($bIsEdit ? _p('Edit Sports Option') . ': ' . $aListing['title'] : _p('Create Sports Option')))
            ->setEditor()
            ->setPhrase([
                    'select_a_file_to_upload'
                ]
            )
            ->setHeader([
                    'country.js' => 'module_core',
                    '<script>var sports_valid_convert_rate = \''. json_encode($aValidConvertRate) .'\'</script>'
                ]
            )
            ->assign([
                    'sCreateJs' => $oValidator->createJS(),
                    'sGetJsForm' => $oValidator->getJsForm(false),
                    'bIsEdit' => $bIsEdit,
                ]
            );
        
        (($sPlugin = Phpfox_Plugin::get('sports.component_controller_add_process')) ? eval($sPlugin) : false);
    }

    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
        (($sPlugin = Phpfox_Plugin::get('sports.component_controller_add_clean')) ? eval($sPlugin) : false);
    }
	
	public function buildPageMenuCUS($sName, $aMenu, $aLink = null, $bIsFullLink = false){
		 $this->_aSectionMenu = [
            'name'        => $sName,
            'menu'        => $aMenu,
            'link'        => $aLink,
            'bIsFullLink' => $bIsFullLink
        ];

        // current url
        $sPageCurrentUrl = $this->url()->makeUrl('current');
        // current tab
        $sCurrentTab = $this->request()->get('req4');
		if(empty($sCurrentTab)){
			$sCurrentTab = $this->request()->get('tab');
		}
        // check active tab
        foreach ($aMenu as $sTabId => $sTabName) {
            if (($bIsFullLink && ($sTabId == $sPageCurrentUrl)) ||
                (!$bIsFullLink && $sCurrentTab && $sTabId == $sCurrentTab)
            ) {
                $sActiveTab = $sTabId;
            }
        }

        if (!isset($sActiveTab) && !$bIsFullLink) {
            // set first menu as active
            $sActiveTab = key($aMenu);
        }

        $this->template()->assign([
                'sPageSectionMenuName' => $sName,
                'aPageSectionMenu'     => $aMenu,
                'aPageExtraLink'       => $aLink,
                'bPageIsFullLink'      => $bIsFullLink,
                'sActiveTab'           => $sActiveTab
            ]
        );
	}
}