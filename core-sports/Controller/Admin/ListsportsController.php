<?php
/**
 * [PHPFOX_HEADER]
 */
namespace Apps\Core_Sports\Controller\Admin;


use Phpfox;
use Phpfox_Component;
use Phpfox_Pager;
use Phpfox_Plugin;
use Phpfox_Url;

defined('PHPFOX') or exit('NO DICE!');


class ListsportsController extends Phpfox_Component
{ 
    /**
     * Controller
     */
   public function process()
    {

        $this->_processSports();
        $iLimit = 10;
        $iPage = $this->request()->getInt('page', 1);
        $aSearch = $this->request()->getArray('search');
        $sSort = $this->request()->get('sort');
        $iLocation = $this->request()->get('location');
        $aConditions = $this->_getSearchConditions($aSearch);

        if (!empty($sSort)) {
            $this->search()->setSort(reset(explode(' ', $sSort)));
        }

        list($iCnt, $aSports) = Phpfox::getService('sports')->getSports($aConditions,
            empty($sSort) ? 'sp.listing_id DESC' : 'sp.' . $sSort, $iPage, $iLimit);

        Phpfox_Pager::instance()->set(array(
            'page' => $iPage,
            'size' => $iLimit,
            'count' => $iCnt
        ));

        $this->template()
            ->setTitle(_p('Manage Sports'))
            ->setBreadCrumb(_p('apps'), $this->url()->makeUrl('admincp.apps'))
            ->setBreadCrumb(_p('sports'), $this->url()->makeUrl('admincp.sports.listsports'))
            ->setBreadCrumb(_p('Manage Sports'))
            ->setPhrase([
                'are_you_sure_you_want_to_delete_selected_sports_permanently',
            ])
            ->assign([
                'aSports' => $aSports,
                'bIsSearch' => !empty($aSearch),
                'sCurrentSort' => empty($sSort) ? '' : $sSort,
                'aForms' => $aSearch ? $aSearch : [
                    'from_day' => date('j'),
                    'from_month' => date('n'),
                    'from_year' => date('Y'),
                    'to_day' => date('j'),
                    'to_month' => date('n'),
                    'to_year' => date('Y'),
                ]
            ]);

    }

    private function _processSports()
    {
        $aVals = $this->request()->getArray('val');
        $ids = [];

        if (!empty($aVals)) {
            $ids = array_filter($aVals['id']);
        }

        if (($iId = $this->request()->getInt('delete')) || (!empty($aVals) && isset($aVals['delete']))) {
            if (!empty($iId)) {
                $ids[] = $iId;
            }

            foreach ($ids as $id) {
                Phpfox::getService('sports.process')->delete($id);
            }

            $this->url()->send('admincp.sports.listsports', null, _p('sport_s_deleted_successfully'));
        }
    }

    private function _getSearchConditions($aParams)
    {
        if (empty($aParams)) {
            return [];
        }

        $aConditions = [];

        if (!empty($aParams['name'])) {
            $aConditions['sp.title'] = ['like' => '%' . $aParams['name'] . '%'];
        }

        
        return $aConditions;
    }

    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
        (($sPlugin = Phpfox_Plugin::get('sports.component_controller_admincp_index_clean')) ? eval($sPlugin) : false);
    }
}