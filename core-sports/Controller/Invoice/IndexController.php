<?php
/**
 * [PHPFOX_HEADER]
 */

namespace Apps\Core_Sports\Controller\Invoice;

use Phpfox;
use Phpfox_Component;
use Phpfox_Plugin;

defined('PHPFOX') or exit('NO DICE!');

define('PHPFOX_SKIP_POST_PROTECTION', true);


class IndexController extends Phpfox_Component
{
    /**
     * Controller
     */
    public function process()
    {
        Phpfox::isUser(true);

        $aCond = array();

        $aCond[] = 'AND mi.user_id = ' . Phpfox::getUserId();

        list(, $aInvoices) = Phpfox::getService('sports')->getInvoices($aCond);


        $this->template()->setTitle(_p('sports_invoices'))
            ->setBreadCrumb(_p('sports'), $this->url()->makeUrl('sports'))
            ->setBreadCrumb(_p('invoices'), $this->url()->makeUrl('sports.invoice'))
            ->assign(array(
                    'aInvoices' => $aInvoices
                )
            );

        Phpfox::getService('sports')->buildSectionMenu();
    }

    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
        (($sPlugin = Phpfox_Plugin::get('sports.component_controller_invoice_index_clean')) ? eval($sPlugin) : false);
    }
}