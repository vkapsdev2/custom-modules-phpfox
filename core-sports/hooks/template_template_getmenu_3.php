<?php
/**
 * Created by PhpStorm.
 * User: phpFox
 * Date: 5/31/17
 * Time: 16:04
 */
defined('PHPFOX') or exit('NO DICE!');

if (Phpfox::isAppActive('Core_Sports') && !Phpfox::getUserParam('sports.can_access_sports')) {
    foreach ($aMenus as $key => $value) {
        if ($value['module'] == 'sports' && ($value['url'] = 'sports' || $value['url'] = 'profile.sports')) {
            unset($aMenus[$key]);
            break;
        }
    }
}
