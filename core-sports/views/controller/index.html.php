<?php
	defined('PHPFOX') or exit('NO DICE!');
?>

{if (count($aListings))}
	<div class="item-container market-app listing" id="collection-item-listings">
	    {foreach from=$aListings name=listings item=aListing}
            {template file='sports.block.rows'}
	    {/foreach}
	</div>
{pager}
{elseif !PHPFOX_IS_AJAX}
	{_p var='No Sports Found'}
{/if}

{if $bShowModerator}
    {moderation}
{/if}