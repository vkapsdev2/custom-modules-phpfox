<?php

defined('PHPFOX') or exit('NO DICE!');

?>

{$sCreateJs}
<form method="post" class="form" action="{url link='current'}" enctype="multipart/form-data" onsubmit="$('#js_sports_submit_form').attr('disabled',true); return startProcess({$sGetJsForm}, false);" id="js_sports_form">

  
    {if $bIsEdit}
        <input type="hidden" name="id" value="{$aForms.sports_option_id}" />
    {/if}
  
    <div><input type="hidden" name="page_section_menu" value="" id="page_section_menu_form" /></div>

    <div id="js_mp_block_detail" class="js_mp_block page_section_menu_holder market-app add" {if !empty($sActiveTab) && $sActiveTab != 'detail'}style="display:none;"{/if}>
        <div class="form-group">
            <label for="title">{required} Option Name</label>
            <input class="form-control" type="text" name="val[title]" value="{value type='input' id='title'}" id="title" size="40" maxlength="100" />
        </div>
       
        <div class="form-group footer">
            <button type="submit" class="btn btn-primary" id="js_sports_submit_form">{if $bIsEdit}{_p var='update'}{else}{_p var='submit'}{/if}</button>
        </div>
    </div>

</form>
{section_menu_js}