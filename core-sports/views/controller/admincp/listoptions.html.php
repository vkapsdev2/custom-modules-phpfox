<?php 
defined('PHPFOX') or exit('NO DICE!');
?>

<form method="get" action="{url link='admincp.sports.listoptions'}" class="form">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                {_p var='Search Sports Options'}
            </div>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">{_p var='Option Name'}</label>
                        <input name="search[name]" class="form-control" id="name" value="{value type='input' id='name'}">
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" name="search[submit]" class="btn btn-primary"><i class="fa fa-search"></i> {_p var='search'}</button>
            <a role="button" href="{url link='admincp.sports.listoptions'}" class="btn btn-default">{_p var='reset'}</a>
        </div>
    </div>
</form>



{if count($aSports)}
<form method="post" action="{url link='admincp.sports.listoptions'}" id="manage_sports_form">
    <div class="panel panel-default">
        <table class="table table-admin">
            <tr>
                <th class="w20">
                    <div class="custom-checkbox-wrapper">
                        <label>
                            <input type="checkbox" name="val[id][]" value="" id="js_check_box_all" class="main_checkbox" />
                            <span class="custom-checkbox"></span>
                        </label>
                    </div>
                </th>
                <th class="w100 t_center">{_p var='Option Id'}</th>
                <th>{_p var='name'}</th>
                <th class="t_center w80">{_p var='settings'}</th>
            </tr>
            {foreach from=$aSports key=iKey item=aSport}
            <tr class="{if is_int($iKey/2)} tr{else}{/if}{if $aSport.is_custom && $aSport.is_custom == '2'} is_checked{/if}">
                <td class="t_center">
                    <div class="custom-checkbox-wrapper">
                        <label>
                            <input type="checkbox" name="val[id][]" class="checkbox" value="{$aSport.sports_option_id}" id="js_id_row{$aSport.sports_option_id}" />
                            <span class="custom-checkbox"></span>
                        </label>
                    </div>
                </td>
                <td class="t_center">{$aSport.sports_option_id}</td>
                <td><a href="{url link='admincp.sports.sportoption' id=$aSport.sports_option_id}">{$aSport.title|clean|convert}</a></td>
                <td class="t_center">
                    <a class="js_drop_down_link" title="{_p var='better_ads_manage'}"></a>
                    <div class="link_menu">
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="{url link='admincp.sports.sportoption' id=$aSport.sports_option_id}">{_p var='edit'}</a></li>
                            
                            <li><a href="{url link='admincp.sports.listoptions' delete=$aSport.sports_option_id}" class="sJsConfirm" data-message="{_p var='are_you_sure_you_want_to_delete_selected_sports_permanently'}">{_p var='delete'}</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        {/foreach}
        </table>
    </div>
    <div class="table_hover_action hide">
        <a role="button" class="btn btn-danger sJsCheckBoxButton disabled" disabled="disabled" onclick="$Core.sports.confirmSubmitForm(this, '#manage_sports_form')" data-action="delete">{_p var='delete_selected'}</a>
    </div>
</form>
{else}
<div class="alert alert-info">
	{if $bIsSearch}
	{_p var='better_ads_no_search_results_were_found'}.
	{else}
	{_p var='no options found'}.
	{/if}
</div>
{/if}
{pager}