<?php

defined('PHPFOX') or exit('NO DICE!');

?>
{if $bIsEdit && $aForms.view_id == '2'}
<div class="error_message">
    {_p var='notice_this_listing_is_marked_as_sold'}
</div>
{/if}

{$sCreateJs}
<form method="post" class="form" action="{url link='current'}" enctype="multipart/form-data" onsubmit="$('#js_sports_submit_form').attr('disabled',true); return startProcess({$sGetJsForm}, false);" id="js_sports_form">

    {if isset($iItem) && isset($sModule)}
        <div><input type="hidden" name="val[module_id]" value="{$sModule|htmlspecialchars}" /></div>
        <div><input type="hidden" name="val[item_id]" value="{$iItem|htmlspecialchars}" /></div>
    {/if}

    {if $bIsEdit}
        <input type="hidden" name="id" value="{$aForms.listing_id}" />
    {/if}
    <div id="js_custom_privacy_input_holder">
        {if $bIsEdit && Phpfox::isModule('privacy') && empty($sModule)}
            {module name='privacy.build' privacy_item_id=$aForms.listing_id privacy_module_id='sports'}
        {/if}
    </div>
    <div><input type="hidden" name="page_section_menu" value="" id="page_section_menu_form" /></div>
    <div><input type="hidden" name="val[attachment]" class="js_attachment" value="{value type='input' id='attachment'}" /></div>
    <div><input type="hidden" name="val[current_tab]" value="" id="current_tab"></div>

    <div id="js_mp_block_detail" class="js_mp_block page_section_menu_holder market-app add" {if !empty($sActiveTab) && $sActiveTab != 'detail'}style="display:none;"{/if}>
        <div class="form-group">
            <label for="title">{required} Title</label>
            <input class="form-control" type="text" name="val[title]" value="{value type='input' id='title'}" id="title" size="40" maxlength="100" />
        </div>
       
	   <div class="form-group">
            <label for="title">{required} Maximum Player</label>
            <input class="form-control" type="number" name="val[max_player]" value="{value type='input' id='max_player'}" id="max_player" size="5" min="1" maxlength="100" />
        </div>
		
	   <div class="form-group">
            <label for="mini_description">{_p var='short_description'}</label>
            <textarea id="mini_description" class="form-control" rows="1" name="val[mini_description]">{value type='textarea' id='mini_description'}</textarea>
        </div>

        <div class="form-group">
            <label for="description">{_p var='description'}</label>
            {editor id='description' rows='6' class='form-control'}
        </div>

        {if empty($sModule) && Phpfox::isModule('privacy')}
            <div class="form-group" style="display:none">
                <label>{_p var='listing_privacy'}</label>
                {module name='privacy.form' privacy_info='control_who_can_see_this_listing' privacy_name='privacy' default_privacy='sports.display_on_profile'}
            </div>
        {/if}
        <div class="form-group footer">
            <button type="submit" class="btn btn-primary" id="js_sports_submit_form">{if $bIsEdit}{_p var='update'}{else}{_p var='submit'}{/if}</button>
        </div>
    </div>

    <div id="js_mp_block_customize" class="js_mp_block page_section_menu_holder" {if empty($sActiveTab) || $sActiveTab != 'customize'}style="display:none;"{/if}>
        {module name='sports.photo'}
    </div>

    <div id="js_mp_block_invite" class="js_mp_block page_section_menu_holder" {if empty($sActiveTab) || $sActiveTab != 'invite'}style="display:none;"{/if}>
        <div class="block">
            {if Phpfox::isModule('friend')}
                <div class="form-group">
                    <label for="js_find_friend">{_p var='invite_friends'}</label>
                    {if isset($aForms.listing_id)}
                        <div id="js_selected_friends" class="hide_it"></div>
                        {module name='friend.search' input='invite' hide=true friend_item_id=$aForms.listing_id friend_module_id='sports'}
                    {/if}
                </div>
            {/if}
            <div class="form-group invite-friend-by-email">
                <label for="emails">{_p var='invite_people_via_email'}</label>
                <input name="val[emails]" id="invite_people_via_email" class="form-control" data-component="tokenfield" data-type="email" >
                <p class="help-block">
                    {_p var='separate_multiple_emails_with_comma_or_enter_or_tab'}
                </p>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="val[invite_from]" value="1"> {_p var='send_from_my_own_address_semail' sEmail=$sMyEmail}
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label for="add_a_personal_message">{_p var='add_a_personal_message'}</label>
                <textarea rows="3" name="val[personal_message]" id="add_a_personal_message" class="form-control" placeholder="{_p var='write_message'}"></textarea>
            </div>
            <div class="form-group">
                <input type="submit" value="{_p var='send_invitations'}" class="btn btn-primary" name="invite_submit"/>
            </div>
        </div>
    </div>

    {if isset($aForms.listing_id) && $bIsEdit}
        <div id="js_mp_block_manage" class="js_mp_block page_section_menu_holder" {if empty($sActiveTab) || $sActiveTab != 'manage'}style="display:none;"{/if}>
            {module name='sports.list'}
        </div>
    {/if}
</form>
{section_menu_js}