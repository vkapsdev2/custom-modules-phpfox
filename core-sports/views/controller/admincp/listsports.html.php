<?php 
defined('PHPFOX') or exit('NO DICE!');
?>

<form method="get" action="{url link='admincp.sports.listsports'}" class="form">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                {_p var='Search Sports'}
            </div>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">{_p var='Sports Name'}</label>
                        <input name="search[name]" class="form-control" id="name" value="{value type='input' id='name'}">
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" name="search[submit]" class="btn btn-primary"><i class="fa fa-search"></i> {_p var='search'}</button>
            <a role="button" href="{url link='admincp.sports.listsports'}" class="btn btn-default">{_p var='reset'}</a>
        </div>
    </div>
</form>

{if $iPendingCount > 0}
<div class="alert alert-warning">
    {if $iPendingCount > 1}
        {_p var='better_ads_there_are_number_pending_ads_that_require_your_attention' link=$sPendingLink number=$iPendingCount}
    {else}
        {_p var='better_ads_there_is_1_pending_ad_that_require_your_attention' link=$sPendingLink}
    {/if}
</div>
{/if}

{if count($aSports)}
<form method="post" action="{url link='admincp.sports.listsports'}" id="manage_sports_form">
    <div class="panel panel-default">
        <table class="table table-admin">
            <tr>
                <th class="w20">
                    <div class="custom-checkbox-wrapper">
                        <label>
                            <input type="checkbox" name="val[id][]" value="" id="js_check_box_all" class="main_checkbox" />
                            <span class="custom-checkbox"></span>
                        </label>
                    </div>
                </th>
                <th class="w100 t_center">{_p var='Sports Id'}</th>
                <th>{_p var='name'}</th>
                <th class="">{_p var='user'}</th>
                <th class="">{_p var='image'}</th>
                <th class="t_center w80">{_p var='settings'}</th>
            </tr>
            {foreach from=$aSports key=iKey item=aSport}
            <tr class="{if is_int($iKey/2)} tr{else}{/if}{if $aSport.is_custom && $aSport.is_custom == '2'} is_checked{/if}">
                <td class="t_center">
                    <div class="custom-checkbox-wrapper">
                        <label>
                            <input type="checkbox" name="val[id][]" class="checkbox" value="{$aSport.listing_id}" id="js_id_row{$aSport.listing_id}" />
                            <span class="custom-checkbox"></span>
                        </label>
                    </div>
                </td>
                <td class="t_center">{$aSport.listing_id}</td>
                <td><a href="{url link='admincp.sports.addnew' id=$aSport.listing_id}">{$aSport.title|clean|convert}</a></td>
                <td>{$aSport.user|user}</td>
                <td>
				{if !empty($aSport.image_path)}
                    {img server_id=$aSport.server_id title=$aSport.title path='sports.url_image' file=$aSport.image_path  suffix='_200_square' width="100px"}
                {else}
                   <img src="{param var='sports.sports_default_photo'}" width="100px"/>
                {/if}
				</td>
                <td class="t_center">
                    <a class="js_drop_down_link" title="{_p var='better_ads_manage'}"></a>
                    <div class="link_menu">
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="{url link='admincp.sports.addnew' id=$aSport.listing_id}">{_p var='edit'}</a></li>
                            
                            <li><a href="{url link='admincp.sports.listsports' delete=$aSport.listing_id}" class="sJsConfirm" data-message="{_p var='are_you_sure_you_want_to_delete_selected_sports_permanently'}">{_p var='delete'}</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        {/foreach}
        </table>
    </div>
    <div class="table_hover_action hide">
        <a role="button" class="btn btn-danger sJsCheckBoxButton disabled" disabled="disabled" onclick="$Core.sports.confirmSubmitForm(this, '#manage_sports_form')" data-action="delete">{_p var='delete_selected'}</a>
    </div>
</form>
{else}
<div class="alert alert-info">
	{if $bIsSearch}
	{_p var='better_ads_no_search_results_were_found'}.
	{else}
	{_p var='better_ads_no_ads_have_been_created'}.
	{/if}
</div>
{/if}
{pager}