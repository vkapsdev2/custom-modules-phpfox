<?php
    defined('PHPFOX') or exit('NO DICE!');
?>
<article class="{if $aListing.is_sponsor}is_sponsored {/if}{if $aListing.is_featured}is_featured {/if} {if $aListing.hasPermission}has-action{/if}" id="js_mp_item_holder_{$aListing.listing_id}">
    <div class="item-outer">
        <div class="item-media">
            <a href="{$aListing.url}" class="mp_listing_image"
            style="background-image: url(
                {if !empty($aListing.image_path)}
                    {img server_id=$aListing.server_id title=$aListing.title path='sports.url_image' file=$aListing.image_path  suffix='_400_square' return_url=true}
                {else}
                    {param var='sports.sports_default_photo'}
                {/if}
            )" >
            </a>
            <div class="flag_style_parent">
                {if isset($sListingView) && $sListingView == 'my' && $aListing.view_id == 1}
                    <div class="sticky-label-icon sticky-pending-icon">
                        <span class="flag-style-arrow"></span>
                        <i class="ico ico-clock-o"></i>
                    </div>
                {/if}
                {if $aListing.is_sponsor}
                    <div class="sticky-label-icon sticky-sponsored-icon">
                        <span class="flag-style-arrow"></span>
                        <i class="ico ico-sponsor"></i>
                    </div>
                {/if}
                {if $aListing.is_featured}
                    <div class="sticky-label-icon sticky-featured-icon">
                        <span class="flag-style-arrow"></span>
                        <i class="ico ico-diamond"></i>
                    </div>
                {/if}
            </div>
            {if $bShowModerator}
                <div class="moderation_row">
                    <label class="item-checkbox">
                       <input type="checkbox" class="js_global_item_moderate" name="item_moderate[]" value="{$aListing.listing_id}" id="check{$aListing.listing_id}" />
                       <i class="ico ico-square-o"></i>
                   </label>
                </div>
            {/if}
				{*<div class="item-info">
                {img user=$aListing suffix='_50_square'}
                <div class="item-info-author ml-1">
                    <div>{_p var="By"} {$aListing|user:'':'':50}</div>
                    <div>{$aListing.time_stamp|convert_time}</div>
                </div>
				</div>*}
        </div>
        <div class="item-inner">
            <div class="item-title ">
                <a href="{$aListing.url}">
                    {$aListing.title|clean|shorten:100:'...'|split:25}
                </a>
            </div>
            
           
        </div>
    </div>
</article>