<?php

defined('PHPFOX') or exit('NO DICE!');

?>

<div id="js_sports_form_holder" class="uploader-photo-fix-height">
    {if $iTotalImage < $iTotalImageLimit}
        {module name='core.upload-form' type='sports' params=$aForms.params}
        <div class="market-app cancel-upload">
            <a href="javascript:void(0)" onclick="$Core.sports.toggleUploadSection({$iListingId}); return false;"><i class="ico ico-arrow-left"></i>&nbsp;{_p var='back_to_manage'}</a>
            <a href="{url link='admincp.sports.listsports'}" id="js_listing_done_upload" style="display: none;" class="text-uppercase"><i class="ico ico-check"></i>&nbsp;{_p var='finish_upload'}</a>
        </div>
    {else}
        <p>{_p var='you_cannot_add_more_image_to_your_listing'}</p>
    {/if}
</div>
