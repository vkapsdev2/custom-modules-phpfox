<?php 
	defined('PHPFOX') or exit('NO DICE!'); 
?>
<div class="Join_sports_div">
<div style="background: red none repeat scroll 0% 0%; color: white; padding: 5px; margin-bottom: 5px;display:none;" class="join_sports_error">Select Any one</div>
<div>
	<label class="item-checkbox">Sports Option</label>
	<select id="sports_option_id">
		<option value="">Select Option</option>
		{foreach from=$aSportsOptions key=iKey item=options}
		<option value="{$options.sports_option_id}">{$options.title}</option>
		{/foreach}
	</select>
</div>
<div>
	<label class="item-checkbox">
		<input type="checkbox" class="js_global_item_moderate" name="manager" value="1" id="check{$aItem.blog_id}" />
		Manager
	</label>
</div>

<div>
	<label class="item-checkbox">
		<input type="checkbox" class="js_global_item_moderate" name="player" value="2" id="player" />
		Player
	</label>
</div>
<div>
	<label class="item-checkbox">
		<input type="checkbox" class="js_global_item_moderate" name="fan" value="3" id="fan" />
		Fan
	</label>
</div>
<a href="javascript:void(0)" onclick="callJoinSports({$aSportsId});" class="btn btn-primary" title="{_p var='Join Sports'}">Join</a>
</div>
{literal}
<script type="text/javascript">
function callJoinSports($aSportsId)
{
	var atLeastOneIsChecked = $('.Join_sports_div input:checkbox').is(':checked');
	var sports_option_id = $("#sports_option_id").val();
	if(!sports_option_id)
	{
		$(".join_sports_error").show();
		return false;
	}
	if(atLeastOneIsChecked)
	{
		var yourArray = []
		$(".Join_sports_div input:checkbox:checked").each(function(){
		yourArray.push($(this).val());
		});
		$.ajaxCall('sports.joinsports', 'id='+$aSportsId+'&options='+yourArray+'&sports_option_id='+sports_option_id);
	}
	else
	{
		$(".join_sports_error").show();
	}
}
</script>
  
{/literal}