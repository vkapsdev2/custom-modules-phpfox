<?php 
	defined('PHPFOX') or exit('NO DICE!'); 
?>

<div class="market-app detail-extra-info">

    <div class="item-info-author" style="display:none">
        {img user=$aListing suffix='_50_square'}
        <div class="item-detail-author">
            <div>{_p var="By"} {$aListing|user:'':'':50}</div>
            <div>{_p var="posted_on"} {$aListing.time_stamp|convert_time}</div>
        </div>
    </div>
    {if !empty($aListing.mini_description)}
        <div class="item-info-short-desc">
            {$aListing.mini_description|clean}
        </div>
    {/if}
	{if !empty($aListing.description)}
		<div class="item-info-long-desc item_view_content">
			<div class="item-text" itemprop="description">
				{$aListing.description|parse|shorten:200:'feed.view_more':true|split:70}
				{if $aListing.total_attachment}
					{module name='attachment.list' sType=sports iItemId=$aListing.listing_id}
				{/if}
			</div>
		</div>
    {/if}
	{if $aListing.join_id}
		{*<a href="javascript:void(0)" onclick="$.ajaxCall('sports.unjoinsports', 'id={$aListing.join_id}');" class="btn btn-primary btn-alert" title="{_p var='Unjoin Sports'}">Unjoin</a>*}
		<span>Already Joined</span>
	{else}
		<a href="javascript:void(0)" onclick="tb_show('Join Sports',$.ajaxBox('sports.joinsportsinitial','id={$aListing.listing_id}&width=400')); return false;" class="btn btn-primary" title="{_p var='Join Sports'}">Join</a>
	{/if}
</div>

