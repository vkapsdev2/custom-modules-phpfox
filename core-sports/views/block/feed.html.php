<?php
	defined('PHPFOX') or exit('NO DICE!');
?>

<div class="item-container market-app feed">
	<article>
			<div class="item-outer flex">
				<div class="item-media">
					<a href="{$aListing.url}" style="background-image: url(
                        {if !empty($aListing.image_path)}
                            {img server_id=$aListing.server_id title=$aListing.title path='sports.url_image' file=$aListing.image_path  suffix='_200_square' return_url=true}
                        {else}
                            {param var='sports.sports_default_photo'}
                        {/if}
					)"></a>
				</div>
				<div class="item-inner overflow">
					<a href="{$aListing.url}" class="item-title">{$aListing.title|clean|shorten:100:'...'|split:25}</a>
					
					<div class="item-description item_view_content">{$aListing.mini_description|stripbb|feed_strip|split:55|shorten:100}</div>
				</div>
		</div>
	</article>
</div>